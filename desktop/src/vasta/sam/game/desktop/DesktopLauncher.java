package vasta.sam.game.desktop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;

import vasta.sam.DecayGame;
import vasta.sam.elements.Atom;
import vasta.sam.elements.Molecule;
import vasta.sam.elements.PeriodicTable;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String...arg) {
		//Remove minimize, maximize, and exit buttons from frame
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
//		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		config.width = DecayGame.GAME_WIDTH;
		config.height = DecayGame.GAME_HEIGHT;
		config.foregroundFPS = Math.max(DecayGame.FRAMES_PER_SECOND, DecayGame.UPDATES_PER_SECOND);
		config.backgroundFPS = 10;
		config.forceExit = true;
		config.resizable = true;
		config.vSyncEnabled = DecayGame.V_SYNC;
		config.fullscreen = DecayGame.FULLSCREEN;
		config.samples = DecayGame.MSAA_SAMPLES;
		
		LwjglApplication app = new LwjglApplication(new DecayGame(), config);
		
		
		int num = 173;
		System.out.println(num & 0b00001000);
		System.out.println(num & 0b00000100);
		System.out.println(num & 0b00000010);
		System.out.println(num & 0b00000001);
		
		
//		PeriodicTable t = new PeriodicTable(true);
//		ArrayList<Atom> atoms = new ArrayList<Atom>();
//		for(int i = 0; i < 5; i++){
//			atoms.add(new Atom(t.getRandomElement(true)));
//		}
//		Molecule m = new Molecule(atoms);
//		float totalDamage = m.getDamage();
//		for(float f: m.getDamageMods()){
//			totalDamage += m.getDamage()*f;
//		}
//		System.out.println("\n\nDamage: " + m.getDamage()+"\nDamageMods: " + Arrays.toString(m.getDamageMods())+"\nTotalDamage: " + totalDamage);
		
//		testTable();
	}
	
	private static void testTable(){
		int test = 2;
		
		PeriodicTable t = null;
		if(test == 1){
			t = new PeriodicTable(true);
			System.out.println("\n\n\n\n");
			System.out.printf("Average Min. Hardness:\t\t%f%n"
					+ "Average Min. Malleability:\t%f%n"
					+ "Average Min. Reactiveness:\t%f%n"
					+ "Average Max Reactiveness:\t%f%n"
					+ "Average avg. Hardness:\t\t%f%n"
					+ "Average avg. Malleability:\t%f%n"
					+ "Average avg. Reactiveness:\t%f%n"
					+ "Average rarity:\t\t\t%f%n"
					+ "Rarity Range:\t\t\t%f - %f%n",
					t.getMinH(), t.getMinM(), t.getMinR(), t.getMaxR(), t.getAvgH(), t.getAvgM(), t.getAvgR(), t.getAvgRar(), t.getMinRar(), t.getMaxRar());
			test = 3;
		}else if(test == 2){
			float avgMinH=0, avgMinM=0, avgMinR=0, avgMaxR=0, avgAvgH=0, avgAvgM=0, avgAvgR=0, avgAvgRar=0, avgMinRar=0, avgMaxRar=0;
			int numPasses = 10000;
			PeriodicTable table;
			
			int[] vals = new int[1000];
			for(int i = 0; i < numPasses; i++){
				table = new PeriodicTable(false);
				avgMinH += table.getMinH();
				avgMinM += table.getMinM();
				avgMinR += table.getMinR();
				avgMaxR += table.getMaxR();
				avgAvgH += table.getAvgH();
				avgAvgM += table.getAvgM();
				avgAvgR += table.getAvgR();
				avgAvgRar += table.getAvgRar();
				avgMinRar += table.getMinRar();
				avgMaxRar += table.getMaxRar();
				
				for(int j = 0; j < 1000; j++){
					vals[j] += table.getVals()[j];
				}
				
				if(i%(numPasses/10) == 0) System.out.println(100*i/(float)numPasses + "%");
			}
			avgMinH /= (float)numPasses;
			avgMinM /= (float)numPasses;
			avgMinR /= (float)numPasses;
			avgMaxR /= (float)numPasses;
			avgAvgH /= (float)numPasses;
			avgAvgM /= (float)numPasses;
			avgAvgR /= (float)numPasses;
			avgAvgRar /= (float)numPasses;
			avgMinRar /= (float)numPasses;
			avgMaxRar /= (float)numPasses;
			
			System.out.printf("Average Min. Hardness:\t\t%f%n"
					+ "Average Min. Malleability:\t%f%n"
					+ "Average Min. Reactiveness:\t%f%n"
					+ "Average Max Reactiveness:\t%f%n"
					+ "Average avg. Hardness:\t\t%f%n"
					+ "Average avg. Malleability:\t%f%n"
					+ "Average avg. Reactiveness:\t%f%n"
					+ "Average rarity:\t\t\t%f%n"
					+ "Rarity Range:\t\t\t%f - %f%n",
					avgMinH, avgMinM, avgMinR, avgMaxR, avgAvgH, avgAvgM, avgAvgR, avgAvgRar, avgMinRar, avgMaxRar);
					
				
			int max = 0, min = 10000000;
			for(int v: vals){
				if(v < min) min = v;
				if(v > max) max = v;
			}
			min--;
			for(int i = 0; i < vals.length; i++){
				vals[i] -= min;
				vals[i] = (int)(100*(vals[i]/(float)(max-min)));
			}
			final int[] finalVals = Arrays.copyOf(vals, vals.length);
			int mouseX;
			final listener l = new listener();
			JFrame frame = new JFrame(){
				public void paint(Graphics g){
					BufferedImage bi = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
					Graphics bg = bi.getGraphics();
					bg.setColor(Color.WHITE);
					bg.fillRect(getWidth()*3/4, 20, 100, 50);
					bg.setColor(Color.black);
					for(int i = 0; i < finalVals.length; i++){
						bg.drawLine(i+10, getHeight() - 20, i+10, getHeight() - 20 - (finalVals[i] * ((getHeight()-50)/100)));
						if(l.getX() < finalVals.length && l.getX() >= 0)
							bg.drawString("#: " + finalVals[l.getX()] + ", " + (l.getX()/10f) + "%", getWidth()*3/4, 50);
					}
					g.drawImage(bi, 0, 0, null);
					repaint();
				}
			};
			frame.setSize(new Dimension(1050, 750));
			
			frame.addMouseMotionListener(l);
			frame.setLocationRelativeTo(null);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
 		}
		if(test == 3){
			
//			PeriodicTable t = new PeriodicTable(false);
			float vals[] = new float[t.getNumElements()];
			float rarityVals[] = new float[t.getNumElements()];
			for(int i = 0; i < t.getNumElements(); i++){
				rarityVals[i] = t.getElement(i).getRarity();
			}
			final int numPasses = 10000;
			for(int i = 0; i < numPasses; i++){
				vals[t.getRandomElement(true).getAtomicNum()-1]++;
			}
//			for(int i = 0; i < vals.length; i++)
//				vals[i] /= (float)numPasses;
			final float[] finalVals = Arrays.copyOf(vals, vals.length);
			final float[] finalRarityVals = Arrays.copyOf(rarityVals, rarityVals.length);
			int mouseX;
			final listener l = new listener();
			JFrame frame = new JFrame(){
				public void paint(Graphics g){
					BufferedImage bi = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
					Graphics bg = bi.getGraphics();
					bg.setColor(Color.WHITE);
					bg.fillRect(getWidth()/2, 20, 100, 50);
					bg.setColor(Color.BLACK);
					for(int i = 0; i < finalVals.length; i++){
						bg.drawLine(i+10, getHeight() - 20, i+10, (int)(getHeight() - 20 - (finalVals[i] * numPasses / numPasses)));
						if(l.getX() >= 0 && l.getX() < finalVals.length)
							bg.drawString("#: " + l.getX() + ", " +  finalVals[l.getX()] + "%", getWidth()/2, 50);
						
					}
					bg.setColor(new Color(255, 0, 0, 128));
					for(int i = 0; i < finalVals.length; i++){
						bg.drawLine(i+10, getHeight() - 20, i+10, (int)(getHeight() - 20 - (finalRarityVals[i] * (getHeight()*3/10))));
						
					}
					g.drawImage(bi, 0, 0, null);
					repaint();
				}
			};
			frame.setSize(new Dimension(250, 750));
			
			frame.addMouseMotionListener(l);
			frame.setLocationRelativeTo(null);
			frame.setLocation(frame.getLocation().x - 3*frame.getWidth(), frame.getLocation().y);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
		}
	}
	
}
class listener implements MouseMotionListener{
	int mouseX;
	@Override
	public void mouseDragged(MouseEvent e) {
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX()-10;
	}
	
	public int getX(){
		return mouseX;
	}
	
}
