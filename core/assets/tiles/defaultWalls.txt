defaultWalls.png
format: RGBA8888
filter: Linear,Linear
repeat: none
tile00
  rotate: false
  xy: 4, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile01
  rotate: false
  xy: 4, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile02
  rotate: false
  xy: 4, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile03
  rotate: false
  xy: 4, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile04
  rotate: false
  xy: 4, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile05
  rotate: false
  xy: 202, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile06
  rotate: false
  xy: 202, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile07
  rotate: false
  xy: 202, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile08
  rotate: false
  xy: 202, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile09
  rotate: false
  xy: 202, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile10
  rotate: false
  xy: 400, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile11
  rotate: false
  xy: 400, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile12
  rotate: false
  xy: 400, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile13
  rotate: false
  xy: 400, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile14
  rotate: false
  xy: 202, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile15
  rotate: false
  xy: 400, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile16
  rotate: false
  xy: 598, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile17
  rotate: false
  xy: 598, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile18
  rotate: false
  xy: 598, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile19
  rotate: false
  xy: 598, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile20
  rotate: false
  xy: 598, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile21
  rotate: false
  xy: 796, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile22
  rotate: false
  xy: 796, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile23
  rotate: false
  xy: 796, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile24
  rotate: false
  xy: 796, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile25
  rotate: false
  xy: 796, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile26
  rotate: false
  xy: 994, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile27
  rotate: false
  xy: 994, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile28
  rotate: false
  xy: 994, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile29
  rotate: false
  xy: 994, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile30
  rotate: false
  xy: 994, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile31
  rotate: false
  xy: 1192, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile32
  rotate: false
  xy: 1390, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile33
  rotate: false
  xy: 1588, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile34
  rotate: false
  xy: 1786, 4
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile35
  rotate: false
  xy: 1192, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile36
  rotate: false
  xy: 1192, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile37
  rotate: false
  xy: 1192, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile38
  rotate: false
  xy: 1192, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile39
  rotate: false
  xy: 1390, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile40
  rotate: false
  xy: 1588, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile41
  rotate: false
  xy: 1786, 202
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile42
  rotate: false
  xy: 1390, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile43
  rotate: false
  xy: 1390, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile44
  rotate: false
  xy: 1390, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile45
  rotate: false
  xy: 1588, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile46
  rotate: false
  xy: 1786, 400
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile47
  rotate: false
  xy: 1588, 598
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
tile48
  rotate: false
  xy: 1588, 796
  size: 192, 192
  orig: 192, 192
  offset: 0, 0
  index: -1
