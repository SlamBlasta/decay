package vasta.sam.game;

import vasta.sam.DecayGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Jul 14, 2014<br>
 * A custom implementation of the Orthographic Camera. Moves towards the destination point.
 * @author Sam Vasta
 */
public class Camera extends OrthographicCamera{

	/**
	 * The point which the camera moves towards
	 */
	protected Vector2 dest;
	/**
	 * The percent of the distance between the current position and destination to move the camera each cycle.
	 */
	protected float moveSpeed = 0.41f;
	
	public Camera(){
		super();
		dest = new Vector2(DecayGame.GAME_WIDTH/2, DecayGame.GAME_HEIGHT/2);
		this.setToOrtho(false, DecayGame.GAME_WIDTH, DecayGame.GAME_HEIGHT);
//		this.setZoom(0.5f);
	}
	
	float acc = 0;	//accumulator
	/**
	 * updates the camera's position to move towards the destination
	 * @param updateFrustum - boolean of some junk for super class.
	 * @param delta - % of one second since last method call
	 */
	public void update(boolean updateFrustum, float delta){
		acc += delta;
		translate((float)((dest.x-position.x) * moveSpeed * delta), (float)((dest.y - position.y) * moveSpeed * delta));
		setZoom(0.8f + (1 + (float)Math.cos(acc))/15f);
		super.update(updateFrustum);
	}
	
	/**
	 * updates the camera's position to move towards the destination
	 * @param delta - % of one cycle since last method call
	 */
	public void update(float delta){
		this.update(true, delta);
	}
	
	@Override
	public void update(){
		update(true, Gdx.graphics.getDeltaTime());
	}
	
	public float getX(){
		return position.x;
	}
	
	public float getY(){
		return position.y;
	}
	
	public Vector2 getPosition(){
		return new Vector2(position.x, position.y);
	}
	
	/**
	 * Sets the destination of the camera
	 * @param dest
	 */
	public void setDest(Vector2 dest){
		this.dest = dest;
	}
	
	/**
	 * Sets zoom to zoomAmt
	 * @param zoom
	 */
	public void setZoom(float zoomAmt){
		this.zoom = 1f/zoomAmt;
	}
	
	/**
	 * Adds zoomAmt to zoom
	 * @param zoomAmt
	 */
	public void zoomIn(float zoomAmt){
		if(zoomAmt < 1)	zoomAmt++;
		this.zoom *= 1f/zoomAmt;
	}
	
	/**
	 * Subtracts zoomAmt from zoom
	 * @param zoomAmt
	 */
	public void zoomOut(float zoomAmt){
		if(zoomAmt < 1)	zoomAmt++;
		this.zoom *= zoomAmt;
	}
	
	
}
