package vasta.sam.game.states;

import vasta.sam.DecayGame;
import vasta.sam.game.GameState;
import vasta.sam.game.entities.Actor;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityFactory;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.attacks.Attack;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.game.entities.systems.movers.physics.AttackPhysics;
import vasta.sam.game.entities.systems.movers.physics.PhysicsSystem;
import vasta.sam.game.entities.systems.movers.physics.PlayerController;
import vasta.sam.game.world.WorldStage;
import vasta.sam.game.world.gen.ChunkMap;
import vasta.sam.game.world.gen.rooms.RoomGeneratorSimple;
import vasta.sam.util.Font;
import vasta.sam.util.MathHelper;
import vasta.sam.util.world.TileSetManager;
import box2dLight.ConeLight;
import box2dLight.DirectionalLight;
import box2dLight.Light;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

/**
 *Created by Sam Vasta, Jul 14, 2014<br>
 *The Game State that the actual game takes place in.
 *GameWorlds contain Box2D Worlds which simulate and handle
 *physics and collision. GameWorlds are the only GameStates
 *that can contain {@link Actor}.
 *@author Sam Vasta
 */
public class GameWorld extends GameState implements ContactListener{
	
	public static final int BOX_VELOCITY_ITERATIONS = 2,
							BOX_POSITION_ITERATIONS= 2;
	
	public static final int POINT_LIGHT = 0,
							CONE_LIGHT = 1,
							DIRECTIONAL_LIGHT = 2;
	
	public static final int NUM_LIGHT_RAYS = 1500;
	
	public static final short GROUP_INDEX_NO_SHADOW = -1;
	
	/**
	 * Time between box2d world steps.
	 */
	public static final float BOX_STEP = 0.01667f;
	
	/**
	 * The Box2D World for physics simulation
	 */
	protected static World boxWorld;
	
	protected ChunkMap map;
	/**
	 * The Box2DLights handler for Lighting
	 */
	protected RayHandler boxWorldLightsHandler;
	
	/**
	 * A delta time counter to ensure equal time between Box2D world steps.
	 */
	protected float accumulator = 0;
	
	/**
	 * A weird debug thing that I'll never use.
	 */
	Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer(true, false, false, false, false, false);
	
	
	Actor player;
	
	public static World getBoxWorld(){
		return boxWorld;
	}
	
	@Override
	protected void initStage(){
		stage = new WorldStage();
	}
	
	@Override
	public void init(){
		boxWorld = new World(new Vector2(0,0), true);
		boxWorld.setContactListener(this);
		int num = 2;
		map = new ChunkMap(this);
		for(int i = -num; i < num+1; i++){
			for(int j = -num; j < num+1; j++){
				map.generate(i, j);
			}
		}
		
		boxWorldLightsHandler = new RayHandler(boxWorld, DecayGame.GAME_WIDTH*4, DecayGame.GAME_HEIGHT*4);
		boxWorldLightsHandler.useDiffuseLight(false);
		boxWorldLightsHandler.setShadows(true);
		boxWorldLightsHandler.setWorld(boxWorld);
		boxWorldLightsHandler.setBlurNum(1);
		boxWorldLightsHandler.setAmbientLight(0.3f);
		
		Light.setContactFilter(GROUP_INDEX_NO_SHADOW, GROUP_INDEX_NO_SHADOW, GROUP_INDEX_NO_SHADOW);

		addLight(POINT_LIGHT, new Color(1f, 1f, 1f, 0.4f), 1000f, 0f, 0f);
		for(int i = 0; i < 5; i++){
			addLight(POINT_LIGHT, new Color((float)Math.random(), (float)Math.random(), (float)Math.random(), (float)Math.random()*0.4f), MathHelper.nextInt(100, 1000), MathHelper.nextInt(-2000, 2000), MathHelper.nextInt(-2000, 2000), 0, 0, 0, false, false);
		}
		
		
//		addLight(POINT_LIGHT, new Color(0.8f, 0.8f, 0.16f, 0.1f), (float)Math.sqrt(DecayGame.GAME_WIDTH*DecayGame.GAME_WIDTH + DecayGame.GAME_HEIGHT*DecayGame.GAME_HEIGHT), -200, 200, 0, 0, 0f, false, false);
//		addLight(POINT_LIGHT, new Color(0f, 0f, 0.6f, 0.2f), 1000, 0, 0);
//		addLight(POINT_LIGHT, new Color(0.5f, 0.1f, 0.5f, 0.3f), 1000, -200, -200);
//		
//		addLight(POINT_LIGHT, new Color(0.8f, 0f, 0.16f, 0.4f), (float)Math.sqrt(DecayGame.GAME_WIDTH*DecayGame.GAME_WIDTH + DecayGame.GAME_HEIGHT*DecayGame.GAME_HEIGHT), 1200, -1100, 0, 0, 0f, false, false);
//		addLight(POINT_LIGHT, new Color(1f, 0f, 0.6f, 0.4f), 1000, -1000, -390);
//		addLight(POINT_LIGHT, new Color(0.5f, 0.1f, 1f, 0.4f), 1000, 300, -1200);
		
		player = EntityFactory.getPlayer(this);
		System.out.println("Table from EntityFactory (under player)");
		addActor(player);
		l = new PointLight(boxWorldLightsHandler, 1000, new Color(1f, 1f, 0f, 0.3f), 1000, 0, 0);
	}
	
	@Override
	public void render(DecayGame q) {
		map.render();
		stage.draw();
		boxWorldLightsHandler.setCombinedMatrix(camera.combined.scale(PhysicsSystem.BOX_TO_WORLD, PhysicsSystem.BOX_TO_WORLD, 0f));
		boxWorldLightsHandler.updateAndRender();
		
//		batch.begin();
//		Font.setAndRender(100, Color.WHITE, batch, "DECAY", 100, 100);
//		Font.setAndRender(20, Color.WHITE, batch, "A Game by Sam Vasta", 520, 100);
//		Font.setAndRender(20, Color.WHITE, batch, DecayGame.TITLE + " " + DecayGame.VERSION, 20, DecayGame.GAME_HEIGHT-40);
//		batch.end();
		
		
//		Batch b = new SpriteBatch();
//		b.begin();
//		b.draw(TileSetManager.getStandardTile(9, "tiles/defaultWalls.png").getTextureRegion(), 0, 0);
//		b.end();
	}

	Light l;
	long lastUpdate = 0;
	@Override
	public void update(DecayGame q, float delta) {
//		System.out.println("DELTA: " + delta);
		l.setPosition(PhysicsSystem.convertToBox(player.getMover().getPosition()));
		camera.setDest(player.getPhysics().getPosition());
		
		accumulator += delta;
		while(accumulator > BOX_STEP){
			boxWorld.step(delta*DecayGame.UPDATE_TIME, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
			accumulator -= BOX_STEP;
		}
		

		if(System.currentTimeMillis() - lastUpdate >= 1000){
			lastUpdate = System.currentTimeMillis();
			map.getLoadedRooms((int)camera.getX(), (int)camera.getY());
		}
		
//		player.broadcast(new Notification(Notification.ATTACK_TOWARDS_POSITION, 0f, 10f));
		
		if(Math.random() <= 0){
			Actor a = EntityFactory.getTestActor(this);
			a.setPosition(MathHelper.nextInt(-500, 500), MathHelper.nextInt(-500, 500));
			addActor(a);
		}
		

//		if(System.currentTimeMillis() % 1000 <= 20)
//			System.out.println("Box bodies: " + boxWorld.getBodyCount() + ", Stage bodies: " + stage.getActors().size);
	}
	
	/**
	 * Adds a light. Assumes a 0 direction, 0 cone degree, 0 softness, xRay false, isStatic false.
	 * @param type
	 * @param color
	 * @param dist
	 * @param x
	 * @param y
	 */
	public void addLight(int type, Color color, float dist, float x, float y){
		this.addLight(type, color, dist, x, y, 0, 0, 0, false, false);
	}
	
	/**
	 * Helper method for adding lights. Automatically sets softness/xRay/Static, etc.
	 * @param type
	 * @param color
	 * @param dist
	 * @param x
	 * @param y
	 * @param dir
	 * @param coneDir
	 * @param softness
	 * @param xRay
	 * @param isStatic
	 */
	public void addLight(int type, Color color, float dist, float x, float y, float dir, float coneDir, float softness, boolean xRay, boolean isStatic){
		dist = PhysicsSystem.convertToBox(dist);
		x = PhysicsSystem.convertToBox(x);
		y = PhysicsSystem.convertToBox(y);
		Light l;
		switch(type){
		default:
		case POINT_LIGHT:
			l = new PointLight(boxWorldLightsHandler, NUM_LIGHT_RAYS, color, dist, x, y);
			break;
		case CONE_LIGHT:
			l = new ConeLight(boxWorldLightsHandler, NUM_LIGHT_RAYS, color, dist, x, y, dir, coneDir);
			break;
		case DIRECTIONAL_LIGHT:
			l = new DirectionalLight(boxWorldLightsHandler, NUM_LIGHT_RAYS, color, dir);
			break;
		}
		if(softness != 0)
			l.setSoft(true);
		l.setSoftnessLength(softness);
		l.setXray(xRay);
		l.setStaticLight(isStatic);
	}
	
	
	/**
	 * Translates a given point directly from the screen into a point in the world.
	 * @param point
	 * @return
	 */
	public Vector2 getScreenPointInWorld(Vector2 point){
		return new Vector2(camera.getX() + (point.x - DecayGame.GAME_WIDTH/2), camera.getY() + (DecayGame.GAME_HEIGHT - (point.y + DecayGame.GAME_HEIGHT/2)));
	}
	
	public Vector2 getScreenPointInWorld(float x, float y){
		return getScreenPointInWorld(new Vector2(x, y));
	}
	
	/**
	 * 
	 * @param a
	 */
	public void addActor(Actor a){
		stage.addActor(a);
	}
	
	@Override
	public int getID(){
		return DecayGame.WORLD_ID;
	}

	@Override
	public boolean keyTyped(char character) {
		if(character == 'z'){
			for(com.badlogic.gdx.scenes.scene2d.Actor a: stage.getActors()){
				if(a instanceof Entity)
					((Entity)a).setActive(false);
			}
		}
		else if(character == 'x'){
			for(com.badlogic.gdx.scenes.scene2d.Actor a: stage.getActors()){
				if(a instanceof Entity)
					((Entity)a).setActive(true);
			}
		}
		else if(character == 'c'){
			for(com.badlogic.gdx.scenes.scene2d.Actor a: stage.getActors()){
				if(a instanceof Entity){
					if(!(a instanceof Actor && ((Actor)a).getPhysics() instanceof PlayerController))
						((Entity)a).kill();
				}
			}
		}
		else if(character == 'a'){
//			System.out.println("NUM ACTORS: " + stage.getActors().size);
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button == Buttons.LEFT){
			Actor a = EntityFactory.getTestActor(this);
			a.setPosition(getScreenPointInWorld(screenX, screenY));
			addActor(a);
		}
		if(button == Buttons.RIGHT){
//			for(com.badlogic.gdx.scenes.scene2d.Actor actor: stage.getActors()){
//				if(actor instanceof Actor){
//					Actor a = (Actor)actor;
//					if(a.collidesWith(getScreenPointInWorld(screenX, screenY)))
//						a.kill();
//				}
//			}
//			player.broadcast(new Notification(Notification.ATTACK_TOWARDS_POSITION, stage.getCamera().position.x - (float)screenX, stage.getCamera().position.y - DecayGame.GAME_HEIGHT + (float)screenY));
		}
		player.broadcast(new Notification(NotificationType.ATTACK_TOWARDS_POSITION, getScreenPointInWorld(screenX, screenY).x, getScreenPointInWorld(screenX, screenY).y));
		return false;
	}

	@Override
	public void beginContact(Contact contact) {
		if(contact.getFixtureA().getUserData() instanceof AttackPhysics && ((EntitySystem)contact.getFixtureA().getUserData()).getEntity() instanceof Attack){
			EntitySystem es = (EntitySystem)(contact.getFixtureA().getUserData());
			Attack a = (Attack)(es.getEntity());
			
			EntitySystem es2 = (EntitySystem)(contact.getFixtureB().getUserData());
			a.startDamage(es2.getEntity());
		}
		else if(contact.getFixtureB().getUserData() instanceof AttackPhysics && ((EntitySystem)contact.getFixtureB().getUserData()).getEntity() instanceof Attack){
			EntitySystem es = (EntitySystem)(contact.getFixtureB().getUserData());
			Attack a = (Attack)(es.getEntity());
			
			EntitySystem es2 = (EntitySystem)(contact.getFixtureA().getUserData());
			a.startDamage(es2.getEntity());
		}
		
//		System.out.println("A: " + contact.getFixtureA().getUserData().getClass() + "\n\tB: " + contact.getFixtureB().getUserData().getClass());
//		
//		if(contact.getFixtureA().getUserData() instanceof PhysicsBox){
//			System.out.println("A is good!");
//		}
//		if(contact.getFixtureB().getUserData() instanceof PhysicsBox){
//			System.out.println("B is good!");
//		}else System.out.println("B IS FALSE");
	}

	@Override
	public void endContact(Contact contact) {
		if(contact.getFixtureA().getUserData() instanceof AttackPhysics && ((EntitySystem)contact.getFixtureA().getUserData()).getEntity() instanceof Attack){
			EntitySystem es = (EntitySystem)(contact.getFixtureA().getUserData());
			Attack a = (Attack)(es.getEntity());
			
			EntitySystem es2 = (EntitySystem)(contact.getFixtureB().getUserData());
			a.stopDamage(es2.getEntity());
		}
		else if(contact.getFixtureB().getUserData() instanceof AttackPhysics && ((EntitySystem)contact.getFixtureB().getUserData()).getEntity() instanceof Attack){
			EntitySystem es = (EntitySystem)(contact.getFixtureB().getUserData());
			Attack a = (Attack)(es.getEntity());
			
			EntitySystem es2 = (EntitySystem)(contact.getFixtureA().getUserData());
			a.stopDamage(es2.getEntity());
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if(pointer == Buttons.LEFT){
			Actor a = EntityFactory.getTestActor(this);
			a.setPosition(getScreenPointInWorld(screenX, screenY));
			addActor(a);
		}
		
		player.broadcast(new Notification(NotificationType.ATTACK_TOWARDS_POSITION, getScreenPointInWorld(screenX, screenY).x, getScreenPointInWorld(screenX, screenY).y));
		
		return false;
	}

}
