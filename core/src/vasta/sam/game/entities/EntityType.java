package vasta.sam.game.entities;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * EntityType signifies what the entity is used for.
 * <p>
 * @author Sam Vasta
 */
public enum EntityType {
	HUD(),
	PARTICLE(),
	PLAYER(),
	ACTOR(),
	ATTACK(),
	TILE(),
}
