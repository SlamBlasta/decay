package vasta.sam.game.entities.attacks;

import java.util.ArrayList;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;

/**
 * Created by Sam Vasta, Sep 1, 2014
 *<p>
 * AOE Attacks deal damage to all entities in an area. They last for a duration of time
 * instead of a number of hits.
 *<p>
 * @author Sam Vasta
 */
public class AreaOfEffectAttack extends Attack{
	
	protected float life;
	protected long duration;
	
	public AreaOfEffectAttack(Entity origin, int damage, long duration){
		super(origin, damage);
		this.duration = duration;
		life = 0;
	}
	
	public AreaOfEffectAttack(Entity origin, int damage, float[] damageMods, long duration){
		super(origin, damage, damageMods);
		this.duration = duration;
		life = 0;
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		life += delta;
		if(life >= duration){
			this.kill();
		}
	}

}
