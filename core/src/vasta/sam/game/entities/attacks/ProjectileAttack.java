package vasta.sam.game.entities.attacks;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.movers.physics.AttackPhysics;

/**
 * Created by Sam Vasta, Sep 1, 2014
 *<p>
 * Projectile Attacks hit one enemy at a time, and last for a certain number of hits.
 *<p>
 * @author Sam Vasta
 */
public class ProjectileAttack extends Attack{
	/**
	 * The maximum lifetime of a ProjectileAttack. When a Projectile
	 * Attack has been active for longer than this time, it will destroy
	 * itself
	 */
	public static final long MAX_LIFE_TIME = 5 * 1000;
	
	/**
	 * The number of times this {@link Attack} can damage an {@link Entity}
	 */
	protected int pierceNum = 1;
	
	/**
	 * Creation time. Used for automatically killing an attack after it has
	 * been alive for {@link MAX_LIFE_TIME}
	 */
	private long startTime;

	public ProjectileAttack(Entity origin, int damage) {
		super(origin, damage);
		startTime = System.currentTimeMillis();
	}
	
	public ProjectileAttack(Entity origin, int damage, float[] damageMods) {
		super(origin, damage, damageMods);
		startTime = System.currentTimeMillis();
	}
	
	public ProjectileAttack(Entity origin, int damage, float[] damageMods, int pierceNum){
		super(origin, damage, damageMods);
		this.pierceNum = pierceNum;
		startTime = System.currentTimeMillis();
	}
	
	public ProjectileAttack(Entity origin, int damage, int pierceNum){
		super(origin, damage);
		this.pierceNum = pierceNum;
		startTime = System.currentTimeMillis();
	}

	@Override
	public void act(float delta){
		super.act(delta);
		if(pierceNum <= 0){
			this.kill();
		}
		
		if(System.currentTimeMillis() - startTime >= MAX_LIFE_TIME){
			this.kill();
		}
	}
	
	@Override
	protected boolean hit(Entity e){
		if(super.hit(e)){
			pierceNum--;
			return true;
		}
		return false;
	}
	
	@Override
	public void startDamage(Entity e){
		entitiesInAttack.add(e);
	}
	
	@Override
	public void stopDamage(Entity e){
		entitiesInAttack.remove(e);
	}
	
	@Override
	public void addSystem(EntitySystem sys){
		if(sys instanceof AttackPhysics){
			if(mover != null) systems.remove(mover);
			this.mover = (AttackPhysics)sys;
		}
		if(sys instanceof DrawSystem){
			if(drawer != null) systems.remove(drawer);
			this.drawer = (DrawSystem)sys;
		}
		systems.add(sys);
	}

}
