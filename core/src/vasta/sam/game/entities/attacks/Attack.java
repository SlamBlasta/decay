package vasta.sam.game.entities.attacks;

import java.util.ArrayList;

import vasta.sam.game.entities.Actor;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.movers.physics.AttackPhysics;

/**
 * Created by Sam Vasta, Aug 30, 2014
 *<p>
 * An attack is an area in the game world which can deal damage to actors.
 * There are two types of attacks: Projectiles, and Areas of Effect.
 *<p>
 * @author Sam Vasta
 */
public abstract class Attack extends Actor{

	/**
	 * The amount of physical damage this attack does.
	 */
	protected int damage;
	/**
	 * The modifiers for each damage type of this attack.
	 */
	protected float[] damageMods;
	/**
	 * A list of entities that this attack effects.
	 */
	protected ArrayList<Entity> entitiesInAttack;
	/**
	 * The entity which created this attack. Attacks will not effec this entity.
	 */
	protected Entity origin;
	
	public Attack(Entity origin, int damage){
		super(EntityType.ATTACK);
		this.origin = origin;
		this.damage = damage;
		this.damageMods = new float[]{0f,0f,0f,0f,0f,0f};
		this.entitiesInAttack = new ArrayList<Entity>();
	}
	
	public Attack(Entity origin, int damage, float[] damageMods){
		super(EntityType.ATTACK);
		this.origin = origin;
		this.damage = damage;
		this.damageMods = damageMods;
		this.entitiesInAttack = new ArrayList<Entity>();
	}
	
	@Override
	public void act(float delta){
		super.act(delta);
		
		Entity[] entities = entitiesInAttack.toArray(new Entity[]{});
		for(Entity e: entities){
			hit(e);
			if(e.isDead()){
				stopDamage(e);
			}
		}
	}
	
	@Override
	public void kill(){ 
		//Release references to entities
		entitiesInAttack.clear();
		origin = null;
		//Continue normal kill process
		super.kill();
	}
	
	/**
	 * Applies this attack to the given entity
	 * @param e
	 */
	protected boolean hit(Entity e){
		if(origin != null && e.equals(origin))return false;
		
		e.broadcast(new Notification(NotificationType.DAMAGE, damage, (float[])damageMods));
		return true;
	}
	
	/**
	 * Adds the entity to this attack. This is called when the attack and entity first begin collision
	 * @param e
	 */
	public void startDamage(Entity e){
		entitiesInAttack.add(e);
	}
	/**
	 * Removes the entity from this attack. This is called when the attack and the entity end their collision
	 * @param e
	 */
	public void stopDamage(Entity e){
		if(entitiesInAttack.contains(e))
			entitiesInAttack.remove(e);
	}
	
	@Override
	public void addSystem(EntitySystem sys){
		if(sys instanceof AttackPhysics){
			if(mover != null) systems.remove(mover);
			this.mover = (AttackPhysics)sys;
		}
		if(sys instanceof DrawSystem){
			if(drawer != null) systems.remove(drawer);
			this.drawer = (DrawSystem)sys;
		}
		systems.add(sys);
	}
	
	//GETTERS
	
	public int getDamage(){
		return damage;
	}
	
	public float[] getDamageMods(){
		return damageMods;
	}
	
	public Entity getOrigin(){
		return origin;
	}

}
