package vasta.sam.game.entities.systems;

/**
 * Created by Sam Vasta, Aug 7, 2014<br>
 * A list of all possible states for an actor to be in.
 * @author Sam Vasta
 */
public enum State {
	
	IDLE("Idle"),
	STOPPING("Stopping"),
	MOVING("Moving"),
	GETTING_HIT("Getting Hurt"),
	SEARCHING("Acquiring Target"),
	ATTACKING("Attacking Target"),
	WAITING("Waiting"),
	RESTING("Resting"),
	ATTACK_COOLDOWN("Waiting For Attack Cooldown"),
	DYING("Dying"),
	DEAD("Dead"),
	SPAWNING("Spawning"),
	;
	
	
	public static final State[] ALL_STATES = new State[]{
		IDLE, STOPPING, MOVING, GETTING_HIT, SEARCHING,
		ATTACKING, WAITING, RESTING, ATTACK_COOLDOWN,
		DYING, DEAD, SPAWNING
		
	};
	public static final boolean[] LOOPING_DATA = new boolean[]{
		true,	//IDLE
		false,	//STOPPING
		true,	//MOVING
		false,	//GETTING_HIT
		true,	//SEARCHING
		false,	//ATTACKING
		true,	//WAITING
		true,	//RESTING
		false,	//ATTACK_COOLDOWN
		false,	//DYING
		true,	//DEAD
		false	//SPAWNING
		
	};
	public static final int NUM_STATES = ALL_STATES.length;
	
	
	private State(String name){this.name = name;}
	private String name;
	public String toString(){ return name;}
}
