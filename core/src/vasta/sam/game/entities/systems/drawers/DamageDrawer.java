package vasta.sam.game.entities.systems.drawers;

import vasta.sam.game.entities.Entity;
import vasta.sam.util.Font;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by Sam Vasta, Aug 29, 2014
 *<p>
 *
 *<p>
 * @author Sam Vasta
 */
public class DamageDrawer extends DrawSystem{
	private int amt;
	private Color col;
	public DamageDrawer(Entity e, int amt, Color col) {
		super(e, null);
		this.amt = amt;
		this.col = col;
	}
	
	@Override
	public void draw(Batch batch, float x, float y){
//		Font.render(Font.SMALL_FONT, Integer.toString(amt), (int)x, (int)y);
	}
	
	@Override
	public void draw(Batch batch, float x, float y, float rotation){
//		Font.render(Font.SMALL_FONT, Integer.toString(amt), (int)x, (int)y);
	}

	@Override
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
		Font.setAndRender(40, col, batch, Integer.toString(amt), (int)x + getWidth()/2, (int)y + getHeight()/2);
	}

}