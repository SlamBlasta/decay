package vasta.sam.game.entities.systems.drawers;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.util.AnimationHandler;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Aug 5, 2014<br>
 * @author Sam Vasta
 */
public class DrawSystem extends EntitySystem{

	/**
	 * The list of animations this Entity can use
	 */
	protected AnimationHandler animationHandler;
	
	public DrawSystem(Entity e, AnimationHandler ah){
		super(e);
		this.animationHandler = ah;
		if(ah != null)
			animationHandler.update();
	}
	
	@Override
	public void update(float delta) {
		if(animationHandler != null)
			animationHandler.update();
	}
	
	public boolean switchTo(String animationName){
		if(animationHandler == null) return false;
		return animationHandler.switchTo(animationName);
	}
	
	/**
	 * Draws the current animation at the given X,Y Position
	 * @param batch
	 * @param alpha
	 * @param x
	 * @param y
	 */
	public void draw(Batch batch, float x, float y){
		this.draw(batch, x, y, getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1f, 1f, 0);
	}
	
	/**
	 * A draw function which allows specification of rotation.
	 * @param batch
	 * @param x
	 * @param y
	 * @param rotation
	 */
	public void draw(Batch batch, float x, float y, float rotation){
		this.draw(batch, x, y, getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1f, 1f, rotation);
	}

	/**
	 * Returns the current frame of the current animation
	 * @return TextureRegion that represents the current frame of the current animation
	 */
	protected TextureRegion getTexture(){
		if(animationHandler == null) return null;
		return animationHandler.getCurrentFrame();
	}

	/**
	 * Draws the current Animation
	 * @param batch
	 * @param x
	 * @param y
	 * @param originX
	 * @param originY
	 * @param width
	 * @param height
	 * @param scaleX
	 * @param scaleY
	 * @param rotation
	 */
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
		batch.draw(getTexture(), x, y, originX, originY, width, height, scaleX, scaleY, rotation);
	}
	
	/**
	 * Returns the width of the current Frame
	 * @return width of the current frame
	 */
	public int getWidth(){
		if(animationHandler == null) return 0;
		return getTexture().getRegionWidth();
	}
	
	/**
	 * Returns the height of the current frame
	 * @return height of the current frame
	 */
	public int getHeight(){
		if(animationHandler == null) return 0;
		return getTexture().getRegionHeight();
	}

	@Override
	public void notify(Notification n) {
//		System.err.println("TODO: " + this.getClass().getName() + ".notify(Notification)");
		switch(n.getType()){
		case SET_VELOCITY:
			//Change animation based on movement
			break;
		default:break;
		}
	}
}
