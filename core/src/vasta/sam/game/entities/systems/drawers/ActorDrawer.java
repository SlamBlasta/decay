package vasta.sam.game.entities.systems.drawers;

import vasta.sam.game.entities.Actor;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.systems.State;
import vasta.sam.util.AnimationHandler;
import vasta.sam.util.Animations;


/**
 * Created by Sam Vasta, Aug 11, 2014<br>
 * @author Sam Vasta
 */
public class ActorDrawer extends DrawSystem{

	protected String nameBase;
	
	public ActorDrawer(Entity e, String animationNameBase) {
		super(e, generateAnimationHandler(animationNameBase));
		nameBase = animationNameBase;
	}
	
	@Override
	public void update(float delta){
		super.update(delta);
	}
	
	/**
	 * Takes the given name base and tries to load an animation for
	 * each State in {@link State}.
	 * @param nameBase
	 * @return an AnimationHandler with all animations for each state.
	 */
	protected static AnimationHandler generateAnimationHandler(String nameBase){
		String[] anims = new String[State.NUM_STATES];
		for(int i = 0; i < State.NUM_STATES; i++){
			if(Animations.get(nameBase + State.ALL_STATES[i].toString() + ".png") != null){
				anims[i] = nameBase + State.ALL_STATES[i].toString() + ".png";
			}
			else{
				if(i == 0){
					anims[i] = "default.png";
				}
				else{
					anims[i] = anims[0];
				}
			}
		}
		
		return new AnimationHandler(anims, State.LOOPING_DATA);
	}

}