package vasta.sam.game.entities.systems.drawers;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import vasta.sam.game.entities.Entity;
import vasta.sam.util.AnimationHandler;
import vasta.sam.util.Animations;

/**
 * Created by Sam Vasta, Sep 1, 2014
 *<p>
 *
 *<p>
 * @author Sam Vasta
 */
public class InvisibleDrawer extends DrawSystem{

	protected int width, height;
	
	public InvisibleDrawer(Entity e, int width, int height) {
		super(e, new AnimationHandler(Animations.DEFAULT_ANIMATION));
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void update(float delta) {
		//Do Nothing
	}
	
	public boolean switchTo(String animationName){
		return false;
	}
	
	@Override
	public void draw(Batch batch, float x, float y){
		//Do Nothing
	}
	
	@Override
	public void draw(Batch batch, float x, float y, float rotation){
		//Do Nothing
	}

	@Override
	protected TextureRegion getTexture(){
		return new TextureRegion();
	}

	@Override
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
		//Do nothing
	}
	
	@Override
	public int getWidth(){
		return width;
	}
	
	@Override
	public int getHeight(){
		return height;
	}

}
