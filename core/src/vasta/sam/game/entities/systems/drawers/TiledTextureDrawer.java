package vasta.sam.game.entities.systems.drawers;

import vasta.sam.game.entities.Entity;
import vasta.sam.util.AnimationHandler;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Sep 7, 2014
 * <p>
 * This drawer draws a texture over a given area.
 * The texture is not scaled, but instead tiled over
 * this area.
 * <p>
 * @author Sam Vasta
 */
public class TiledTextureDrawer extends DrawSystem{

	protected int width, height;
	TextureRegion currTex;
	
	public TiledTextureDrawer(Entity e, AnimationHandler ah, int width, int height) {
		super(e, ah);
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
		batch.draw(getTexture(), x, y, originX, originY, this.width, this.height, scaleX, scaleY, rotation);
	}
	
	@Override
	public int getWidth(){
		return width;
	}
	
	@Override
	public int getHeight(){
		return height;
	}
	
	@Override
	public TextureRegion getTexture(){
		if(currTex == null)
			currTex = getCurrTex();
		return currTex;
	}
	
	@Override
	public boolean switchTo(String animationName){
		if(super.switchTo(animationName)){
			currTex = getCurrTex();
			return true;
		}
		return false;
	}

	/**
	 * Ensures the texture is repeated (tiled)
	 * @return
	 */
	protected TextureRegion getCurrTex(){
		if(animationHandler == null) return null;
		Texture t = animationHandler.getCurrentFrame().getTexture();
		t.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		return new TextureRegion(t, width, height);
	}
}