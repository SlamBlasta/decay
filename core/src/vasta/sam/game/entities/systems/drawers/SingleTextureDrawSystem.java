package vasta.sam.game.entities.systems.drawers;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;
import vasta.sam.util.AnimationHandler;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * Only uses a single texture to draw instead of an animation handler.
 * <p>
 * @author Sam Vasta
 */
public class SingleTextureDrawSystem extends DrawSystem{

	protected TextureRegion tex;
	public SingleTextureDrawSystem(Entity e, TextureRegion tex) {
		super(e, null);
		this.tex = tex;
	}

	@Override
	public TextureRegion getTexture(){
		return tex;
	}
	
	
	/**
	 * Returns the width of the current Frame
	 * @return width of the current frame
	 */
	public int getWidth(){
		return getTexture().getRegionWidth();
	}
	
	/**
	 * Returns the height of the current frame
	 * @return height of the current frame
	 */
	public int getHeight(){
		return getTexture().getRegionHeight();
	}
}
