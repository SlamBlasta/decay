package vasta.sam.game.entities.systems.movementControllers;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.util.MathHelper;
import vasta.sam.util.Queue;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * This extension of {@link MovementControlSystem} contains a queue
 * of movement nodes. The entity will move to one node, then the next
 * until the queue is empty.
 * <p>
 * @author Sam Vasta
 */
public class QueuedMovementSystem extends MovementControlSystem{
	
	/**
	 * A list of nodes which the entity must travel to.
	 */
	protected Queue<Vector2> nodes = new Queue<>();

	public QueuedMovementSystem(Entity e) {
		this(e, new Vector2[]{e.getMover().getPosition()}, DEFAULT_MAGNITUDE);
	}
	
	public QueuedMovementSystem(Entity e, Vector2[] destinations) {
		this(e, destinations, DEFAULT_MAGNITUDE);
	}
	
	public QueuedMovementSystem(Entity e, Vector2[] destinations, float speed) {
		super(e, destinations[0], speed);
		for(Vector2 v: destinations){
			nodes.enqueue(v);
		}
	}
	
	@Override
	public void update(float delta){
		/*
		 * If the entity is at the previous destination, and there are more nodes, set the entity's destination
		 * to the next node.
		 */
		if(!nodes.isEmpty() && entity.getMover().getPosition().epsilonEquals(getDestination(), magnitudeAndAngle.x)){
			setDestination(nodes.dequeue());
		}
		super.update(delta);
	}

}
