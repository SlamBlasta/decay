package vasta.sam.game.entities.systems.movementControllers;

import vasta.sam.game.entities.Entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * A simple extension of {@link MovementControlSystem} which will
 * have an entity follow the mouse cursor. Used for testing.
 * <p>
 * @author Sam Vasta
 */
public class MouseFollower extends MovementControlSystem{
	
	public MouseFollower(Entity e) {
		super(e);
	}
	
	public MouseFollower(Entity e, Vector2 destination) {
		super(e, destination);
	}

	public MouseFollower(Entity e, Vector2 destination, float speed) {
		super(e, destination, speed);
		setDestination(entity.getStage().screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY())));
	}

	@Override
	public void update(float delta){
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			setDestination(entity.getStage().screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY())));
		}
		super.update(delta);
	}
}
