package vasta.sam.game.entities.systems.movementControllers;

import com.badlogic.gdx.math.Vector2;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * This {@link vasta.sam.game.entities.systems.EntitySystem} controls the
 * {@link vasta.sam.game.entities.systems.movers.MoveSystem} by reading
 * its state and changing velocity to accomplish specialized goals.
 * <br>
 * By default, MovementControlSystems direct an entity toward a destination
 * position.
 * <p>
 * @author Sam Vasta
 */
public class MovementControlSystem extends EntitySystem{
	
	public static final float DEFAULT_MAGNITUDE = 15f;

	private Vector2 currentDestination;
	/**
	 * This vector stores the magnitude (speed) of the entity,
	 * as well as the direction the direction of the entity.
	 */
	protected Vector2 magnitudeAndAngle;
	
	public MovementControlSystem(Entity e) {
		this(e, e.getMover().getPosition(), DEFAULT_MAGNITUDE);
	}
	
	public MovementControlSystem(Entity e, Vector2 destination) {
		this(e, destination, DEFAULT_MAGNITUDE);
	}
	
	public MovementControlSystem(Entity e, Vector2 destination, float speed) {
		super(e);
		currentDestination = destination;
		magnitudeAndAngle = new Vector2(speed, MathHelper.angleTo(entity.getMover().getPosition(), currentDestination));
	}

	/**
	 * Sets the y component of the magnitude/angle vector to the correct angle
	 */
	protected void calculateAngle(){
		magnitudeAndAngle.y = (float)Math.PI+MathHelper.angleTo(entity.getMover().getPosition(), currentDestination);
	}
	
	@Override
	public void update(float delta) {
		if(entity.getMover().getPosition().epsilonEquals(currentDestination, magnitudeAndAngle.x/4f)){
			entity.broadcast(new Notification(NotificationType.SET_POSITION, currentDestination.x, currentDestination.y));
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, 0f, 0f));
		}
		else if(MathHelper.angleTo(entity.getMover().getVelocity(), currentDestination) != magnitudeAndAngle.x){
			calculateAngle();
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, (float)Math.cos(magnitudeAndAngle.y)*magnitudeAndAngle.x, (float)Math.sin(magnitudeAndAngle.y)*magnitudeAndAngle.x));
		}
	}

	@Override
	public void notify(Notification n) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Sets destination, then recalculates angle.
	 * @param destination
	 */
	protected void setDestination(Vector2 destination){
		currentDestination = destination;
		calculateAngle();
	}

	public Vector2 getDestination(){
		return currentDestination;
	}
}