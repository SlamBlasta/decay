package vasta.sam.game.entities.systems.attackers;

import vasta.sam.elements.Molecule;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;
import vasta.sam.game.entities.attacks.Attack;
import vasta.sam.game.entities.attacks.ProjectileAttack;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.movers.physics.ProjectileAttackPhysics;
import vasta.sam.game.states.GameWorld;
import vasta.sam.util.AnimationHandler;
import vasta.sam.util.MathHelper;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Sep 3, 2014
 * <p>
 * BasicProjectileAttackSystems are attack systems which create explicitly
 * projectile attacks.
 * <p>
 * @author Sam Vasta
 */
public class BasicProjectileAttackSystem extends AttackSystem{

	public static final String DEFAULT_PROJECTILE_ANIMATION = "bullet.png";
	public static final float DEFAULT_PROJECTILE_VELOCITY = 15f;
	
	protected String animationName;
	protected float velocityMagnitude;
	protected int numPiercings;
	
	public BasicProjectileAttackSystem(Entity e, Molecule atoms) {
		this(e, atoms, DEFAULT_PROJECTILE_ANIMATION, DEFAULT_PROJECTILE_VELOCITY, 1);
	}
	
	public BasicProjectileAttackSystem(Entity e, Molecule atoms, String animationName){
		this(e, atoms, animationName, DEFAULT_PROJECTILE_VELOCITY, 1);
	}
	
	public BasicProjectileAttackSystem(Entity e, Molecule atoms, String animationName, float velocityMagnitude) {
		this(e, atoms, animationName, velocityMagnitude, 1);
	}
	
	public BasicProjectileAttackSystem(Entity e, Molecule atoms, String animationName, float velocityMagnitude, int numPiercings) {
		super(e, atoms);
		this.animationName = animationName;
		this.velocityMagnitude = velocityMagnitude;
		this.numPiercings = numPiercings;
	}

	@Override
	protected Attack createAttack(float xDest, float yDest) {
		Attack a = new ProjectileAttack(entity, atoms.getDamage(), atoms.getDamageMods(), numPiercings);
		a.addSystem(new DrawSystem(a, new AnimationHandler(animationName)));
		a.addSystem(new ProjectileAttackPhysics(a, GameWorld.getBoxWorld()));
		a.setPosition(entity.getMover().getPosition());
		float angle = MathHelper.angleTo(entity.getMover().getPosition().x, entity.getMover().getPosition().y, xDest, yDest);
		a.getMover().setVelocity(new Vector2((float)Math.cos(angle), (float)Math.sin(angle)).scl(-velocityMagnitude));
		a.getMover().setRotation(angle);
		return a;
	}

}
