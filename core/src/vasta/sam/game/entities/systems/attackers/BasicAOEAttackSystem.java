package vasta.sam.game.entities.systems.attackers;

import vasta.sam.elements.Molecule;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityType;
import vasta.sam.game.entities.attacks.AreaOfEffectAttack;
import vasta.sam.game.entities.attacks.Attack;
import vasta.sam.game.entities.systems.drawers.TiledTextureDrawer;
import vasta.sam.game.entities.systems.movers.physics.AOEAttackPhysics;
import vasta.sam.game.states.GameWorld;
import vasta.sam.util.AnimationHandler;

/**
 * Created by Sam Vasta, Sep 7, 2014
 * <p>
 * 
 * <p>
 * @author Sam Vasta
 */
public class BasicAOEAttackSystem extends AttackSystem{

	public static final String DEFAULT_AOE_ANIMATION = "bullet_1.png";
	public static final long DEFAULT_AOE_DURATION = 1;
	
	protected String animationName;
	protected long duration;
	
	public BasicAOEAttackSystem(Entity e, Molecule atoms) {
		this(e, atoms, DEFAULT_AOE_ANIMATION, DEFAULT_AOE_DURATION);
	}
	
	public BasicAOEAttackSystem(Entity e, Molecule atoms, String animationName) {
		this(e, atoms, animationName, DEFAULT_AOE_DURATION);
	}
	
	public BasicAOEAttackSystem(Entity e, Molecule atoms, String animationName, long duration) {
		super(e, atoms);
		this.animationName = animationName;
		this.duration = duration;
	}

	@Override
	protected Attack createAttack(float xDest, float yDest) {
		Attack a = new AreaOfEffectAttack(entity, atoms.getDamage(), atoms.getDamageMods(), duration);
		a.addSystem(new TiledTextureDrawer(a, new AnimationHandler(animationName), 100, 100));
		a.addSystem(new AOEAttackPhysics(a, GameWorld.getBoxWorld()));
		a.setPosition(entity.getMover().getPosition());
		return a;
	}

}
