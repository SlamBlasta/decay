package vasta.sam.game.entities.systems.attackers;

import vasta.sam.elements.Molecule;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.attacks.Attack;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;

/**
 * Created by Sam Vasta, Sep 2, 2014<br>
 * @author Sam Vasta
 */
public abstract class AttackSystem extends EntitySystem{
	
	Molecule atoms;
	
	public AttackSystem(Entity e, Molecule atoms) {
		super(e);
		this.atoms = atoms;
	}

	@Override
	public void update(float delta) {
		atoms.update(delta);
	}

	@Override
	public void notify(Notification n) {
		if(n.getType() == NotificationType.ATTACK_TOWARDS_POSITION || n.getType() == NotificationType.ATTACK_TOWARDS_TARGET){
			try{
				float xDest, yDest;
				if(n.getType() == NotificationType.ATTACK_TOWARDS_POSITION){
					xDest = (Float)n.getParams()[0];
					yDest = (Float)n.getParams()[1];
				}
				else if(n.getType() == NotificationType.ATTACK_TOWARDS_TARGET){
					xDest = ((Entity)n.getParams()[0]).getMover().getX();
					yDest = ((Entity)n.getParams()[0]).getMover().getY();
				}
				else{
					xDest = entity.getMover().getX();
					yDest = entity.getMover().getY();
				}
				entity.spawn(createAttack(xDest, yDest), false);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected abstract Attack createAttack(float xDest, float yDest);

}
