package vasta.sam.game.entities.systems;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;


/**
 * Created by Sam Vasta, Aug 5, 2014<br>
 * An Entity System effects the container entity in some way.
 * @author Sam Vasta
 */
public abstract class EntitySystem {
	
	/**
	 * The Entity which contains this system
	 */
	protected Entity entity;
	
	public EntitySystem(Entity e){
		this.entity = e;
	}
	
	/**
	 * Updates the entity
	 * @param delta
	 */
	public abstract void update(float delta);
	
	public void kill(){
		//DO NOTHING
	}
	
	/**
	 * Receives and handles a notification
	 * @param n
	 */
	public abstract void notify(Notification n);

	public Entity getEntity(){
		return entity;
	}
}
