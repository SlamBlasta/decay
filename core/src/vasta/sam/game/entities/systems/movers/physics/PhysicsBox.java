package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Sam Vasta, Aug 6, 2014<br>
 * @author Sam Vasta
 */
public class PhysicsBox extends PhysicsSystem {
	
	public static final float DENSITY = 0.5f,
							  RESTITUTION = 0.5f,
							  DAMPENING = 0.5f;

	
	public PhysicsBox(Entity e, World world){
		super(e, world);
	}
	
	public PhysicsBox(Entity e, World world, float x, float y){
		super(e, world, x, y);
	}
	
	public PhysicsBox(Entity e, World world, Vector2 position){
		super(e, world, position);
	}

	@Override
	protected void createBody(World world, Vector2 position) {
		BodyDef bodyDef = new BodyDef(); 
	    bodyDef.type = BodyType.DynamicBody;
	    bodyDef.position.set(convertToBox(position.x),convertToBox(position.y));
	    bodyDef.angle=0f;
	    bodyDef.fixedRotation = true;
	    body = world.createBody(bodyDef);
	    body.setLinearDamping(DAMPENING);
	    body.setAngularDamping(DAMPENING);
	}

	@Override
	protected void createFixture(float width, float height, Vector2 position) {
		PolygonShape bodyShape = new PolygonShape();
		
		float w=convertToBox(width/2f);
		float h=convertToBox(height/2f);
		bodyShape.setAsBox(w,h);
		
		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.density= DENSITY;
		fixtureDef.restitution= RESTITUTION;
		fixtureDef.shape=bodyShape;
		fixtureDef.isSensor = false;
		body.createFixture(fixtureDef);
		bodyShape.dispose();
	}

	@Override
	public void notify(Notification n) {
		super.notify(n);
	}
	
}
