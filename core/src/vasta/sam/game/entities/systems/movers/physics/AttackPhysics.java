package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.states.GameWorld;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Sam Vasta, Aug 30, 2014
 *<p>
 * Attack Physics ensure that attacks are sensors, and cannot be effected by lights.
 *<p>
 * @author Sam Vasta
 */
public abstract class AttackPhysics extends PhysicsSystem{
	public AttackPhysics(Entity e, World world){
		super(e, world);
	}
	
	public AttackPhysics(Entity e, World world, float x, float y){
		super(e, world, x, y);
	}
	
	public AttackPhysics(Entity e, World world, Vector2 position){
		super(e, world, position);
	}
	
	@Override
	protected void createFixture(float width, float height, Vector2 position) {
		PolygonShape bodyShape = new PolygonShape();
		
		float w=convertToBox(width/2f);
		float h=convertToBox(height/2f);
		bodyShape.setAsBox(w,h);

		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.density= 0f;
		fixtureDef.restitution= 0f;
		fixtureDef.shape=bodyShape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.maskBits = GameWorld.GROUP_INDEX_NO_SHADOW;
		fixtureDef.filter.groupIndex = GameWorld.GROUP_INDEX_NO_SHADOW;
		fixtureDef.filter.categoryBits = GameWorld.GROUP_INDEX_NO_SHADOW;
		body.createFixture(fixtureDef);
		bodyShape.dispose();
	}
}
