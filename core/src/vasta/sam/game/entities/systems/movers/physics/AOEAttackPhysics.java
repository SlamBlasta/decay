package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Created by Sam Vasta, Sep 7, 2014
 * <p>
 * 
 * <p>
 * @author Sam Vasta
 */
public class AOEAttackPhysics extends AttackPhysics{

	public AOEAttackPhysics(Entity e, World world) {
		super(e, world);
	}
	
	public AOEAttackPhysics(Entity e, World world, float x, float y){
		super(e, world, x, y);
	}
	
	public AOEAttackPhysics(Entity e, World world, float x, float y, float dx, float dy){
		super(e, world, x, y);
		body.setLinearVelocity(dx, dy);
	}
	
	public AOEAttackPhysics(Entity e, World world, Vector2 position){
		super(e, world, position);
	}
	
	public AOEAttackPhysics(Entity e, World world, Vector2 position, Vector2 velocity){
		super(e, world, position);
		body.setLinearVelocity(velocity);
	}

	@Override
	protected void createBody(World world, Vector2 position) {
		BodyDef bodyDef = new BodyDef();
	    bodyDef.type = BodyType.KinematicBody;
	    bodyDef.position.set(convertToBox(position.x),convertToBox(position.y));
	    bodyDef.angle=0f;
	    bodyDef.fixedRotation = false;
	    bodyDef.bullet = false;
	    body = world.createBody(bodyDef);
	    body.setLinearDamping(0f);
	    body.setAngularDamping(0f);
	}

}
