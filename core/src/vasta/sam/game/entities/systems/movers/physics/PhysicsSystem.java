package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.movers.MoveSystem;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Sam Vasta, Aug 6, 2014<br>
 * Physics Systems contain information concerning the Box2D environment.
 * @author Sam Vasta
 */
public abstract class PhysicsSystem extends MoveSystem{

	public static final float WORLD_TO_BOX = 0.01f;
	public static final float BOX_TO_WORLD = 100f;
	
	public static float convertToBox(float f){
		return f*WORLD_TO_BOX;
	}
	public static Vector2 convertToBox(Vector2 vec){
		return new Vector2(convertToBox(vec.x), convertToBox(vec.y));
	}
	public static float convertToWorld(float f){
		return f*BOX_TO_WORLD;
	}
	public static Vector2 convertToWorld(Vector2 vec){
		return new Vector2(convertToWorld(vec.x), convertToWorld(vec.y));
	}
	
	/**
	 * The Box2D body assigned to this system.
	 */
	protected Body body;
	
	public PhysicsSystem(Entity e, World world){
		this(e, world, new Vector2(0, 0));
	}
	
	public PhysicsSystem(Entity e, World world, float x, float y){
		this(e, world, new Vector2(x, y));
	}
	
	public PhysicsSystem(Entity e, World world, Vector2 position){
		super(e, position.x, position.y);
		createBody(world, position);
		createFixture(e.getDrawer().getWidth(), e.getDrawer().getHeight(), position);
		setUserData();
	}
	
	public boolean collidesWith(Vector2 pos){
		return body.getFixtureList().get(0).testPoint(convertToBox(pos));
	}
	
	@Override
	public void update(float delta) {
		dx = x-convertToWorld(body.getPosition().x);
		dy = y-convertToWorld(body.getPosition().y);
		x = convertToWorld(body.getPosition().x);
		y = convertToWorld(body.getPosition().y);
		theta = (float)Math.toDegrees(body.getAngle());
		
	}
	
	private void setUserData(){
		body.setUserData(this);
		for(Fixture f: body.getFixtureList()){
			f.setUserData(this);
		}
	}
	
	@Override
	public void kill(){
		for(Fixture f: body.getFixtureList()){
			body.destroyFixture(f);
		}
		body.getWorld().destroyBody(body);
	}
	
	@Override
	public void setPosition(Vector2 pos){
		body.setTransform(convertToBox(pos), body.getAngle());
		x = pos.x;
		y = pos.y;
	}
	
	@Override
	public void setVelocity(Vector2 vel){
		body.setLinearVelocity(convertToBox(vel));
		dx = vel.x;
		dy = vel.y;
	}
	
	@Override
	public Vector2 getPosition(){
		return convertToWorld(body.getPosition());
	}
	
	@Override
	public Vector2 getVelocity(){
		return convertToWorld(body.getLinearVelocity());
//		return convertToWorld(body.getLinearVelocity());
	}
	
	/**
	 * Returns position directly from the body. Does not convert to World from Box.
	 * @return position - position of the body in BoxWorld values.
	 */
	public Vector2 getBoxPosition(){
		return body.getPosition();
	}
	/**
	 * Returns velocity directly from the body. Does not convert to World from box.
	 * @return velocity - velocity of the body in BoxWorld values
	 */
	public Vector2 getBoxVelocity(){
		return body.getLinearVelocity();
	}
	
	/**
	 * Sets the position directly. Does not convert from world to box.
	 * @param position - Position in BoxWorld values
	 */
	public void setBoxPosition(Vector2 position){
		body.setTransform(position,  body.getAngle());
		x = convertToWorld(position.x);
		y = convertToWorld(position.y);
	}
	
	/**
	 * Sets the velocity directly. Does not convert from world to box.
	 * @param vel - Velocity in BoxWorld values.
	 */
	public void setBoxVelocity(Vector2 vel){
		body.setLinearVelocity(vel);
		dx = convertToWorld(vel.x);
		dy = convertToWorld(vel.y);
		
	}
	
	public void setRotation(float theta){
		body.setTransform(body.getPosition(), theta);
	}
	
	/**
	 * awakens or puts to sleep the Box2D body.
	 * @param active
	 */
	public void setAwake(boolean awake){
		body.setAwake(awake);
	}
	
	/**
	 * Activates or deactivates the Box2D Body.
	 * @param active
	 */
	public void setActive(boolean active){
		body.setActive(active);
	}
	
	/**
	 * Creates a body to represent the Entity
	 * @param world - Box2D world the body will exist in
	 * @param position - The starting position of the body in the world
	 */
	protected abstract void createBody(World world, Vector2 position);
	/**
	 * Attatches a Box2D fixture to the body
	 * @param width - width of the fixture
	 * @param height - height of the fixture
	 * @param position - position of the fixture
	 */
	protected abstract void createFixture(float width, float height, Vector2 position);

}
