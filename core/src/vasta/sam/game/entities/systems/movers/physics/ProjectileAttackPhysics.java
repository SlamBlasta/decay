package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Created by Sam Vasta, Aug 30, 2014
 *<p>
 * This extension of {@link AttackPhysics} ensures that the body of an 
 * {@link AttackPhysics} object is a {@link com.badlogic.gdx.physics.box2d.BodyDef.BodyType} Kineamatic body.
 * This is to make sure that this attack does not move any other entities.
 *<p>
 * @author Sam Vasta
 */
public class ProjectileAttackPhysics extends AttackPhysics{

	public ProjectileAttackPhysics(Entity e, World world){
		super(e, world);
	}
	
	public ProjectileAttackPhysics(Entity e, World world, float x, float y){
		super(e, world, x, y);
	}
	
	public ProjectileAttackPhysics(Entity e, World world, float x, float y, float dx, float dy){
		super(e, world, x, y);
		body.setLinearVelocity(dx, dy);
	}
	
	public ProjectileAttackPhysics(Entity e, World world, Vector2 position){
		super(e, world, position);
	}
	
	public ProjectileAttackPhysics(Entity e, World world, Vector2 position, Vector2 velocity){
		super(e, world, position);
		body.setLinearVelocity(velocity);
	}
	
	@Override
	protected void createBody(World world, Vector2 position) {
		BodyDef bodyDef = new BodyDef(); 
	    bodyDef.type = BodyType.KinematicBody;
	    bodyDef.position.set(convertToBox(position.x),convertToBox(position.y));
	    bodyDef.angle=0f;
	    bodyDef.fixedRotation = false;
	    body = world.createBody(bodyDef);
	    body.setLinearDamping(0f);
	    body.setAngularDamping(0f);
	}
	
}
