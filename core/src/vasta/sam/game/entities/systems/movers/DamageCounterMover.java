package vasta.sam.game.entities.systems.movers;

import com.badlogic.gdx.math.Vector2;

import vasta.sam.game.entities.Entity;
import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Aug 29, 2014
 *<p>
 *
 *<p>
 * @author Sam Vasta
 */
public class DamageCounterMover extends MoveSystem{

	public static final long DURATION = 500;
	
	private long startTime;
	public DamageCounterMover(Entity e) {
		super(e);
		startTime = System.currentTimeMillis();
		this.setVelocity(new Vector2(0, 1f + (float)MathHelper.random()));
	}

	@Override
	public void update(float delta){
		super.update(delta);
		if(System.currentTimeMillis() - startTime >= DURATION){
			entity.kill();
		}
	}
	
}
