package vasta.sam.game.entities.systems.movers;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Aug 5, 2014<br>
 * @author Sam Vasta
 */
public abstract class MoveSystem extends EntitySystem{
	/**
	 * Position, Velocity, Acceleration in 2d space
	 */
	protected float x, y, dx, dy, d2x, d2y, theta;
	
	public MoveSystem(Entity e){
		this(e, 0, 0);
	}
	
	public MoveSystem(Entity e, float x, float y){
		super(e);
		this.x = x;
		this.y = y;
		dx = 0;
		dy = 0;
		d2x = 0;
		d2y = 0;
	}
	
	@Override
	public void update(float delta){
		dx += d2x;
		dy += d2y;
		x += dx;
		y += dy;
	}

	@Override
	public void notify(Notification n){
		switch(n.getType()){
		case SET_POSITION:
			this.setPosition(new Vector2((Float)n.getParams()[0], (Float)n.getParams()[1]));
			break;
		case ADD_POSITION:
			this.setPosition(new Vector2(getPosition().x + (Float)n.getParams()[0], getPosition().y + (Float)n.getParams()[1]));
			break;
			
		case SET_VELOCITY:
			this.setVelocity(new Vector2((Float)n.getParams()[0], (Float)n.getParams()[1]));
			break;
		case ADD_VELOCITY:
			this.setVelocity(new Vector2(getVelocity().x + (Float)n.getParams()[0], getVelocity().y + (Float)n.getParams()[1]));
			break;
			
		case SET_ACC:
			this.setAcceleration(new Vector2((Float)n.getParams()[0], (Float)n.getParams()[1]));
			break;
			
		case ADD_ACC:
			this.setAcceleration(new Vector2(getAcceleration().x + (Float)n.getParams()[0], getAcceleration().y + (Float)n.getParams()[1]));
			break;
			
		case SET_ROTATION:
			this.setRotation((Float)n.getParams()[0]);
			break;
			
		case ADD_ROTATION:
			this.setRotation(getRotation() + (Float)n.getParams()[0]);
			break;
		default:break;
		}
	}
	
	//GETTERS & SETTERS
	
	public Vector2 getPosition(){
		return new Vector2(x, y);
	}
	
	public void setPosition(Vector2 newPos){
		x = newPos.x;
		y = newPos.y;
	}
	
	public Vector2 getVelocity(){
		return new Vector2(dx, dy);
	}
	
	public void setVelocity(Vector2 vel){
		dx = vel.x;
		dy = vel.y;
	}
	
	public void setAcceleration(Vector2 acc){
		d2x = acc.x;
		d2y = acc.y;
	}
	
	public Vector2 getAcceleration(){
		return new Vector2(d2x, d2y);
	}
	
	public void setRotation(float theta){
		this.theta = theta;
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getDx() {
		return dx;
	}

	public float getDy() {
		return dy;
	}

	public float getD2x() {
		return d2x;
	}

	public float getD2y() {
		return d2y;
	}

	public float getRotation(){
		return theta;
	}
	
}
