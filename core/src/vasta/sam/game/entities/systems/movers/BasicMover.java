package vasta.sam.game.entities.systems.movers;

import vasta.sam.DecayGame;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Aug 5, 2014<br>
 * @author Sam Vasta
 */
public class BasicMover extends MoveSystem{

	
	public BasicMover(Entity e){
		super(e, MathHelper.nextInt(DecayGame.GAME_WIDTH - 400) + 200, MathHelper.nextInt(DecayGame.GAME_HEIGHT - 400) + 200);
	}
	
	
	@Override
	public void update(float delta) {
		super.update(delta);
	}


	@Override
	public void notify(Notification n) {
		System.err.println("TODO: " + this.getClass().getName() + ".notify(Notification)");
	}

}
