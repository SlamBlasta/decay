package vasta.sam.game.entities.systems.movers.physics;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.NotificationType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Sam Vasta, Aug 10, 2014<br>
 * @author Sam Vasta
 */
public class PlayerController extends PhysicsBox{

	public PlayerController(Entity e, World world, Vector2 position) {
		super(e, world, position);
	}
	
	@Override
	public void update(float delta){
		if(Gdx.input.isKeyPressed(Input.Keys.A)){
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, -5f, getVelocity().y));
		}
		else if(Gdx.input.isKeyPressed(Input.Keys.D)){
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, 5f, getVelocity().y));
		}
		else
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, 0f, getVelocity().y));
		
		
		if(Gdx.input.isKeyPressed(Input.Keys.S)){
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, getVelocity().x, -5f));
		}
		else if(Gdx.input.isKeyPressed(Input.Keys.W)){
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, getVelocity().x, 5f));
		}
		else 
			entity.broadcast(new Notification(NotificationType.SET_VELOCITY, getVelocity().x, 0f));

		super.update(delta);
	}

}