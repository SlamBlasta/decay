package vasta.sam.game.entities.systems.health;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Aug 29, 2014
 *<p>
 *
 *<p>
 * @author Sam Vasta
 */
public class AutoKillSystem extends HealthSystem{

	private float acc = 0;
	
	public AutoKillSystem(Entity e) {
		super(e, 50);
	}

	@Override
	public void update(float delta){
		acc += delta;
		if(acc > 0.5f){
			acc -= 0.5f;
			entity.broadcast(new Notification(NotificationType.DAMAGE, MathHelper.nextInt(1, 11), new float[]{0,0,0,0,0,0}));
		}
	}
}
