package vasta.sam.game.entities.systems.health;

import java.util.Arrays;

import vasta.sam.elements.Element;
import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.EntityFactory;
import vasta.sam.game.entities.Notification;
import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;
import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Aug 28, 2014
 *<p>
 * A HealthSystem is used to keep track of the kill status of an {@link Entity}.
 * HealthSystems will automatically set the kill flag to true if the currentHealth
 * falls to or below 0
 *<p>
 * @author Sam Vasta
 */
public class HealthSystem extends EntitySystem{

	public static final int DEFAULT_HEALTH = 1;
	protected int currentHealth, maxHealth, tempHealth = 0;
	
	/**
	 * Protection from physical damage. Physical damage is reduced by this amount
	 */
	protected float armor;
	
	/**
	 * Multipliers for damage types. Each damage type is multiplied by its respective value,
	 * then applied to the health system.
	 */
	protected float damageModifiers[];
	
	public HealthSystem(Entity e) {
		this(e, DEFAULT_HEALTH);
	}
	
	public HealthSystem(Entity e, int maxHealth){
		super(e);
		this.maxHealth = maxHealth;
		this.currentHealth = maxHealth;
		damageModifiers = new float[Element.NUM_DAMAGE_TYPES];
		Arrays.fill(damageModifiers, 1);
	}

	public HealthSystem(Entity e, int maxHealth, float[] damageModifiers){
		this(e, maxHealth, damageModifiers, 0);
	}
	
	public HealthSystem(Entity e, int maxHealth, float[] damageModifiers, float armor){
		super(e);
		this.maxHealth = maxHealth;
		this.currentHealth = maxHealth;
		this.damageModifiers = damageModifiers;
		this.armor = armor;
	}
	
	@Override
	public void update(float delta) {
		// Do nothing
	}

	@Override
	public void notify(Notification n) {
		if(n.getType() == NotificationType.DAMAGE){
			
			try{
				int damage = (Integer)n.getParams()[0];
				float[] damageMods = (float[])n.getParams()[1];
				float totalDamage = damage - armor;
				if(totalDamage < 0) totalDamage = 0;
				for(int i = Math.min(damageMods.length, damageModifiers.length)-1; i >= 0; i--){
					totalDamage += damageModifiers[i] * damageMods[i] * damage;
				}
				
				totalDamage = Math.round(totalDamage);
				
				if(totalDamage == 0){
					return;
				}
				
				Entity e = EntityFactory.getDamageCounter((int)totalDamage, Element.getColor(damageMods));
				e.setPosition(entity.getMover().getPosition().add(MathHelper.nextInt((int)-entity.getWidth()/3, (int)entity.getWidth()/5), MathHelper.nextInt((int)entity.getHeight()/2, (int)entity.getHeight()*2/3)));
//				e.setPosition(entity.getMover().getPosition());
				entity.spawn(e);
				
				if(tempHealth > 0){
					if(totalDamage > tempHealth){
						totalDamage -= tempHealth;
						tempHealth = 0;
					}
					else{
						tempHealth -= totalDamage;
						totalDamage = 0;
					}
				}
				else{
					currentHealth -= totalDamage;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(currentHealth > maxHealth){
				currentHealth = maxHealth;
			}
			if(currentHealth <= 0){
				entity.kill();
			}
		}
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public int getTempHealth() {
		return tempHealth;
	}

	public void setTempHealth(int tempHealth) {
		this.tempHealth = tempHealth;
	}

	public float getArmor() {
		return armor;
	}

	public void setArmor(float armor) {
		this.armor = armor;
	}

	public float[] getDamageModifiers() {
		return damageModifiers;
	}

	public void setDamageModifiers(float[] damageModifiers) {
		this.damageModifiers = damageModifiers;
	}
	
}
