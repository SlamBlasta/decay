package vasta.sam.game.entities.systems.health;

import vasta.sam.game.entities.Entity;

/**
 * Created by Sam Vasta, Aug 30, 2014
 *<p>
 *
 *<p>
 * @author Sam Vasta
 */
public class PlayerHealthSystem extends HealthSystem {

	public PlayerHealthSystem(Entity e) {
		super(e, 100, new float[]{0f, 0f, 0f, 0f, 0f, 0f});
	}
	
	public void update(float delta){
		currentHealth = maxHealth;
	}
}
