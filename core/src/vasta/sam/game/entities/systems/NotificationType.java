package vasta.sam.game.entities.systems;

/**
 * Created by Sam Vasta, Sep 4, 2014
 * <p>
 * This Enum keeps track of the minimum number of arguments needed
 * for each type of notification.
 * <p>
 * @author Sam Vasta
 */
public enum NotificationType {
	
	INVALID_ARGS(0),
	SET_POSITION(2),
	ADD_POSITION(2),
	SET_VELOCITY(2),
	ADD_VELOCITY(2),
	SET_ACC(2),
	ADD_ACC(2),
	SET_ROTATION(1),
	ADD_ROTATION(1),
	ANIMATION_STATE_SWITCH(1),
	AI_STATE_SWITCH(1),
	DAMAGE(2),
	ATTACK_TOWARDS_POSITION(2),
	ATTACK_TOWARDS_TARGET(1);
	
	private int args;
	private NotificationType(int args){
		this.args = args;
	}
	public int getArgs(){return args;}
}