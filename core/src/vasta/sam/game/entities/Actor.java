package vasta.sam.game.entities;

import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.movers.physics.PhysicsSystem;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Aug 6, 2014
 * <p>
 * Actors are Entities which can be interacted with in the game world.
 * Because all actors exist in the Game World, all actors have Physics
 * systems in place of movement systems.
 * <p>
 * An Actor only differs from an Entity in that it ensures the Movement
 * System is a Physics System.
 * @author Sam Vasta
 */
public class Actor extends Entity{
	
	public Actor(EntityType type){
		super(type);
	}
	
	@Override
	public void addSystem(EntitySystem sys){
		if(sys instanceof PhysicsSystem){
			if(mover != null) systems.remove(mover);
			this.mover = (PhysicsSystem)sys;
		}
		if(sys instanceof DrawSystem){
			if(drawer != null) systems.remove(drawer);
			this.drawer = (DrawSystem)sys;
		}
		systems.add(sys);
	}
	
	public boolean collidesWith(float x, float y){
		return collidesWith(new Vector2(x, y));
	}
	
	public boolean collidesWith(Vector2 point){
		return getPhysics().collidesWith(point);
	}
	
	@Override
	public void kill(){
		super.kill();
	}
	
	@Override
	public void setActive(boolean active){
		super.setActive(active);
		((PhysicsSystem)mover).setActive(active);
	}
	
	public PhysicsSystem getPhysics(){
		return (PhysicsSystem)mover;
	}
	
}
