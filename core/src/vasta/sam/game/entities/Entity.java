package vasta.sam.game.entities;

import java.util.ArrayList;

import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.movers.MoveSystem;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Sam Vasta, Jul 15, 2014<br>
 * Entities are actors inside of a stage. All actors have movements and drawing systems.
 * @author Sam Vasta
 */
public class Entity extends Actor{
	/**
	 * The ID assigned to the last entity created.
	 */
	private static int LAST_ID = 1;
	/**
	 * Adds 1 to LAST_ID, then returns LAST_ID
	 * @return LAST_ID+1
	 */
	private static int getUniqueID(){return ++LAST_ID;}
	
	
	/**
	 * The unique ID of this entity.
	 */
	protected int id;
	
	/**
	 * The system which specifically handles the movement of this entity
	 */
	protected MoveSystem mover;
	/**
	 * The system which specifically handles rendering of this entity
	 */
	protected DrawSystem drawer;
	/**
	 * A list of systems which control this entity
	 */
	protected ArrayList<EntitySystem> systems = new ArrayList<EntitySystem>();
	/**
	 * Notes if this entity should act.
	 */
	protected boolean active;
	/**
	 * Notes if this entity should be drawn.
	 */
	protected boolean visible;
	/**
	 * Notes if this entity is dead.
	 */
	protected boolean isDead;
	
	/**
	 * Signifies what type of entity this is.
	 */
	protected EntityType type;
	
	public Entity(EntityType type){
		this.type = type;
		id = getUniqueID();
		active = true;
		visible = true;
		isDead = false;
	}
	
	/**
	 * Setter for 'active'
	 * @param active
	 */
	public void setActive(boolean active){
		this.active = active;
	}
	
	/**
	 * Getter for 'active'
	 * @return active
	 */
	public boolean isActive(){
		return active;
	}
	
	@Override
	public void act(float delta){
		//Removes this actor from the stage if it is dead.
		if(isDead) remove();
		
		//Does nothing if there is no mover, drawer, or is inactive.
		if(mover == null || drawer == null || !active) return;
		
		super.act(delta);
		
		for(EntitySystem sys: systems){
			sys.update(delta);
		}
		
		this.setX(mover.getX());
		this.setY(mover.getY());
		this.setWidth(drawer.getWidth());
		this.setHeight(drawer.getHeight());
		this.setRotation(mover.getRotation());
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha){
		if(visible)
			drawer.draw(batch, getX() - getWidth()/2, getY() - getHeight()/2, getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1f, 1f, getRotation());
	}
	
	/**
	 * Creates an entity in this Entity's current stage.
	 * @param e
	 */
	public void spawn(Entity e){
		getStage().addActor(e);
	}
	
	/**
	 * Creates an entity in this Entity's current stage.
	 * @param e
	 */
	public void spawn(Entity e, boolean onTop){
		getStage().addActor(e);
	}
	
	/**
	 * Notifies all systems of the notification
	 * @param n
	 */
	public void broadcast(Notification n){
		for(EntitySystem s: systems){
			s.notify(n);
		}
	}
	
	/**
	 * Adds sys to the list of systems which control this entity
	 * @param sys - the system to be added
	 */
	public void addSystem(EntitySystem sys){
		if(sys instanceof MoveSystem){
			if(mover != null) systems.remove(mover);
			this.mover = (MoveSystem)sys;
		}
		if(sys instanceof DrawSystem){
			if(drawer != null) systems.remove(drawer);
			this.drawer = (DrawSystem)sys;
		}
		systems.add(sys);
	}
	
	@Override
	public void setPosition(float x, float y){
		super.setPosition(x, y);
		mover.setPosition(new Vector2(x, y));
	}
	
	public void setPosition(Vector2 pos){
		setPosition(pos.x, pos.y);
	}
	
	/**
	 * Sets rotation to angle
	 * @param angle
	 */
	public void pointTowards(float angle){
		this.mover.setRotation(angle);
	}
	
	/**
	 * Calls setActive(false) and setVisible(false), and sets the dead flag to true.
	 */
	public void kill(){
		//You can only kill something once...
		if(isDead) return;
		
		setActive(false);
		setVisible(false);
		disposeSystems();
		isDead = true;
	}
	
	/**
	 * kills then removes all EntitySystems
	 */
	private void disposeSystems(){
		EntitySystem[] sys = systems.toArray(new EntitySystem[]{});
		for(EntitySystem s: sys){
			s.kill();
		}
		
	}
	
	@Override
	public String toString(){
		return new String("ID: " + id + ", Type: " + this.getClass().getName() + ", Position: [" + mover.getX() + ", " + mover.getY() + "]");
	}

	public boolean equals(Entity e){
		return e.getID() == this.getID();
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof Entity){
			return equals((Entity)obj);
		}
		else
			return false;
	}
	
	//GETTERS & SETTERS
	
	public int getID(){
		return id;
	}
	
	public void setVisible(boolean visible){
		this.visible = visible;
	}
	
	public MoveSystem getMover() {
		return mover;
	}

	public DrawSystem getDrawer() {
		return drawer;
	}
	
	public boolean isDead(){
		return isDead;
	}
	
	public EntityType getType(){
		return type;
	}
}
