package vasta.sam.game.entities;

import java.util.ArrayList;

import vasta.sam.elements.Atom;
import vasta.sam.elements.Molecule;
import vasta.sam.elements.PeriodicTable;
import vasta.sam.game.entities.systems.attackers.BasicAOEAttackSystem;
import vasta.sam.game.entities.systems.drawers.DamageDrawer;
import vasta.sam.game.entities.systems.drawers.DrawSystem;
import vasta.sam.game.entities.systems.health.HealthSystem;
import vasta.sam.game.entities.systems.health.PlayerHealthSystem;
import vasta.sam.game.entities.systems.movementControllers.MouseFollower;
import vasta.sam.game.entities.systems.movementControllers.MovementControlSystem;
import vasta.sam.game.entities.systems.movers.BasicMover;
import vasta.sam.game.entities.systems.movers.DamageCounterMover;
import vasta.sam.game.entities.systems.movers.physics.PhysicsBox;
import vasta.sam.game.entities.systems.movers.physics.PlayerController;
import vasta.sam.game.states.GameWorld;
import vasta.sam.util.AnimationHandler;
import vasta.sam.util.MathHelper;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Aug 6, 2014<br>
 * A Factory Class which creates different types of Entities and Actors
 * @author Sam Vasta
 */
public class EntityFactory {
	
	static PeriodicTable table = new PeriodicTable(false);
	
	public static final int TEST_ENTITY = 0;
	public static final int TEST_ACTOR = 1;
	
	public static Entity getTestEntity(){
		Entity e = new Entity(EntityType.ACTOR);
		e.addSystem(new BasicMover(e));
		e.addSystem(new DrawSystem(e, new AnimationHandler("playerWalkUp_1.png")));
		return e;
	}
	
	public static Actor getTestActor(GameWorld world){
		Actor a = new Actor(EntityType.ACTOR);
		a.addSystem(new DrawSystem(a, new AnimationHandler("playerWalkUp_1.png")));
		a.addSystem(new PhysicsBox(a, world.getBoxWorld(), MathHelper.nextInt(400) + 200, MathHelper.nextInt(200) + 200));
		a.addSystem(new HealthSystem(a, 1));
		a.addSystem(new MovementControlSystem(a, new Vector2(MathHelper.nextInt(-1000, 1000), MathHelper.nextInt(-1000, 1000))));
		return a;
	}
	
	public static Entity getDamageCounter(int damage, Color col){
		Entity e = new Entity(EntityType.PARTICLE);
		e.addSystem(new DamageDrawer(e, damage, col));
		e.addSystem(new DamageCounterMover(e));
		return e;
	}
	
	public static Actor getPlayer(GameWorld world){
		Actor a = new Actor(EntityType.PLAYER);
		a.addSystem(new DrawSystem(a, new AnimationHandler("default.png")));
		a.addSystem(new PlayerController(a, world.getBoxWorld(), new Vector2(0, 0)));
		a.addSystem(new PlayerHealthSystem(a));
		ArrayList<Atom> atoms = new ArrayList<Atom>();
		for(int i = 0; i < 500; i++){
			atoms.add(new Atom(table.getRandomElement(true)));
		}
		Molecule m = new Molecule(atoms);
		a.addSystem(new BasicAOEAttackSystem(a, m));
		a.addSystem(new MouseFollower(a));
		return a;
	}
	
	//Private constructor to disallow object instances
	private EntityFactory(){}

}
