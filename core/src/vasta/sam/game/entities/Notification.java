package vasta.sam.game.entities;

import vasta.sam.game.entities.systems.EntitySystem;
import vasta.sam.game.entities.systems.NotificationType;

/**
 * Notification contains information to convey to an {@link EntitySystem}.
 * Notifications have two fields:<br>
 * <ol>
 * <li>
 * type: The general type of message. ie. "Movement." This is an enum from {@link NotificationType}
 * </li>
 * <li>
 * params: A list of arguments & information for the
 * {@link EntitySystem} to handle.
 * </li>
 * </ol>
 * <p>
 * Created by Sam Vasta, Aug 20, 2014
 * <br>
 * @author Sam Vasta
 */
public class Notification {
	
	/**
	 * Notes the type of notification.
	 * This is usually a general category (i.e. "Movement")
	 */
	private NotificationType type;
	/**
	 * Extra information about the notification.
	 * Example: coordinates for movement
	 */
	private Object[] params;
	
	public Notification(NotificationType type, Object...params){
		this.type = type;
		this.params = params;

		try{
			if(params.length < type.getArgs()){
				System.err.println("An invalid number of arguments was given to a Notification of type: " + type + "!!"
						+ "\n\tChanging type to 'INVALID_ARGS'!");
				
			}
		}catch(ArrayIndexOutOfBoundsException e){
			System.err.println("The Notification type " + type + " is not valid!");
		}
	}
	
	public NotificationType getType(){
		return type;
	}
	
	public Object[] getParams(){
		return params;
	}

}
