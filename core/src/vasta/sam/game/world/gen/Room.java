package vasta.sam.game.world.gen;

import java.util.LinkedList;
import java.util.List;

import vasta.sam.game.states.GameWorld;
import vasta.sam.game.world.RoomMapRenderer;
import vasta.sam.util.MathHelper;
import vasta.sam.util.world.TileSetManager;
import static vasta.sam.util.world.TileSetManager.TILE_SIZE;
import vasta.sam.util.world.WallPhysicsBox;
import box2dLight.Light;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

/**
 * Rooms represent a chunk of the overall world. Rooms contain an integer array which holds the tile IDs
 * of the room.
 * @author Sam
 *
 */
public class Room {
	
	public static final int ROOM_SIZE = 32;
	public static final int LIGHTS_PER_ROOM = 30;

	/**
	 * A list of all box2d bodies for the walls.
	 */
	protected WallPhysicsBox[] tiles = new WallPhysicsBox[ROOM_SIZE*ROOM_SIZE];
	protected GameWorld world;
	/**
	 * A flag which determines if this room will be rendered.
	 */
	protected boolean isLoaded = false;
	/**
	 * The data structure which holds information for the 
	 * layers of tiles.
	 */
	protected TiledMap map;
	/**
	 * The device which renders this room's tiles.
	 */
	protected TiledMapRenderer renderer;
	/**
	 * Information such as type of room, and connection availabilities.
	 * This information is used when creating rooms adjacent to this one.
	 */
	protected byte generationInfo;
	
	/**
	 * A flag to determine if the room has ever been loaded
	 */
	protected boolean hasLoaded = false;
	
	/**
	 * X and Y position in Room map
	 */
	protected int x, y;
	
	public Room(int x, int y, GameWorld world, byte generationInfo){
		this.x = x;
		this.y = y;
		this.world = world;
		map = new TiledMap();
		renderer = new RoomMapRenderer(map, x*ROOM_SIZE*TILE_SIZE, y*ROOM_SIZE*TILE_SIZE);
		
		for(int i = 0; i < tiles.length; i++){
			tiles[i] = new WallPhysicsBox(world.getBoxWorld(), new Vector2(x*ROOM_SIZE*TILE_SIZE + (i%ROOM_SIZE)*TILE_SIZE, y*ROOM_SIZE*TILE_SIZE + (i/ROOM_SIZE)*TILE_SIZE), new Vector2(TILE_SIZE, TILE_SIZE));
			tiles[i].unload();
		}
		isLoaded = false;
		this.generationInfo = generationInfo;
	}
	
	public Room(int x, int y, GameWorld world, short roomType, boolean connectsNorth, boolean connectsEast, boolean connectsSouth, boolean connectsWest){
		this(x, y, world, convertToByte(roomType, connectsNorth, connectsEast, connectsSouth, connectsWest));
	}
	
	public void render(){
		renderer.setView(world.getCamera());
		renderer.render();
	}
	
	/**
	 * sets the tile ID at the specified location
	 * @param xOffset - the x position in this room of the tile to set
	 * @param yOffset - the y position in this room of the tile to set
	 * @param tileID - the ID of the new tile
	 * @return returns true if the tile was set, false otherwise.
	 */
	public boolean setTile(int xOffset, int yOffset, int layerNum, TiledMapTile tile, boolean isWall){
//		tile.setOffsetX(-x*ROOM_SIZE*TileSetManager.TILE_SIZE + xOffset*TileSetManager.TILE_SIZE);
//		tile.setOffsetY(-y*ROOM_SIZE*TileSetManager.TILE_SIZE + yOffset*TileSetManager.TILE_SIZE);
//		tile.setOffsetX(-500);
		tile.setOffsetX(-TileSetManager.TILE_SIZE/2);
		tile.setOffsetY(-TileSetManager.TILE_SIZE/2);
		try{
			map.getLayers().get(layerNum);
		}catch(Exception e){
			TiledMapTileLayer layer = new TiledMapTileLayer(ROOM_SIZE, ROOM_SIZE, TileSetManager.TILE_SIZE, TileSetManager.TILE_SIZE);
			map.getLayers().add(layer);
		}
		Cell cell = new Cell();
		cell.setTile(tile);
		((TiledMapTileLayer)map.getLayers().get(layerNum)).setCell(xOffset, yOffset, cell);
		
		if(!isWall){
			tiles[xOffset + yOffset*ROOM_SIZE].unload();
		}
		else if(isWall){
			tiles[xOffset + yOffset*ROOM_SIZE].load();
		}
		
		return true;
	}
	
	/**
	 * Returns the tile on the given layer at the given coordinates.
	 * @param xOffset
	 * @param yOffset
	 * @param layerName
	 * @return
	 */
	public TiledMapTile getTile(int xOffset, int yOffset, String layerName){
		return ((TiledMapTileLayer)map.getLayers().get(layerName)).getCell(xOffset, yOffset).getTile();
	}
	
	/**
	 * Awaken all box2d bodies in room so that they
	 * will be included in physics calculations
	 */
	public void load(){
		if(isLoaded) return;
		for(WallPhysicsBox box: tiles){
			if(box != null)
				box.load();
		}
		if(!hasLoaded){
			for(int i = 0; i < LIGHTS_PER_ROOM; i++){
				int lx = x*ROOM_SIZE*TILE_SIZE, ly = y*ROOM_SIZE*TILE_SIZE;
				lx += (int)(Math.random()*(ROOM_SIZE*TILE_SIZE));
				ly += (int)(Math.random()*(ROOM_SIZE*TILE_SIZE));
				System.out.println("Light at: " + lx + ", " + ly);
				world.addLight(GameWorld.POINT_LIGHT, new Color((float)Math.random(), (float)Math.random(), (float)Math.random(), 1f), MathHelper.nextInt(200, 2000), lx, ly, 0, 0, 0, false, false);
			}
			hasLoaded = true;
		}
		isLoaded = true;
	}
	
	/**
	 * Put all box2d bodies to sleep so that their
	 * bodies are not included in physics calculations.
	 * This will save CPU time.
	 */
	public void unload(){
		if(!isLoaded) return;
		for(WallPhysicsBox box: tiles){
			if(box != null)
				box.unload();
		}
		isLoaded = false;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	/**
	 * Takes room generation data and converts it into a byte by manipulating its bits.
	 * @param roomType
	 * @param north
	 * @param east
	 * @param south
	 * @param west
	 * @return
	 */
	private static byte convertToByte(short roomType, boolean north, boolean east, boolean south, boolean west){
		byte b = 0;
		b += roomType<<4;
		if(north)b += 8;
		if(east) b+=4;
		if(south)b+=2;
		if(west) b+=1;
		
		return b;
		
	}
}
