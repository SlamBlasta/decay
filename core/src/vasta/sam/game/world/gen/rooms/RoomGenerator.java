package vasta.sam.game.world.gen.rooms;

import java.util.Arrays;

import vasta.sam.game.states.GameWorld;
import vasta.sam.game.world.gen.Room;
import vasta.sam.util.world.PatternMatcher;
import vasta.sam.util.world.TilePattern;
import vasta.sam.util.world.TileSetManager;

import com.badlogic.gdx.maps.tiled.TiledMapTile;

/**
 * Room Generator is an abstract class which provides a structure for generating rooms. 
 * Subclasses are expected to override the addHallways() and generateRoom() methods, as 
 * this class handles everything else.
 * @author Sam
 *
 */
public abstract class RoomGenerator {
	
	public static final PatternMatcher matcher = new PatternMatcher();
	
	/**
	 * creates hallways to the room by modifying the provided integer array 
	 * such that floor tiles are placed.
	 * @param tiles
	 * @param layerNames
	 */
	protected abstract void addHallways(int[] layerNums, int[] tiles);
	
	/**
	 * creates the general space of the room by modifying the provided integer 
	 * array such that wall tiles are placed.
	 * @param tiles
	 * @param layerNames
	 */
	protected abstract void generateRoom(int[] layerNums, int[] tiles);
	/**
	 * Helper method for setting a tile into the arrays.
	 * @param stringArr - the array of tileSetNames
	 * @param intArr - The array of tileIds
	 * @param tileX - the x position (in the room) of the tile to be set
	 * @param tileY - the y position (in the room) of the tile to be set
	 * @param layerName - the name of the tileSet to assign to the tile
	 * @param tileId - the id to assign to the tile
	 */
	protected void setTile(String[] stringArr, int[] intArr, int tileX, int tileY, String layerName, int tileId){
		stringArr[tileX + tileY*Room.ROOM_SIZE] = layerName;
		intArr[tileX + tileY*Room.ROOM_SIZE] = tileId;
	}
	
	protected abstract TiledMapTile getFloorTile();
	protected abstract TiledMapTile getWallTile(int tileId);
	
	public Room generate(int x, int y, GameWorld world){
		int[] layerNums = new int[Room.ROOM_SIZE * Room.ROOM_SIZE];
		int[] tiles = new int[Room.ROOM_SIZE * Room.ROOM_SIZE];
		Arrays.fill(tiles, TileSetManager.floor);
		Room r = new Room(x, y, world, (byte) 120);
		generateRoom(layerNums, tiles);
		addHallways(layerNums, tiles);
		
		convertArrayToTiles(layerNums, tiles, r);
		return r;
	}
	
	/**
	 * manipulates the tileIDs based on neighboring tiles to give a smoother
	 * look to the room.
	 * @param layerNames
	 * @param tiles
	 * @param r
	 */
	private void convertArrayToTiles(int[] layerNums, int[] tiles, Room r){
		for(int i = 1; i < Room.ROOM_SIZE-1; i++){
			for(int j = 1; j < Room.ROOM_SIZE-1; j++){
				if(TileSetManager.isWall(tiles[i + j*Room.ROOM_SIZE])){
					char[] neighbors = new char[]{
						getChar(tiles[(i-1) + (j-1)*Room.ROOM_SIZE]),
						getChar(tiles[(i) + (j-1)*Room.ROOM_SIZE]),
						getChar(tiles[(i+1) + (j-1)*Room.ROOM_SIZE]),
						getChar(tiles[(i-1) + (j)*Room.ROOM_SIZE]),
						getChar(tiles[(i) + (j)*Room.ROOM_SIZE]),
						getChar(tiles[(i+1) + (j)*Room.ROOM_SIZE]),
						getChar(tiles[(i-1) + (j+1)*Room.ROOM_SIZE]),
						getChar(tiles[(i) + (j+1)*Room.ROOM_SIZE]),
						getChar(tiles[(i+1) + (j+1)*Room.ROOM_SIZE]),
					};
					int tileId = matcher.getTile(neighbors);
					r.setTile(i, j, layerNums[i + j*Room.ROOM_SIZE], getWallTile(tileId), true);
				}
				else{
					r.setTile(i, j, layerNums[i + j*Room.ROOM_SIZE], getFloorTile(), false);
				}
			}
		}
	}

	/**
	 * returns the proper character to signify a floor/wall/Does not matter value
	 * @param i
	 * @return
	 */
	private char getChar(int i){
		if(i == TileSetManager.floor){
			return TilePattern.FLOOR;
		}
		if(i == TileSetManager.wall){
			return TilePattern.WALL;
		}
		return TilePattern.DOES_NOT_MATTER;
	}

}