package vasta.sam.game.world.gen;

import java.util.ArrayList;
import java.util.List;

import vasta.sam.DecayGame;
import vasta.sam.game.states.GameWorld;
import vasta.sam.game.world.gen.rooms.RoomGenerator;
import vasta.sam.game.world.gen.rooms.RoomGeneratorSimple;
import vasta.sam.util.DynamicMap;
import vasta.sam.util.IntPoint;
import vasta.sam.util.world.TileSetManager;

import com.badlogic.gdx.graphics.g2d.Batch;

public class ChunkMap extends DynamicMap<Room>{
	
	public static final int UPDATE_TIME = 5;
	
	private List<IntPoint> loadedChunks;
	private IntPoint currentChunk;
	private IntPoint lastCurrentChunk;
	private List<Room> loadedRooms;
	private GameWorld world;
	
	public ChunkMap(GameWorld world){
		loadedChunks = new ArrayList<IntPoint>();
		loadedRooms = new ArrayList<Room>();
		currentChunk = new IntPoint();
		lastCurrentChunk = null;
		this.world = world;
	}
	
	/**
	 * generates a Room at the specified position. Uses an appropriate room generator
	 * @param x
	 * @param y
	 */
	public void generate(int x, int y){
		System.out.println("Generating chunk " + x + ", " + y);
		this.set(x, y, getGenerator(x, y).generate(x, y, world));
	}
	
	public void render(){
		for(Room r: loadedRooms){
			r.render();
		}
	}
	
	/**
	 * Updates loadedChunks list with chunks immediately around the currentChunk
	 */
	private void updateLoadedChunks(){
		System.out.println("UPDATING CHUNK LIST");
//		System.out.println("Current Chunk (" + loadedChunks.size() + "): " + currentChunk);
		
		if(get(currentChunk.getX(), currentChunk.getY()) != null){
			loadedChunks.clear();
			loadedRooms.clear();
			int centerChunkX = currentChunk.getX()/(TileSetManager.TILE_SIZE*Room.ROOM_SIZE);
			int centerChunkY = currentChunk.getY()/(TileSetManager.TILE_SIZE*Room.ROOM_SIZE);
			for(int i = centerChunkX-1; i < centerChunkX+2; i++){
				for(int j = centerChunkY-1; j < centerChunkY+2; j++){
					if(get(currentChunk.getX() + i, currentChunk.getY() + j) == null){
						generate(currentChunk.getX() + i, currentChunk.getY() + j);
					}
					if(get(currentChunk.getX() + i, currentChunk.getY() + j) != null){
						Room r = get(currentChunk.getX() + i, currentChunk.getY() + j);
						loadedChunks.add(new IntPoint(r.getX(), r.getY()));
						loadedRooms.add(r);
//						System.out.println("\t" + new IntPoint(currentChunk.getX() + i, currentChunk.getY() + j));
					}
				}
			}
			for(Object r: getArray()){
				if(r instanceof Room){
					if(loadedChunks.contains(r)){
						((Room)r).load();
					}else{
						((Room)r).unload();
					}
				}
			}
		}
		
	}

	/**
	 * Checks for current chunk. If the current chunk has changed,
	 * updates the loaded chunks list.
	 * @param x
	 * @param y
	 * @return a list of chunks witch are loaded and active
	 */
	public List<Room> getLoadedRooms(int x, int y){
		lastCurrentChunk = currentChunk;
		currentChunk = getChunk(x, y);
//		System.out.println(currentChunk);
		if(!currentChunk.equals(lastCurrentChunk) || loadedChunks.size() == 0){
			updateLoadedChunks();
		}
		return loadedRooms;
	}
	
	private IntPoint getChunk(int x, int y){
		int chunkX, chunkY;
		if(x < 0){
			chunkX = (int)Math.floor(((float)x)/(Room.ROOM_SIZE*(float)TileSetManager.TILE_SIZE));
		}else{
			chunkX = (x)/(Room.ROOM_SIZE*TileSetManager.TILE_SIZE);
		}
		if(y < 0){
			chunkY = (int)Math.floor(((float)y)/(Room.ROOM_SIZE*(float)TileSetManager.TILE_SIZE));
		}else{
			chunkY = (y)/(Room.ROOM_SIZE*TileSetManager.TILE_SIZE);
		}
		
		return new IntPoint(chunkX, chunkY);
	}
	
	public IntPoint getChunk(double x, double y){
		int chunkX, chunkY;
		if(x < 0){
			chunkX = (int)Math.floor(((float)x)/(Room.ROOM_SIZE*(float)TileSetManager.TILE_SIZE));
		}else{
			chunkX = (int)((float)x)/(Room.ROOM_SIZE*TileSetManager.TILE_SIZE);
		}
		if(y < 0){
			chunkY = (int)Math.floor(((float)y)/(Room.ROOM_SIZE*(float)TileSetManager.TILE_SIZE));
		}else{
			chunkY = (int)((float)y)/(Room.ROOM_SIZE*TileSetManager.TILE_SIZE);
		}
		return new IntPoint(chunkX, chunkY);
	}
	
	/**
	 * Finds an appropriate type of room generator based on position and returns it.
	 * @param x
	 * @param y
	 * @return a room generator.
	 */
	private RoomGenerator getGenerator(int x, int y){
		// TODO
		return new RoomGeneratorSimple();
	}
}

