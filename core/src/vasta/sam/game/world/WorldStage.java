package vasta.sam.game.world;

import vasta.sam.game.entities.Entity;
import vasta.sam.game.entities.attacks.Attack;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Sam Vasta, Aug 5, 2014
 * <p>
 * A WorldStage is an extension of {@link com.badlogic.gdx.scenes.scene2d.Stage}
 * which has multiple groups for different objects that may exist in the world.
 * This ensures that rendering these objects is in the correct order.
 * <p>
 * @author Sam Vasta
 */
public class WorldStage extends Stage{
	
	private static final int HUD_INDEX = 6,
							 PARTICLE_INDEX = 5,
							 PLAYER_INDEX = 4,
							 ACTOR_INDEX = 3,
							 ATTACK_INDEX = 2,
							 TILE_INDEX = 1;
	
	protected Group HUD;
	protected Group particles;
	protected Group player;
	protected Group actors;
	protected Group attacks;
	protected Group tiles;
	
	public WorldStage(){
		super();
		HUD = new Group();
		HUD.setZIndex(HUD_INDEX);
		
		particles = new Group();
		particles.setZIndex(PARTICLE_INDEX);
		
		player = new Group();
		player.setZIndex(PLAYER_INDEX);
		
		actors = new Group();
		actors.setZIndex(ACTOR_INDEX);
		
		attacks = new Group();
		attacks.setZIndex(ATTACK_INDEX);
		
		tiles = new Group();
		tiles.setZIndex(TILE_INDEX);
		
		addActor(tiles);
		addActor(attacks);
		addActor(actors);
		addActor(player);
		addActor(particles);
		addActor(HUD);
	}
	
	@Override
	public void addActor(Actor a){
		if(a instanceof vasta.sam.game.entities.Entity){
			switch(((Entity)a).getType()){
			case HUD:
				HUD.addActor(a);
				break;
			case PARTICLE:
				particles.addActor(a);
				break;
			case PLAYER:
				player.addActor(a);
				break;
			case ACTOR:
				actors.addActor(a);
				break;
			case ATTACK:
				attacks.addActor(a);
				break;
			case TILE:
				tiles.addActor(a);
				break;
			}
		}
		else{
			super.addActor(a);
		}
	}

}
