package vasta.sam.game;

import vasta.sam.DecayGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 *Created by Sam Vasta, Jul 14, 2014<br>
 *An abstract representation of a game state. Follows the State pattern.
 *@author Sam Vasta
 */
public abstract class GameState implements InputProcessor{
	
	protected Stage stage;
	protected Camera camera;
	protected Batch batch;
	
	/**
	 * Sets up the Stage and Camera, then calls init()
	 */
	public GameState(){
		Gdx.input.setInputProcessor(this);
		initStage();
		initCamera();
		initBatch();
		init();
	}
	
	/**
	 * Instantiates the stage.
	 */
	protected void initStage(){
		stage = new Stage();
	}
	
	/**
	 * Instantiates the camera and adds it to the stage.
	 */
	protected void initCamera(){
		camera = new Camera();
		stage.getViewport().setCamera(camera);
	}
	
	/**
	 * Instantiates the batch for rendering
	 */
	protected void initBatch(){
		batch = new SpriteBatch();
	}
	
	/**
	 * returns the batch to draw with.
	 * @return
	 */
	public Batch getBatch(){
		return batch;
	}
	
	/**
	 * Returns this gameState's stage
	 * @return
	 */
	public Stage getStage(){
		return stage;
	}
	
	/**
	 * Returns the camera for this gamestate
	 * @return
	 */
	public Camera getCamera(){
		return camera;
	}
	
	/**
	 * Initializes the Game State
	 */
	public abstract void init();
	
	/**
	 * Draw the Game State
	 * @param q - QuaffGame
	 * @param batch - SpriteBatch
	 */
	public abstract void render(DecayGame q);
	/**
	 * Update the Game Logic
	 * @param q - QuaffGame
	 * @param delta - % of one cycle since last update
	 */
	public abstract void update(DecayGame q, float delta);
	/**
	 * 
	 * @return the Game State's ID. Used to identify the Game State when switching states.
	 */
	public abstract int getID();
	
	/**
	 * Handles things before the actual update, then calls update()
	 * @param q - QuaffGame
	 * @param delta - % of one cycle since last update.
	 */
	public void preUpdate(DecayGame q, float delta){
		camera.update(delta);
		stage.act();
		update(q, delta);
	}
	
	/**
	 * Handles things before the actual rendering, then calls render()
	 * @param q - QuaffGame
	 * @param batch - SpriteBatch
	 */
	public void preRender(DecayGame q){
		//No pre-Render calls
		render(q);
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == Input.Keys.ESCAPE){
			Gdx.app.exit();
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
}
