package vasta.sam.elements;

import java.util.ArrayList;
import java.util.Arrays;

import vasta.sam.util.MathHelper;

/**
 * Created by Sam Vasta, Aug 26, 2014
 *<p>
 *An Atom is a clone of an {@link Element} which may undergo various changes such as
 *half-life decay.
 *<p>
 * @author Sam Vasta
 */
public class Atom {

	public static final double CHANCE_TO_SPLIT = 0.001;
	public static final int ALPHA_DECAY = 0,
							BETA_DECAY = 1;
	
	private Element parent;
	private int atomicWeight;
	private int[] electrons;
	private long halfLife;
	private float reactiveness;
	private float hardness;
	private float malleability;
	private float damageMods[];
	private int valenceElectrons;
	private String name;
	/**
	 * Accumulates ms to subtract from halfLife
	 */
	private float accumulator;
	
	/**
	 * Creates an exact copy of the parent element.
	 * @param parent
	 */
	public Atom(Element parent){
		this.parent = parent;
		this.atomicWeight = parent.getAtomicWeight();
		this.electrons = parent.getElectrons();
		this.valenceElectrons = parent.getValenceElectrons();
		this.halfLife = parent.getHalfLife();
		this.reactiveness = parent.getReactiveness();
		this.hardness = parent.getHardness();
		this.malleability = parent.getMalleability();
		this.damageMods = parent.getDamageMods();
		this.name = parent.getName();
	}
	
	
	/**
	 * 
	 * @param parent
	 * @param atomicWeight
	 * @param electrons
	 * @param halfLife
	 * @param reactiveness
	 * @param hardness
	 * @param malleability
	 * @param damageMods
	 */
	public Atom(Element parent, int atomicWeight, int[] electrons, long halfLife, float reactiveness, float hardness, float malleability, float[] damageMods){
		this.parent = parent;
		this.atomicWeight = atomicWeight;
		this.electrons = Arrays.copyOf(electrons, electrons.length);
		this.halfLife = halfLife;
		this.reactiveness = reactiveness;
		this.hardness = hardness;
		this.malleability = malleability;
		this.damageMods = Arrays.copyOf(damageMods, damageMods.length);
		this.name = parent.getName();
		if(this.atomicWeight != parent.getAtomicWeight() || !Arrays.equals(this.electrons, parent.getElectrons())){
			evaluate();
		}
	}
	
	public void update(float delta){
		accumulator += delta;
		if(accumulator > 1){
			halfLife -= (long)accumulator;
			accumulator -= (long)accumulator;
		}
	}
	
	public boolean shouldSplit(){
		return (Math.random() < CHANCE_TO_SPLIT) && halfLife <= 0;
	}
	
	/**
	 * Simulates radiation decay. Picks a type of decay, then creates the new atoms.
	 * @return a list of two or more atoms which this atom will split into after decay.
	 */
	public ArrayList<Atom> split(){
		ArrayList<Atom> atoms = new ArrayList<Atom>();
		int type = ALPHA_DECAY;
		switch (type){
		case ALPHA_DECAY:
			atoms.add(parent.getTable().getAlphaParticle());
			atoms.add(new Atom(parent.getTable().getElement(parent.getProtons()-1), atomicWeight-2, electrons, halfLife, reactiveness, hardness, malleability, damageMods));
			break;
		case BETA_DECAY:
			System.err.println("ADD BETA DECAY IN atom.java");
			break;
		}
		
		return atoms;
	}

	/**
	 * Makes changes to variables based on the new atom structure including name, valence electrons,
	 * hardness, malleability, etc.
	 */
	private void evaluate(){
		fixName();
		calculateValenceElectrons();
		float damageModsAvg = (damageMods[0] + damageMods[1] + damageMods[2] + damageMods[3] + damageMods[4] + damageMods[5])/2;

		hardness = (float)(1-damageModsAvg) + 0.15f*((float)electrons[12]/(float)Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[12]]);
		if(parent.isMetal()){
			hardness *= (MathHelper.random() * 0.25f) + 0.65f;
		}
		else{
			hardness *= (0.5f*MathHelper.random()) + 0.2f;
		}
		
		if(hardness > 1) hardness = 1;
		if(hardness < 0) hardness = 0;
		
		malleability = (1-(hardness*hardness*(float)(MathHelper.random()/2)));
		for(float f: damageMods){
			if(f > 0.5f)
				malleability *= (f/2f);
		}
		if(parent.isOrganic()){
			malleability *= (MathHelper.random() * 0.25f) + 0.9f;
		}
		else{
			malleability *= (0.5f * MathHelper.random()) + 0.35f;
		}
		
		if(malleability > 1) malleability = 1;
		if(malleability < 0) malleability = 0;
		
		reactiveness = (float) (MathHelper.random()*0.05f) + (0.95f*((hardness*malleability)));
		reactiveness *= 1f + (1f - (Math.abs(parent.getAtomicWeight() - atomicWeight) / parent.getAtomicWeight()));
//		reactiveness *= 1-(damageModsAvg*3);
		if(parent.isOrganic()){
			reactiveness *= 1.05f;
		}
		if(parent.isMetal()){
			reactiveness -= Math.random()/10;
		}
		if(reactiveness > 1) reactiveness = 1;
		if(reactiveness < 0) reactiveness = 0;
		
		halfLife = (long)(Math.pow((((PeriodicTable.MIN_ELEMENTS + PeriodicTable.RAND_VALUE) * 1.55)-atomicWeight)*(reactiveness), 2) * 1000) + Element.MIN_HALF_LIFE;
		halfLife *= (1f - reactiveness);
		if(parent.getProtons() < 10) halfLife *= 10;
	}
	
	/**
	 * Gets the number of electrons on the outer-most non-zero of the electrons array
	 */
	private void calculateValenceElectrons(){
		int i = 0;
		while(electrons[i] > 0){
			i++;
		}
		i--;
		valenceElectrons = electrons[i];
	}
	
	/**
	 * checks for isotope and/or ion status and changes name to the following format if applicable:
	 * <br>
	 * (atomicWeight)_(atomicNumber)_(name)_(absoluteCharge)(chargeSign)
	 */
	private void fixName(){
		int chargeDiff = 0;
		for(int i = 0; i < electrons.length; i++){
			chargeDiff += electrons[i]-parent.getElectrons()[i];
		}
		name = atomicWeight + "_" + parent.getProtons() + "_" + parent.getName() + "_";
		if(Math.abs(chargeDiff) == 1){
			name += chargeDiff/Math.abs(chargeDiff);
		}
		else if(chargeDiff != 0){
			name += Math.abs(chargeDiff);
			if(chargeDiff < 0){
				name += "-";
			}
			else if(chargeDiff > 0){
				name += "+";
			}
		}
	}



	//GETTERS
	
	public Element getParent() {
		return parent;
	}


	public int getAtomicWeight() {
		return atomicWeight;
	}


	public int[] getElectrons() {
		return electrons;
	}


	public long getHalfLife() {
		return halfLife;
	}


	public float getReactiveness() {
		return reactiveness;
	}


	public float getHardness() {
		return hardness;
	}


	public float getMalleability() {
		return malleability;
	}


	public float[] getDamageMods() {
		return damageMods;
	}


	public int getValenceElectrons() {
		return valenceElectrons;
	}


	public String getName() {
		return name;
	}
}
