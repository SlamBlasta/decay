package vasta.sam.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by Sam Vasta, Aug 25, 2014
 *<p>
 * A Molecule is a group of elements. Different combinations of elements
 * produce unique results and properties.
 *<p>
 * @author Sam Vasta
 */
public class Molecule {
	
	private volatile LinkedList<Atom> atoms;
	Stack<Atom> toRemove = new Stack<Atom>(),
				toAdd = new Stack<Atom>();
	private volatile int damage;
	private volatile float damageMods[];

	public Molecule(Atom...atoms){
		this.atoms = new LinkedList<Atom>();
		for(Atom a: atoms)
			this.atoms.add(a);
		
		calculateStats();
		
		Thread updater = new Thread(new MoleculeRunner());
		updater.start();
		//TODO: stop thread when molecule goes away, implement Molecule Runner, make things thread-safe
	}
	
	public Molecule(Collection<Atom> atoms){
		this(atoms.toArray(new Atom[0]));
	}
	
	public void update(float delta){
		for(Atom a: atoms){
			a.update(delta);
			if(a.shouldSplit()){
				toRemove.add(a);
				toAdd.addAll(a.split());
			}
		}
		
		if(toRemove.size() > 0 && toAdd.size() > 0){
			for(Atom a: toRemove)
				atoms.remove(a);
			for(Atom a: toAdd)
				atoms.add(a);
			
			toRemove.clear();
			toAdd.clear();
			
			calculateStats();
		}
	}
	
	private void calculateStats(){
		//TODO: fix damage calculation!!
		float hardness = 0;
		damageMods = new float[Element.NUM_DAMAGE_TYPES];
		Arrays.fill(damageMods, 1f);
		float totalMods = 0;
		float maxMod = 0;
		for(Atom a: atoms){
			for(int i = 0; i < damageMods.length; i++){
				if(a.getDamageMods()[i] != 0)
					damageMods[i] *= a.getDamageMods()[i];
				totalMods += a.getDamageMods()[i];
				if(a.getDamageMods()[i] > maxMod){
					maxMod = a.getDamageMods()[i];
				}
			}
			hardness += a.getHardness();
		}
//		for(int i = 0; i < damageMods.length; i++){
//			damageMods[i] /= totalMods;
//		}
		
		damage = (int)hardness;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public float[] getDamageMods(){
		return damageMods;
	}
	
	private class MoleculeRunner implements Runnable{

		@Override
		public void run() {
			//Will eventually take care of updating the molecule every frame.
		}
		
	}
}
