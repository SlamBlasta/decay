package vasta.sam.elements;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JFrame;

import vasta.sam.util.MathHelper;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Sam Vasta, Aug 21, 2014
 *<p>
 *A Periodic Table is used to create and store information about a set of elements which
 * all belong in the same universe.
 *<p>
 * @author Sam Vasta
 */
public class PeriodicTable extends JFrame implements Serializable, MouseMotionListener{

	/**
	 * The total number of elements in the periodic table will be
	 * between MIN_ELEMENTS  and MIN_ELEMENTS+RAND_VALUE
	 */
	public static final int MIN_ELEMENTS = 100,
							RAND_VALUE = 50;
	
	/**
	 * Used in adjusting mod scales from linear to exponential.
	 */
	private static final double MOD_ADJUSTER_A = 0.1218485587;
	private static final double MOD_ADJUSTER_B = 2.096916149;
	private static final double MOD_ADJUSTER_M = 15d/16d;
	
	/**
	 * The minimum orbital required for an element to be eligible for electron orbital scrambling.
	 */
	public static final int MIN_ELECTRON_SCRAMBLE_ORBITAL = 3;
	/**
	 * The chance that an eligible electron will have its orbitals scrambled.
	 */
	public static final double CHANCE_OF_ELECTRON_SCRAMBLING = 0.15;
	/**
	 * Used for generating element names.
	 */
	public static final String[] suffixes = new String[]{
		"ium",
		"ium",
		"ium",
		"ium",
		"ium",
		"ium",
		"ium",
		"ium",
		"um",
		"om",
		"ite",
		"ous",
		"ix",
		"ax",
		"",
		"",
	};
	
	/**
	 * Used for generating element names.
	 */
	public static final char[] vowels = new char[]{
		'a', 'e', 'i', 'o', 'u',
	};

	/**
	 * Used for generating element names.
	 */
	public static final char[] consonants = new char[]{
		'b', 'c', 'd', 'f', 'g','h','j','k','l','m','n','p','r','s','t','v','w','x','y','z', 'b', 'c', 'd', 'f', 'g','h','l','m','n','p','r','s','t', 'b', 'c', 'd', 'f', 'g','h','l','m','n','p','r','s','t'
	};
	
	/**
	 * Used for generating element names.
	 */
	public static final char[] beginningLetters = new char[]{
		'A', 'B', 'C', 'D', 'G', 'H', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'Y', 'Z'
	};

	
	private Element[] elements;
	private int numElements;
	private int tableWidth, tableHeight;
	
	public PeriodicTable(boolean isFrame){
		//Set size of table (#elements)
		//Size is between MIN_ELEMENTS and MIN_ELEMENTS+RAND_VALUE
		numElements = MathHelper.nextInt(RAND_VALUE) + MIN_ELEMENTS;
		
		tableWidth = (int)Math.ceil(Math.sqrt((MIN_ELEMENTS + RAND_VALUE)*1.5));
		tableHeight = tableWidth;
		
		//A temporary table which keeps track of which spaces are occupied and which are free.
		boolean[][] tempTable = new boolean[tableWidth][tableHeight];
		//Clear tempTable
		for(int i = 0; i < tableWidth; i++){
			for(int j = 0; j < tableHeight; j++)
				tempTable[i][j] = false;
		}
		
		Point current = new Point(tableWidth/2, tableHeight/2);			//Current x and y positions for filling in the table.
		int numFilledIn = 0;											//The number of elements currently placed
		
		//Fill in initial element into center of grid.
		tempTable[current.x][current.y] = true;
		numFilledIn++;
		
		//Fill in the rest of the table until number of elements put in matches number of elements in the real table.
		while(numFilledIn < numElements){
			if(MathHelper.random() < 0.1 && numFilledIn > numElements/2){	//Random chance that it will pick a completely new current point
				current = findValidEmptyCell(tempTable);
			}else{
				current = findValidNeighbor(current, tempTable);
			}
			tempTable[current.x][current.y] = true;
			numFilledIn++;
		}
		
		//Fills in empty spaces which are completely surrounded with elements
		for(int i = 1; i < tempTable.length-2; i++){
			for(int j = 1; j < tempTable[0].length-2; j++){
				if(!tempTable[i][j] && tempTable[i-1][j] && tempTable[i+1][j] && tempTable[i][j-1] && tempTable[i][j+1]){
					tempTable[i][j] = true;
					numElements++;
				}
			}
		}
		
		//Calculate minimum and maximum x/y values to remove empty rows and cols
		int minX=Integer.MAX_VALUE, minY=Integer.MAX_VALUE, maxX=Integer.MIN_VALUE, maxY=Integer.MIN_VALUE;
		for(int i = 0; i < tempTable.length; i++){
			for(int j = 0; j < tempTable[0].length; j++){
				if(tempTable[i][j]){
					if(i < minX)minX=i;
					if(i > maxX)maxX=i;
					if(j < minY)minY=j;
					if(j > maxY)maxY=j;
				}
			}
		}
		tableWidth = maxX-minX + 1;
		tableHeight = maxY-minY + 1;
		
		//Actual creation of elements from tempTable data.
		
		elements = new Element[numElements];
		
		int elemNum = 1;
		ArrayList<String> symbols = new ArrayList<String>();
		for(int i = 0; i < maxY+1; i++){
			for(int j = 0; j < maxX+1; j++){
				if(tempTable[j][i]){
					//Gen name and symbol. See methods for more info.
					String name = genName();
					String symbol = genSymbol(name, symbols);
					//add symbol to dictionary of symbols to avaiod duplication
					symbols.add(symbol);
					
					//calculate mass and subtract elemNum (protons) for neutrons
					int neutrons = (int)Math.round(elemNum*1.5) - elemNum;
					
					/**
					 * ELECTRON GENERATION:
					 * Electron orbitals are first filled in, then have a small chance of moving around.
					 */
					
					//initiate electron orbitals array. There is one integer for each orbital possible.
					int[] electrons = new int[Element.ELECTRON_ORBITAL_TYPE_ORDER.length];
					//Keeps track of the number of electrons that have been assigned to an orbital.
					int electronsAssigned = 0;
					//Keeps track of the current orbital being filled up
					int currentOrbital = 0;
					//Fill orbitals until the number of electrons assigned equals the elementNumber (protons). Each loop represents one orbital filling up
					while(electronsAssigned < elemNum){
						/**
						* If there are more electrons left than there are slots in the current orbital, fill the orbital and continue.
						* The ORBITAL_TYPE_ORDER array holds information on the number of type of orbital on the current layer, and this information
						* is used to find the number of electrons allowed in that type of orbital through a lookup in ORBITAL_MAX_VALUES.
						* The appropriate number is added to electronsAssigned.
						*/
						if(elemNum - electronsAssigned > Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[currentOrbital]]){
							electrons[currentOrbital] = Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[currentOrbital]];
							electronsAssigned += Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[currentOrbital]];
						}
						//If there are not enough electrons left to fill the orbital, the remaining electrons are dumped into the current orbital.
						else{
							electrons[currentOrbital] = elemNum - electronsAssigned;
							electronsAssigned += elemNum - electronsAssigned;
						}
						//This orbital is finished, so we move to the next one.
						currentOrbital++;
					}
					
					//Randomly mix the top two orbitals.
					if(electrons[currentOrbital-1] / (double)Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[currentOrbital-1]] < CHANCE_OF_ELECTRON_SCRAMBLING && currentOrbital > MIN_ELECTRON_SCRAMBLE_ORBITAL){
						//put currentOrbital back to the last orbital with electrons in it.
						currentOrbital--;
						
						//If the last orbital is completely full, take an electron and put it to the next orbital instead.
						if(electrons[currentOrbital] == Element.ELECTRON_ORBITAL_MAX_VALUES[Element.ELECTRON_ORBITAL_TYPE_ORDER[currentOrbital]]){
							currentOrbital++;
						}
						
						electrons[currentOrbital] ++;
						electrons[currentOrbital-1] --;
					}
					//End of electron gen.
					
					elements[elemNum-1] = new Element(name, symbol, i-minY, j-minX, elemNum, neutrons, electrons);
					elemNum++;
					
				}
			}
		}
		
		setElementTables();
		calculateDamageMods();
		
		for(Element e: elements){
			e.calculateVariables();
			e.calculateRarity();
		}
		
		long minHL =Long.MAX_VALUE, maxHL=0;
		float avgHL = 0;
		for(Element e: elements){
			if(e.getHalfLife() > maxHL) maxHL = e.getHalfLife();
			if(e.getHalfLife() < minHL) minHL = e.getHalfLife();
			avgHL += e.getHalfLife()/1000;
		}
		avgHL /= elements.length;
		System.out.println("MIN: " + minHL/1000 + "\nMAX: " + maxHL/1000 + "\nAVG: " + avgHL);
		
		float min = 2, avg = 0;
		for(Element e: elements){
			if(e.getHardness() < min) min = e.getHardness();
			avg += e.getHardness();
		}
		avg /= (float)elements.length;
		avgH = avg;
		minH = min;
		
		min = 2;
		avg = 0;
		for(Element e: elements){
			if(e.getMalleability() < min) min = e.getMalleability();
			avg += e.getMalleability();
		}
		avg /= (float)elements.length;
		avgM = avg;
		minM = min;
		
		min = 2;
		avg = 0;
		float max = -1;
		for(Element e: elements){
			if(e.getReactiveness() < min) min = e.getReactiveness();
			if(e.getReactiveness() > max) max = e.getReactiveness();
			avg += e.getReactiveness();
		}
		avg /= (float)elements.length;
		avgR = avg;
		minR = min;
		maxR = max;
		
		min = 2;
		avg = 0;
		max = -1;
		for(Element e: elements){
			if(e.getRarity() < min) min = e.getRarity();
			if(e.getRarity() > max) max = e.getRarity();
			avg += e.getRarity();
		}
		avg /= (float)elements.length;
		avgRar = avg;
		minRar = min;
		maxRar = max;

		if(isFrame){
			setSize(tableWidth*32 + 464, tableHeight*32 + 96);
			setLocationRelativeTo(null);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			addMouseMotionListener(this);
			setVisible(true);
		}
	}
	
	
	//FOR TESTING PURPOSES!!
	private float minH, minM, minR, maxR, avgH, avgM, avgR, minRar, maxRar, avgRar;
	
	public float getMinH(){ return minH;}
	public float getMinM(){ return minM;}
	public float getMinR(){ return minR;}
	public float getMaxR(){ return maxR;}
	public float getAvgH(){ return avgH;}
	public float getAvgM(){ return avgM;}
	public float getAvgR(){ return avgR;}
	public float getAvgRar(){ return avgRar;}
	public float getMinRar(){ return minRar;}
	public float getMaxRar(){ return maxRar;}
	
	int mouseX, mouseY;
	public void paint(Graphics g){
		BufferedImage i = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics bg = i.getGraphics();
		bg.setColor(Color.BLACK);
		bg.fillRect(0, 0, getWidth(), getHeight());
		bg.setColor(new Color(255, 255, 255));
		int x = (mouseX - 32)/32;
		int y = (mouseY - 64)/32;
		bg.drawString(x + ", " + y, tableWidth*32 + 64, 96);
		for(Element e: elements){
			bg.setColor(e.getColor());
			
//			if(e.isOrganic() && !e.isMetal()){
//				bg.setColor(Color.CYAN);
//			}else if(e.isOrganic() && e.isMetal()){
//				bg.setColor(Color.PINK);				
//			}else if(!e.isOrganic() && e.isMetal()){
//				bg.setColor(Color.LIGHT_GRAY);
//			}else if(!e.isOrganic() && !e.isMetal()){
//				bg.setColor(Color.DARK_GRAY);
//			}
			
			bg.fillRect(32 + e.getXPos()*32, 64 + e.getYPos()*32, 32, 32);
			bg.setColor(Color.white);
			bg.drawRect(32 + e.getXPos()*32, 64 + e.getYPos()*32, 32, 32);
			
			if(e.getYPos() == y && e.getXPos() == x){
				String s[] = e.toString().split("\n");
				for(int p = 0; p < s.length; p++){
					bg.drawString(s[p], tableWidth*32 + 64, 120 + p*12);
				}
//				bg.drawString("Fire value:\t" + e.getDamageMods()[0], tableWidth*32 + 64, 120 + 1*12);
//				bg.drawString("Cold value:\t" + e.getDamageMods()[1], tableWidth*32 + 64, 120 + 2*12);
//				bg.drawString("Electric value:\t" + e.getDamageMods()[2], tableWidth*32 + 64, 120 + 3*12);
//				bg.drawString("Poison value:\t" + e.getDamageMods()[3], tableWidth*32 + 64, 120 + 4*12);
//				bg.drawString("Necrotic value:\t" + e.getDamageMods()[4], tableWidth*32 + 64, 120 + 5*12);
//				bg.drawString("Radient value:\t" + e.getDamageMods()[5], tableWidth*32 + 64, 120 + 6*12);
//				bg.drawString("Metal value:\t" + e.isMetal(), tableWidth*32 + 64, 120 + 7*12);
//				bg.drawString("#" + e.getAtomicNum(), tableWidth*32 + 64, 120 + 8*12);
			}
		}
		
		
		g.drawImage(i, 0, 0, null);
		repaint();
	}
	
	public int[] getVals(){
		int[] vals = new int[1000];
		for(Element e: elements){
			vals[(int)(e.getRarity()*999.99)]++;
		}
		return vals;
	}
	
	//END TEST STUFF
	
	public Element getRandomElement(boolean useRarity){
		Element e = null;
		while(e == null){
			e = elements[MathHelper.nextInt(elements.length)];
			if(useRarity && MathHelper.random() > e.getRarity()){
				e = null;
			}
		}
		return e;
	}
	
	public Atom getAlphaParticle(){
		return new Atom(elements[1], elements[1].getAtomicWeight(), new int[Element.ELECTRON_ORBITAL_MAX_VALUES.length], elements[1].getHalfLife(), elements[1].getReactiveness(), elements[1].getHardness(), elements[1].getMalleability(), elements[1].getDamageMods());
	}
	
	private void setElementTables(){
		for(Element e: elements){
			e.setTable(this);
		}
	}
	
	/**
	 * Uses electron orbitals, row/col positions, protons, and neutrons to determine
	 * the damage modifiers of this element. Damage modifiers are calculated on a linear
	 * scale from 0 to 1.
	 */
	private void calculateDamageMods(){
		//Points in this list have the following info: (elementNum, relative score)
		//The list is used to rank elements.
		ArrayList<Point> elementValues = new ArrayList<Point>();
		
		//Fire Mod
		//Affected by: Row number, electrons in 's' orbitals
		for(Element e: elements){
			int score = 0;
			score += (e.getXPos())*10;
			score += e.getElectrons()[0];
			score -= e.getElectrons()[1];
			score += e.getElectrons()[3];
			score -= e.getElectrons()[5];
			score += e.getElectrons()[8];
			score -= e.getElectrons()[11];
			score += e.getElectrons()[15];
			score -= e.getElectrons()[19];
			score += e.getElectrons()[23];
			score -= e.getElectrons()[27];
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_FIRE);
		elementValues.clear();
		
		//Cold Mod
		//Effected by: Row number, electrons in 's' orbitals.
		for(Element e: elements){
			int score = 0;
			score += (tableWidth - e.getXPos())*10;
			score -= e.getElectrons()[0];
			score += e.getElectrons()[1];
			score -= e.getElectrons()[3];
			score += e.getElectrons()[5];
			score -= e.getElectrons()[8];
			score += e.getElectrons()[11];
			score -= e.getElectrons()[15];
			score += e.getElectrons()[19];
			score -= e.getElectrons()[23];
			score += e.getElectrons()[27];
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_COLD);
		elementValues.clear();
		
		//Electric Mod
		//Affected by: Metalicness, atomicNumber, 'd' orbitals, valence electrons
		for(Element e: elements){
			int score = 0;
			if(e.isMetal()) score+=100;
			if(e.getAtomicNum() > elements.length/3) score += 100;
			if(e.getAtomicNum() < elements.length/10) score-= 20;
			score += e.getElectrons()[6]*1;
			score += e.getElectrons()[9]*2;
			score += e.getElectrons()[13]*-1;
			score += e.getElectrons()[17]*-2;
			score += (15 - e.getValenceElectrons())*15;
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_ELECTRIC);
		elementValues.clear();
		
		//Poison Mod
		//Affected by: metalicness, orgainicness, valence electrons, weight, x positoin
		int col = MathHelper.nextInt(tableWidth/2) + tableWidth/4;
		for(Element e: elements){
			int score = 0;
			
			score += Math.abs(e.getXPos() - col)*-250;
			
			if(e.isMetal()) score += e.getAtomicWeight()*3;
			else if(!e.isOrganic()) score += 200;
			
			score += Math.abs(e.getValenceElectrons())*20;
			score -= (e.getAtomicWeight() - 100)*5;
			
			for(int i = 0; i < e.getElectrons().length-1; i++){
				if(e.getElectrons()[i] == 0) continue;
				for(int j = i+1; j < e.getElectrons().length; j++){
					if(e.getElectrons()[j]==0)continue;
					if(e.getElectrons()[i] % 7 == e.getElectrons()[j] % 3){
						score += 25;
					}
				}
			}
			
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_POISON);
		elementValues.clear();
		
		//Necrotic Mod
		//Affected by: Col Number, neutrons
		for(Element e: elements){
			int score = 0;
			score += (e.getYPos())*10;
			score += ((e.getNeutrons()+8) % 15)*5;
			score -= e.getElectrons()[3]*e.getElectrons()[4]/(1+e.getElectrons()[6]);
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_NECROTIC);
		elementValues.clear();
		
		//Radient Mod
		//Affected by: Col Number
		for(Element e: elements){
			int score = 0;
			score += (tableHeight - e.getYPos())*10;
			score += (e.getNeutrons() % 15)*5;
			score -= e.getElectrons()[4]*e.getElectrons()[3]*e.getElectrons()[6];
			elementValues.add(new Point(e.getAtomicNum(), score));
		}
		sortAndAssign(elementValues, Element.EFFECT_RADIENT);
		elementValues.clear();
	}
	
	public int getNumElements(){
		return elements.length;
	}
	
	public Element getElement(int atomicNum){
		return elements[atomicNum];
	}
	
	@Override
	public void write(Json json) {
		// TODO Auto-generated method stub
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * A comparator to sort elements which have been scored on some point system.
	 */
	private final Comparator<Point> scoreSorter = new Comparator<Point>(){
		public int compare(Point o1, Point o2) {
			if(o1.y > o2.y)	return 1;
			if(o1.y < o2.y)	return -1;
			return 0;
		}
	};
	/**
	 * Automatically sorts elements by score, adjusts values, and assigns modifiers.
	 * @param elementValues
	 * @param damageType
	 */
	private void sortAndAssign(ArrayList<Point> elementValues, int damageType){
		Collections.sort(elementValues, scoreSorter);
		for(int i = 0; i < elementValues.size(); i++){
			elements[elementValues.get(i).x-1].setDamageMod(damageType, adjustDamageMod((float)(i+1)/(float)elements.length));
//			System.out.println(elements[elementValues.get(i).x-1].getAtomicNum() + "'s " + damageType + " value: " + elements[elementValues.get(i).x-1].getDamageMods()[damageType]);
		}
		for(Element e: elements)
			e.setDamageMod(damageType, 0f);
	}
	
	/**
	 * Adjusts the linear scale of the damage mod values into a more aggressive scale,
	 * then rounds to the nearest 1/20th for good looking numbers.
	 */
	private float adjustDamageMod(float mod){
		mod = (float)(MOD_ADJUSTER_A * Math.pow(Math.E, MOD_ADJUSTER_B*((mod - MOD_ADJUSTER_M)/(1-MOD_ADJUSTER_M))));
		mod = 5*Math.round(100*(mod/5f))/100f;
		return mod;
	}
	
	/**
	 * 
	 * @param tempTable
	 * @return A point on the table which is both empty, and adjacent to a full cell
	 */
	private Point findValidEmptyCell(boolean[][] tempTable){
		boolean isValid = false;
		Point p;
		do{
			p = new Point(MathHelper.nextInt(tableWidth), MathHelper.nextInt(tableHeight));
			if(!tempTable[p.x][p.y]){
				if(p.x > 0 && tempTable[p.x-1][p.y]){
					isValid = true;
				}
				if(p.x < tempTable.length-1 && tempTable[p.x+1][p.y]){
					isValid = true;
				}
				if(p.y > 0 && tempTable[p.x][p.y-1]){
					isValid = true;
				}
				if(p.y < tempTable[0].length-1 && tempTable[p.x][p.y+1]){
					isValid = true;
				}
			}
		}while(!isValid);
		
		return p;
	}
	
	/**
	 * Tries to find an adjacent empty space. If it cannot find one, it picks a random point until it finds an empty point with a valid neighbor.
	 * @param current
	 * @param tempTable
	 * @return A point to place an element
	 */
	private Point findValidNeighbor(Point current, boolean[][] tempTable){
		Point point = null;

		//Notes if the algorithm has checked the direction for an empty cell yet. Checks each direction randomly
		boolean up = current.y > 0, down = current.y < tempTable[0].length-1, left = current.x > 0, right = current.x < tempTable.length-1, pointChanged = false;
		while(!up && !down && !left && !right){
			int dir = (int)(MathHelper.random()*4) +1;
			pointChanged = false;
			if(dir==1 && !up){
				point = new Point(current.x, current.y-1);
				up = true;
				pointChanged = true;
			}
			if(dir==2 && !down){
				point = new Point(current.x, current.y+1);
				down = true;
				pointChanged = true;
			}
			if(dir==3 && !left){
				point = new Point(current.x-1, current.y);
				left = true;
				pointChanged = true;
			}
			if(dir==4 && !right){
				point = new Point(current.x+1, current.y);
				right = true;
				pointChanged = true;
			}
			
			if(pointChanged && hasNeighbors(point, tempTable) && !tempTable[point.x][point.y])
				return point;
		}
		
		do{
			point = new Point(MathHelper.nextInt(tableWidth), MathHelper.nextInt(tableHeight));
		}while(!hasNeighbors(point, tempTable) || tempTable[point.x][point.y]);
		
		return point;
	}
	
	/**
	 * Checks if the given point has an adjacent cell which is true.
	 * @param p
	 * @param table
	 * @return true if a cell adjacent to the given point is true.
	 */
	private boolean hasNeighbors(Point p, boolean[][] table){
		if(p.x >= table.length || p.x < 0 || p.y >= table[0].length || p.y < 0)
			return false;
		
		boolean hasNeighbors = false;
		if(p.x > 0 && table[p.x-1][p.y])
			hasNeighbors = true;
		if(p.x < table.length-1 && table[p.x+1][p.y])
			hasNeighbors = true;
		if(p.y > 0 && table[p.x][p.y-1])
			hasNeighbors = true;
		if(p.y < table[0].length-1 && table[p.x][p.y+1])
			hasNeighbors = true;
		
		return hasNeighbors;
	}
	
	/**
	 * Generates a random name for an element.
	 * @return a random suitable name for an element
	 */
	private String genName(){
		int numSyllabals = 1 + (int)(MathHelper.random()*2);
		String n = "";
		for(int i = 0; i < numSyllabals; i++){
			if(i == 0){
				n += getFirstSyllabal();
			}else{
				n+=getSyllabal();
			}
		}
		n+=getSuffix();
		return n;
	}
	
	/**
	 * This is a helper method for genName()
	 * @return a random value from the list of suffixes.
	 */
	private String getSuffix(){
		return suffixes[(int)(MathHelper.random()*suffixes.length)];
	}
	
	/**
	 * A helper method for genName()
	 * @return a random syllabal suitable for the beginning of a name.
	 */
	private String getFirstSyllabal(){
		char a, b, c;
		a = beginningLetters[(int)(MathHelper.random()*beginningLetters.length)];
		b = vowels[(int)(MathHelper.random()*vowels.length)];
		c = consonants[(int)(MathHelper.random()*consonants.length)];
		return new String(new char[]{a, b, c});
	}
	
	/**
	 * A helper method for genName()
	 * @return a random syllabal
	 */
	private String getSyllabal(){
		char a, b, c;
		a = consonants[(int)(MathHelper.random()*consonants.length)];
		b = vowels[(int)(MathHelper.random()*vowels.length)];
		c = consonants[(int)(MathHelper.random()*consonants.length)];
		return new String(new char[]{a, b, c});
	}
	
	/**
	 * Takes letters from the name, and uses them to create an abbreviation.
	 * Occasionally uses random letters instead of letters from the name.
	 * @param name
	 * @param existingSymbols
	 * @return a random unique symbol.
	 */
	private String genSymbol(String name, List<String> existingSymbols){
		String sym = "";
		int tries = 0;
		boolean unique;
		do{
			unique = true;
			char c1, c2;
			if(MathHelper.random() < 0.9 - tries*0.01){
				c1 = name.charAt(0);
				c2 = name.toCharArray()[(int)(MathHelper.random() * (name.length()-2)) + 2];
			}else{
				c1 = (char)(MathHelper.random()*(90-65) + 65);
				c2 = (char)(MathHelper.random()*(90-65) + 65);
			}
			
			c1 = Character.toUpperCase(c1);
			c2 = Character.toLowerCase(c2);
			
			if(name.length() <= 4 && MathHelper.random() < 0.5){
				sym = new String(new char[]{c1});
			}else{
				sym = new String(new char[]{c1, c2});
			}
			
			for(String s: existingSymbols){
				if(s.equals(sym)) unique = false;
			}
			tries++;
		}while(!unique);
		return sym;
	}

	/**
	 * A helper class to pass position data between methods.
	 * @author Sam Vasta
	 */
	class Point{
		private int x, y;
		private Point(int x, int y){
			this.x = x;
			this.y = y;
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		mouseX = arg0.getX();
		mouseY = arg0.getY();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		mouseX = arg0.getX();
		mouseY = arg0.getY();
	}
}
