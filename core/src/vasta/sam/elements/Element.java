package vasta.sam.elements;

import java.util.Arrays;

import vasta.sam.util.MathHelper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Sam Vasta, Aug 21, 2014
 *<p>
 * An element is a building block of items in Decay. Elements and their properties are used
 * to determine various statistics of items and attacks. An Element object is merely a
 * blueprint for creating an {@link Atom} Object which exists in a {@link vasta.sam.game.states.GameWorld}.
 *<p>
 * @author Sam Vasta
 */
public class Element implements Serializable{
	
	public static final int[] ELECTRON_ORBITAL_MAX_VALUES = new int[]{2, 6, 10, 14};
	public static final int[] ELECTRON_ORBITAL_TYPE_ORDER = new int[]{0, 0, 1, 0, 1, 0, 2, 1, 0, 2, 1, 0, 3, 2, 1, 0, 3, 2, 1, 0, 3, 2, 1, 0, 3, 2, 1, 0};
	public static final int[] ELECTRON_ORBITAL_LEVEL =		new int[]{1, 2, 2, 3, 3, 4, 3, 4, 5, 4, 5, 6, 4, 5, 6, 7, 5, 6, 7, 8, 6, 7, 8, 9, 7, 8, 9, 10};
	
	public static final int MIN_HALF_LIFE = 60 * 1000;
	
	public static final int NUM_DAMAGE_TYPES = 6;
	
	public static final int EFFECT_FIRE = 0,
							EFFECT_COLD = 1,
							EFFECT_ELECTRIC = 2,
							EFFECT_POISON = 3,
							EFFECT_NECROTIC = 4,
							EFFECT_RADIENT = 5;
	
	private static final Color FIRE_COL = new Color(230/255f, 20/255f, 15/255f, 1),
							   COLD_COL = new Color(80/255f, 190/255f, 255/255f, 1),
							   ELECTRIC_COL = new Color(255/255f, 225/255f, 25/255f, 1),
							   POISON_COL = new Color(15/255f, 200/255f, 70/255f, 1),
							   NECROTIC_COL = new Color(40/255f, 20/255f, 45/255f, 1),
							   RADIENT_COL = new Color(255/255f, 240/255f, 255/255f, 1);
	
	/**
	 * The full name of the element
	 */
	private String name;
	/**
	 * The one or two character abbreviation of the element
	 */
	private String symbol;
	
	/**
	 * The row position of this element on a periodic table
	 */
	private int yPos;
	/**
	 * The column position of the element on a periodic table
	 */
	private int xPos;
	
	/**
	 * The number of protons this element has.
	 * This field also represents the element's Atomic Number.
	 */
	private int protons;
	/**
	 * The number of neutrons this element has.
	 */
	private int neutrons;
	/**
	 * The number of electrons this element has in each orbital
	 */
	private int[] electrons;
	
	/**
	 * The number of electrons on the outer-most orbital.
	 */
	private int valenceElectrons;
	
	/**
	 * How rare this element is on a scale from 0f-1f
	 */
	private float rarity;
	/**
	 * Reactiveness determines the probability of creating a new
	 * effect when combined with another element. On a scale of 0f-1f
	 */
	private float reactiveness;
	/**
	 * Hardness is a factor in determining how much damage a melee attack will do.
	 */
	private float hardness;
	/**
	 * Malleability is a factor in determining crafting difficulty.
	 */
	private float malleability;
	
	/**
	 * Notes if this element is classified as a metal, or a non-metal.
	 */
	private boolean isMetal;
	/**
	 * Not the chemistry definition of organic. Instead, this field notes
	 * if this element is common in living things (and thus non-harmful when
	 * consumed).
	 */
	private boolean isOrganic;
	
	/**
	 * A list of how much extra damage of each type this element will apply when used.
	 * On a scale of 0f-1f
	 */
	private float damageMods[];
	
	/**
	 * The time this element has until it emits a proton (lifespan of this element)
	 */
	private long halfLife;
	
	/**
	 * The table which contains this element.
	 */
	private PeriodicTable table = null;
	
	public Element(String name, String symbol, int yPos, int xPos, int protons, int neutrons, int electrons[]){
		this.name = name;
		this.symbol = symbol;
		this.yPos = yPos;
		this.xPos = xPos;
		this.protons = protons;
		this.neutrons = neutrons;
		this.electrons = electrons;
		damageMods = new float[NUM_DAMAGE_TYPES];
		Arrays.fill(damageMods, -1f);
		
		calculateValenceElectrons();
	}

	/**
	 * Uses electron orbitals, row/col positions, protons, and neutrons to determine
	 * reactiveness, hardness, malleability, metal classification, and
	 * organic classification.
	 */
	protected void calculateVariables(){
		float damageModsAvg = (damageMods[0] + damageMods[1] + damageMods[2] + damageMods[3] + damageMods[4] + damageMods[5])/2;
		
		isMetal = ((yPos*5 > 25 || xPos < 2) && xPos*4 < 30) || ((yPos*4 + xPos*2 > 35) && (electrons[2] == 1 || electrons[4] == 1 || electrons[7] == 1));
		isOrganic = ((protons < 25 && (protons + neutrons) % 23 % 4 != 0) || (protons >= 50 && (protons + neutrons) % 23 % 10 == 0));
		
		hardness = (float)(1-damageModsAvg) + 0.15f*((float)electrons[12]/(float)ELECTRON_ORBITAL_MAX_VALUES[ELECTRON_ORBITAL_TYPE_ORDER[12]]);
		if(isMetal){
			hardness *= (Math.min(Math.min(electrons[6]/10f, electrons[9]/10f), electrons[12]/14f) * 0.25f) + 0.65f;
		}
		else{
			hardness *= (0.25f*(electrons[3]/2f + electrons[4]/6f + electrons[6]/10f)/3f) + 0.2f;
		}
		
		if(hardness > 1) hardness = 1;
		if(hardness < 0) hardness = 0;
		
		malleability = (1-(hardness*hardness)*(float)(electrons[0] + electrons[1] + electrons[3] + electrons[5] + (1-damageModsAvg)*neutrons)/(float)protons);
		for(float f: damageMods){
			if(f > 0)
				malleability *= (1-f) * (1-f);
		}
		
		if(malleability > 1) malleability = 1;
		if(malleability < 0.05) malleability = 0.05f;
		
		reactiveness = (float) (0.45f*(1-(Math.abs(4-getValenceElectrons()+1)/3f)));
		if(isOrganic){
			reactiveness *= 0.5f;
		}
		if(isMetal){
			reactiveness -= Math.pow(getValenceElectrons(), -2);
		}
		if(reactiveness > 1) reactiveness = 1;
		if(reactiveness < 0) reactiveness = 0;
		
		halfLife = (long)(Math.pow((((PeriodicTable.MIN_ELEMENTS + PeriodicTable.RAND_VALUE) * 1.55)-protons-neutrons)*(1-reactiveness)*(1-damageModsAvg)*(1-damageModsAvg), 2) * 1000)/100 + MIN_HALF_LIFE;
		if(protons < table.getNumElements()/3 && damageModsAvg==0) halfLife *= 10;
		System.out.println("#" + protons + ", " + halfLife/(1000f));
	}
	
	/**
	 * Uses aspects such as organicness, metalicness, damageMods, malleability, hardness, etc.
	 * to calculate how rare this element is.
	 */
	protected void calculateRarity(){
		float score = 0;
		score += 750 * hardness;
		score += 500 * malleability;
		score += 250 * (1-reactiveness);
		if(isOrganic)
			score /= 4;
		score += yPos*50;
		
		for(float f: damageMods){
			score += 750 * (Math.pow(f*f, 0.56705));
		}
		
		rarity = (score-500)/2000f;
		rarity = 1-rarity;
		rarity *= rarity;
		if(rarity > 1) rarity = 1;
		if(rarity < 0.02) rarity =0.02f;
	}
	
	protected void setDamageMod(int modNum, float value){
		if(modNum >= 0 && modNum < NUM_DAMAGE_TYPES && damageMods[modNum] == -1f){
			damageMods[modNum] = value;
		}
	}
	
	/**
	 * Gets the number of electrons on the outer-most non-zero of the electrons array
	 */
	private void calculateValenceElectrons(){
		int i = 0;
		while(electrons[i] > 0){
			i++;
		}
		i--;
		valenceElectrons = electrons[i];
	}
	
	public java.awt.Color getColor(){
		return new java.awt.Color(getColor(damageMods).r, getColor(damageMods).g, getColor(damageMods).b);
//		float r=0, g=0, b=0;
//		
//		r += FIRE_COL.r * damageMods[EFFECT_FIRE];
//		g += FIRE_COL.g * damageMods[EFFECT_FIRE];
//		b += FIRE_COL.b * damageMods[EFFECT_FIRE];
//		
//		r += COLD_COL.r * damageMods[EFFECT_COLD];
//		g += COLD_COL.g * damageMods[EFFECT_COLD];
//		b += COLD_COL.b * damageMods[EFFECT_COLD];
//		
//		r += ELECTRIC_COL.r * damageMods[EFFECT_ELECTRIC];
//		g += ELECTRIC_COL.g * damageMods[EFFECT_ELECTRIC];
//		b += ELECTRIC_COL.b * damageMods[EFFECT_ELECTRIC];
//		
//		r += POISON_COL.r * damageMods[EFFECT_POISON];
//		g += POISON_COL.g * damageMods[EFFECT_POISON];
//		b += POISON_COL.b * damageMods[EFFECT_POISON];
//		
//		r += NECROTIC_COL.r * damageMods[EFFECT_NECROTIC];
//		g += NECROTIC_COL.g * damageMods[EFFECT_NECROTIC];
//		b += NECROTIC_COL.b * damageMods[EFFECT_NECROTIC];
//		
//		r += RADIENT_COL.r * damageMods[EFFECT_RADIENT];
//		g += RADIENT_COL.g * damageMods[EFFECT_RADIENT];
//		b += RADIENT_COL.b * damageMods[EFFECT_RADIENT];
//		
//		r = reactiveness;
//		g = 0;
//		b = 1-reactiveness;
//		
//		if(r < 0) r = 0;
//		if(g < 0) g = 0;
//		if(b < 0) b = 0;
//		if(r > 1) r = 1;
//		if(g > 1) g = 1;
//		if(b > 1) b = 1;
//		
//		return new java.awt.Color((int)(255*r), (int)(255*g), (int)(255*b));
	}
	
	protected void setTable(PeriodicTable table){
		if(this.table == null)
			this.table = table;
	}
	
	//GETTERS and SETTERS
	
	public String getName() {
		return name;
	}

	public String getSymbol() {
		return symbol;
	}

	public int getYPos() {
		return yPos;
	}

	public int getXPos() {
		return xPos;
	}

	public int getProtons() {
		return protons;
	}
	
	public int getAtomicNum(){
		return protons;
	}

	public int getNeutrons() {
		return neutrons;
	}
	
	public int getAtomicWeight(){
		return protons + neutrons;
	}

	public int[] getElectrons() {
		return electrons;
	}
	
	public int getValenceElectrons(){
		return valenceElectrons;
	}

	public float getRarity() {
		return rarity;
	}

	public float getReactiveness() {
		return reactiveness;
	}

	public float getHardness() {
		return hardness;
	}
	
	public long getHalfLife(){
		return halfLife;
	}

	public float getMalleability() {
		return malleability;
	}

	public boolean isMetal() {
		return isMetal;
	}

	public boolean isOrganic() {
		return isOrganic;
	}

	public float[] getDamageMods() {
		return damageMods;
	}
	
	public PeriodicTable getTable(){
		return table;
	}

	@Override
	public void write(Json json) {
		// TODO Auto-generated method stub
		String text = json.prettyPrint(this);
		FileHandle fileHandler = Gdx.files.local("element" + getAtomicNum() + ".txt");
		fileHandler.writeString(text, false);
		
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		// TODO Auto-generated method stub
	}

	public String toString(){
		return String.format("%s%n"
							+ "%s%n"
							+ "#: %d%n"
							+ "X: %d, Y: %d%n"
							+ "Rarity: %f%n"
							+ "Weight: %d%n"
							+ "Electron config: %s%n"
							+ "Hardness: %f%n"
							+ "Malleability: %f%n"
							+ "Reactiveness: %f%n"
							+ "Fire: %d%n"
							+ "Cold: %d%n"
							+ "Elec: %d%n"
							+ "Pois: %d%n"
							+ "Necr: %d%n"
							+ "Radi: %d%n"
							+ "isMetal: %s%n"
							+ "isOrganic: %s%n"
							+ "halfLife: %f%n", 
							getName(), getSymbol(), getAtomicNum(), xPos, yPos, rarity, getAtomicWeight(), Arrays.toString(electrons), hardness, malleability, reactiveness, (int)(100*damageMods[0]), (int)(100*damageMods[1]), (int)(100*damageMods[2]), (int)(100*damageMods[3]), (int)(100*damageMods[4]), (int)(100*damageMods[5]), isMetal, isOrganic, (halfLife/1000f));
	}
	
	public static Color getColor(float[] damageMods){
		return MathHelper.getGradient(new Color[]{FIRE_COL,  COLD_COL, ELECTRIC_COL, POISON_COL, NECROTIC_COL, RADIENT_COL}, damageMods);
//		if(damageMods.length < NUM_DAMAGE_TYPES) return Color.WHITE;
//		
//		float r=0, g=0, b=0;
//		
//		r += FIRE_COL.r * damageMods[EFFECT_FIRE];
//		g += FIRE_COL.g * damageMods[EFFECT_FIRE];
//		b += FIRE_COL.b * damageMods[EFFECT_FIRE];
//		
//		r += COLD_COL.r * damageMods[EFFECT_COLD];
//		g += COLD_COL.g * damageMods[EFFECT_COLD];
//		b += COLD_COL.b * damageMods[EFFECT_COLD];
//		
//		r += ELECTRIC_COL.r * damageMods[EFFECT_ELECTRIC];
//		g += ELECTRIC_COL.g * damageMods[EFFECT_ELECTRIC];
//		b += ELECTRIC_COL.b * damageMods[EFFECT_ELECTRIC];
//		
//		r += POISON_COL.r * damageMods[EFFECT_POISON];
//		g += POISON_COL.g * damageMods[EFFECT_POISON];
//		b += POISON_COL.b * damageMods[EFFECT_POISON];
//		
//		r += NECROTIC_COL.r * damageMods[EFFECT_NECROTIC];
//		g += NECROTIC_COL.g * damageMods[EFFECT_NECROTIC];
//		b += NECROTIC_COL.b * damageMods[EFFECT_NECROTIC];
//		
//		r += RADIENT_COL.r * damageMods[EFFECT_RADIENT];
//		g += RADIENT_COL.g * damageMods[EFFECT_RADIENT];
//		b += RADIENT_COL.b * damageMods[EFFECT_RADIENT];
//		
//		if(r < 0) r = 0;
//		if(g < 0) g = 0;
//		if(b < 0) b = 0;
//		if(r > 1) r = 1;
//		if(g > 1) g = 1;
//		if(b > 1) b = 1;
//		
//		if(r == 0 && g == 0 && b == 0) return Color.WHITE;
//		
//		return new Color(r, g, b, 1f);
	}
}