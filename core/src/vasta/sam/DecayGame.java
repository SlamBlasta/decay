package vasta.sam;

import java.util.Hashtable;

import vasta.sam.game.GameState;
import vasta.sam.game.states.GameWorld;
import vasta.sam.util.Animations;
import vasta.sam.util.Font;
import vasta.sam.util.world.TileSetManager;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

/**
 * Created by Sam Vasta, Jul 14, 2014 <br>
 * The main game class. Contains the "main loop"
 * @author Sam Vasta
 *
 */
public class DecayGame extends ApplicationAdapter {
	
	public static final String TITLE = "Decay 7";
	public static final String VERSION = "v0.0.9";
	
	/**
	 * The ratio of screen width:height for the game window.
	 */
	public static final int RATIO_WIDTH = 16, RATIO_HEIGHT = 9;
	/**
	 * This multiplier controls the actual size of the window.
	 */
	public static final double SIZE_MULTIPLIER = (1280) / RATIO_WIDTH;
	/**
	 * Width/Height of the frame
	 */
	public static final int GAME_WIDTH = (int)(RATIO_WIDTH * SIZE_MULTIPLIER),
							GAME_HEIGHT = (int)(RATIO_HEIGHT * SIZE_MULTIPLIER);
	
	/**
	 * Times per second to Update/Render. They should ideally be identical
	 */
	public static final int UPDATES_PER_SECOND = 60,
							FRAMES_PER_SECOND = 60;
	
	/**
	 * Time in ms between update/render cycles
	 */
	public static final float UPDATE_TIME = 1000f/UPDATES_PER_SECOND,
							  FRAME_TIME = 1000f/FRAMES_PER_SECOND;
	
	/**
	 * Is the game in fullscreen?
	 */
	public static final boolean FULLSCREEN = false;
	/**
	 * is the vSync enabled?
	 */
	public static final boolean V_SYNC = false;
	
	/**
	 * The number of MSAA samples to use in rendering.
	 */
	public static final int MSAA_SAMPLES = 2;
	
	/**
	 * List of Game State IDs
	 */
	public static final int WORLD_ID = 0;
	
	/**
	 * A dictionary of all Game States available to switch to
	 */
	private Hashtable<Integer, GameState> gameStates;
	
	/**
	 * The current Game State being used
	 */
	private GameState currentState;
	
	/**
	 * Frames Per Second and Updates Per Second counters
	 */
	private int fps, ups;
	/**
	 * Time of last second interval. Used for counting fps/ups
	 */
	private long lastSec = 0;
	/**
	 * Time of last update and time of last render
	 */
	private long lastUpdate, lastRender;
	/**
	 * Notes if the game loop should update or render the current Game State.
	 */
	private boolean shouldRender, shouldUpdate;
	/**
	 * Notes if the game loop should limit the updates or frames per second.
	 */
	private boolean limitUpdates, limitFrames;
	/**
	 * Notes if the game is paused or not.
	 */
	private boolean isPaused = false;
	
	@Override
	public void create () {
		TileSetManager.init();
		Animations.init();
		Font.init();
		
		Gdx.graphics.setTitle(TITLE + " " + VERSION);
		
		gameStates = new Hashtable<Integer, GameState>();
		
		shouldRender = true;
		shouldUpdate = true;
		limitUpdates = false;
		limitFrames = false;
		fps = 0;
		ups = 0;
		lastUpdate = System.currentTimeMillis();
		lastRender = System.currentTimeMillis();
		initStates();
	}
	
	/**
	 * Initializes the Game States which will be used.
	 */
	private void initStates(){
		GameWorld world = new GameWorld();
		addState(world);
		enterState(world.getID());
	}
	
	/**
	 * Switches to the Game State with the given state ID, if available.
	 * @param stateID - The ID of the Game State to switch to.
	 */
	public void enterState(int stateID){
		if(gameStates.containsKey(stateID)){
			currentState = gameStates.get(stateID);
		}
	}
	
	/**
	 * Adds the Game State to the dictionary of Game States.
	 * @param state - The Game State to add
	 */
	public void addState(GameState state){
		gameStates.put(state.getID(), state);
	}

	@Override
	public void render () {
		//Do nothing if paused.
		if(isPaused) return;
		
		//Keep track of Updates and Frames per Second
		if(System.currentTimeMillis() - lastSec >= 1000){
			lastSec = System.currentTimeMillis();
			System.out.printf("FPS: %d\nUPS: %d\n", fps, ups);
			fps = 0;
			ups = 0;
		}
		
		//Check timings
		shouldUpdate = !limitUpdates || (System.currentTimeMillis() - lastUpdate) >= UPDATE_TIME;
		shouldRender = !limitFrames || (System.currentTimeMillis() - lastRender) >= Math.floor(FRAME_TIME);
		
		//update if appropriate
		if(shouldUpdate){
			float delta = Gdx.graphics.getDeltaTime();
			if(currentState != null)
				currentState.preUpdate(this, delta);
			lastUpdate = System.currentTimeMillis();
			ups++;
		}
		
		//render if appropriate
		if(shouldRender){
			if(currentState != null){
				//Clear screen
				Gdx.gl.glClearColor(0.7f, 1, 0.5f, 1);
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				
				//Draw current state
				currentState.preRender(this);
				
				//update counters
				lastRender = System.currentTimeMillis();
				fps++;
			}
		}
		
	}
	
	/**
	 * Pauses the game
	 */
	public void pause(){
		isPaused = true;
	}
	
	/**
	 * Unpauses the game
	 */
	public void resume(){
		isPaused = false;
	}
	
	/**
	 * If game is paused, unpauses.
	 * If game is unpaused, pauses.
	 */
	public void togglePause(){
		isPaused = !isPaused;
	}
	
	public void dispose(){
		TileSetManager.destroy();
		super.dispose();
	}
	
}
