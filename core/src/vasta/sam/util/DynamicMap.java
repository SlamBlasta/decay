package vasta.sam.util;

import java.awt.Dimension;


public class DynamicMap <T>	{

	public static final int DEFAULT_SIDE_LENGTH = 5;
	
	private transient T[] matrix;
	
	private transient int sideLengthX, sideLengthY;
	
	private int middleX, middleY;
	
	public DynamicMap(){
		this(DEFAULT_SIDE_LENGTH);
	}
	
	public DynamicMap(int sideLength){
		if(sideLength%2 == 1)
			sideLength++;
		matrix = (T[]) new Object[sideLength*sideLength];
		this.sideLengthX = sideLength;
		this.sideLengthY = sideLength;
		middleX = (sideLength+1)/2;
		middleY = (sideLength+1)/2;
	}
	
	public T get(int x, int y){
		if(middleX+x < 0 || middleX+x >= sideLengthX || middleY+y < 0 || middleY+y >= sideLengthY){
			return null;
		}
		else{
			return matrix[middleX+x + sideLengthX*(middleY+y)];
		}
	}
	
	//TODO PROBLEM, YO
	public void set(int x, int y, T obj){
		while(middleX+x <= 0){
			System.out.println("expanding");
			expand(-6, 0);
		}
		while(middleX+x >= sideLengthX){
			expand(6, 0);
		}
		while(middleY+y <= 0){
			expand(0, -6);
		}
		while(middleY+y >= sideLengthY){
			expand(0, 6);
		}
		matrix[middleX+x + sideLengthX*(middleY+y)] = obj;
	}

	public Dimension size(){
		return new Dimension(sideLengthX, sideLengthY);
	}
	
	public void remove(T obj){
		for(int i = 0; i < sideLengthX; i++){
			for(int j = 0; j < sideLengthY; j++){
				if(matrix[i + sideLengthX*j].equals(obj)){
					matrix[i + sideLengthX*j] = null;
				}
			}
		}
	}
	
	public void remove(int x, int y){
		if(middleX+x < 0 || middleX+x >= sideLengthX || middleY+y < 0 || middleY+y >= sideLengthY){
			return;
		}
		else{
			matrix[x + sideLengthX*y] = null;
		}
	}
	
	private void expand(int x, int y){
		if(x%2!=0)x++;
		if(y%2!=0)y++;
		System.out.println(x + " " + y);
		System.out.println("old" + sideLengthX + " " + sideLengthY);
		
		T[] newMatrix = (T[]) new Object[(sideLengthX + Math.abs(x)) * (sideLengthY + Math.abs(y))];
		
		System.out.println("new" + (sideLengthX+Math.abs(x)) + " " + (sideLengthY+Math.abs(y)));
		
		if(x < 0)
			middleX -= x;
		if(y < 0)
			middleY -= y;
		
		System.out.println((sideLengthX+Math.abs(x)) + " " + (sideLengthY+Math.abs(y)) + " " + middleX + " " + middleY);
		
		int xOff = 0, yOff = 0;
		
		if(x<0)
			xOff = x;
		if(y<0)
			yOff = y;
		
		for(int i = 0; i < sideLengthX; i++){
			for(int j = 0; j < sideLengthY; j++){
				newMatrix[i-xOff + (j-yOff)*(sideLengthX + Math.abs(x))] = matrix[i + j*sideLengthX];
			}
		}
		matrix = newMatrix;
		sideLengthX += Math.abs(x);
		sideLengthY += Math.abs(y);
		
		
	}
	

	public void clear() {
		for(int i = 0; i < sideLengthX; i++){
			for(int j = 0; j < sideLengthY; j++){
				matrix[i+sideLengthX + (j+sideLengthY)*sideLengthX] = null;
			}
		}
	}


	public boolean contains(T arg0) {
		for(int i = 0; i < sideLengthX; i++){
			for(int j = 0; j < sideLengthY; j++){
				if(matrix[i + sideLengthX*j].equals(arg0))
					return true;
			}
		}
		return false;
	}
	
	public void check(){
		for(int i = 0; i < sideLengthX; i++){
			for(int j = 0; j < sideLengthY; j++){
				if(matrix[i + sideLengthX*j] !=null)
					System.out.println((i-middleX) + " " + (j-middleY) + " exists");
			}
		}
	}
	
	public T[] getArray(){
		return (T[])matrix;
	}
	
	public int minX(){
		return -(sideLengthX - middleX);
	}
	
	public int minY(){
		return -(sideLengthY - middleY);
	}
	
	public int maxX(){
		return sideLengthX - middleX;
	}
	
	public int maxY(){
		return sideLengthY - middleY;
	}
	
}
