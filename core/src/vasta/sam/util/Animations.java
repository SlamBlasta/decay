package vasta.sam.util;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Jul 15, 2014<br>
 * Animations class loads and registers all animations needed in the game.
 * It will allow other objects/classes to access and use these resources.
 * @author Sam Vasta
 */
public class Animations {
	
	/**
	 * The Default Animation. Given when the requested animation is not available.
	 */
	public static final String DEFAULT_ANIMATION = "animations/default.png";
	
	/**
	 * Dictionary of all available animations.
	 */
	public static HashMap<String, Animation> animations;
	
	/**
	 * Adds animations to the dictionary
	 */
	public static void init(){
		System.out.println("LOADING ANIMATIONS");
		animations = new HashMap<String, Animation>();
		
//		String[] animArgs = Gdx.files.internal("assets/animations/animationArgs.txt").;
		addAnimation(DEFAULT_ANIMATION, 32, 32, 10);
		addAnimation("animations/playerWalkUp_1.png", 32, 32, 20);
		addAnimation("animations/running.png", 180, 340, 60);
		addAnimation("animations/bullet_1.png", 32, 32, 5);
		addAnimation("animations/hazard.png", 32, 32, 10);
		System.out.println("LOADED " + animations.size() + " ANIMATIONS");
	}
	
	/**
	 * Adds an animation with given path, frame dimensions, and frame times
	 * @param texName - Relative path to animation
	 * @param frameWidth - width of each frame in pixels
	 * @param frameHeight - height of each frame in pixels
	 * @param fps - Desired frames per second
	 */
	private static void addAnimation(String texName, int frameWidth, int frameHeight, int fps){
		addAnimation(texName, frameWidth, frameHeight, 1f/fps);
	}

	/**
	 * Adds an animation with given path, frame dimensions, and frame times
	 * @param texName - relative path to animation
	 * @param frameWidth - width of each frame in pixels
	 * @param frameHeight - height of each frame in pixels
	 * @param frameTime - duration of each frame in seconds
	 */
	private static void addAnimation(String texName, int frameWidth, int frameHeight, float frameTime){
		Texture tex = new Texture(Gdx.files.internal(texName));
		int numWide = tex.getWidth()/frameWidth;
		int numHigh = tex.getHeight()/frameHeight;
		TextureRegion[][] temp = TextureRegion.split(tex, frameWidth, frameHeight);
		TextureRegion[] frames = new TextureRegion[numWide*numHigh];
		int index = 0;
		for(int i = 0; i < numHigh; i++){
			for(int j = 0; j < numWide; j++){
				frames[index++] = temp[i][j];
			}
		}
		
		animations.put(texName, new Animation(frameTime, frames));
	}
	
	/**
	 * Returns an animation from the given path. If the animation is not in the dictionary, returns the default animation
	 * @param name - relative path to animation
	 * @return The animation at the given path. If the requested animation is not available, it returns the default animation
	 */
	public static Animation get(String name){
		if(!name.startsWith("animations/"))
			name = "animations/" + name;
		
		if(!name.endsWith(".png"))
			name = name + ".png";
		
		if(animations.containsKey(name)){
			return animations.get(name);
		}
		else{
			try {
				throw new DecayException("Could not find animation by the name of '" + name + "'!!");
			} catch (DecayException e) {
				e.printStackTrace();
			}finally{
				return animations.get(DEFAULT_ANIMATION);
			}
		}
	}
	
	/**
	 * Private constructor ensures Singleton Pattern
	 */
	private Animations(){}
	
}
