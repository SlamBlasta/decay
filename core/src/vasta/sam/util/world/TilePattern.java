package vasta.sam.util.world;

/**
 *@author <b>Sam</b>, Jun 24, 2014, 2:42:49 PM
 */
public class TilePattern {
	
	public static final char WALL = '#',
							 FLOOR = '_',
							 DOES_NOT_MATTER = '?';
	
	private char pattern[];
	
	public TilePattern(char...neighbors){
		if(neighbors.length >= 9){
			this.pattern = neighbors;
		}
		else{
			System.out.println("Char set not long enough! " + neighbors.length);
			this.pattern = null;
		}
	}
	
	public TilePattern(String row1, String row2, String row3){
		try{
			pattern = new char[9];
			pattern[0] = row3.charAt(0);
			pattern[1] = row3.charAt(1);
			pattern[2] = row3.charAt(2);
			
			pattern[3] = row2.charAt(0);
			pattern[4] = '#';
			pattern[5] = row2.charAt(2);
			
			pattern[6] = row1.charAt(0);
			pattern[7] = row1.charAt(1);
			pattern[8] = row1.charAt(2);
		}catch(Exception e){
			System.out.println("Something wrong in reading strings!\n\t"+row1+"\n\t" + row2+"\n\t"+row3+"\n");
			pattern = null;
		}
	}
	
	public TilePattern(String s){
		if(s.length() >= 9){
			pattern = new char[9];
			pattern[0] = s.charAt(0);
			pattern[1] = s.charAt(1);
			pattern[2] = s.charAt(2);
			
			pattern[3] = s.charAt(3);
			pattern[4] = '#';
			pattern[5] = s.charAt(5);
			
			pattern[6] = s.charAt(6);
			pattern[7] = s.charAt(7);
			pattern[8] = s.charAt(8);
		}
	}
	
	public char[] getPattern(){
		return pattern;
	}
	
	public int compare(TilePattern tp){
		int val = 0;
		char[] tpChars = tp.getPattern();
		for(int i = 0; i < 9; i++){
			char c1 = pattern[i];
			char c2 = tpChars[i];
			if(c1 == c2 && (c1 != DOES_NOT_MATTER && c2 != DOES_NOT_MATTER)){
				val+=3;
			}
			else if(c1 == DOES_NOT_MATTER || c2 == DOES_NOT_MATTER){
				val++;
			}
			else{
				return -1;
			}
		}
		
		return val;
	}
	
	private boolean compare(char c1, char c2){
		if(c1 == c2 || c1 == DOES_NOT_MATTER || c2 == DOES_NOT_MATTER){
			return true;
		}
		return false;
	}
}
