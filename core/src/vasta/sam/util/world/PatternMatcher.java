package vasta.sam.util.world;

import java.util.HashMap;

/**
 *@author <b>Sam</b>, Jun 24, 2014, 2:40:49 PM
 */
public class PatternMatcher {
	private HashMap<TilePattern, Integer> patternDictionary;
	
	public PatternMatcher(){
		patternDictionary = new HashMap<TilePattern, Integer>();
		
		//Peninsulas
		addPattern(new TilePattern(
				"?_?",
				"_ #",
				"?_?"
			), TileSetManager.pL);
		addPattern(new TilePattern(
				"?_?",
				"# _",
				"?_?"
			), TileSetManager.pR);
		addPattern(new TilePattern(
				"?_?",
				"# #",
				"?_?"
			), TileSetManager.pSH);
		addPattern(new TilePattern(
				"?_?",
				"_ _",
				"?#?"
			), TileSetManager.pT);
		addPattern(new TilePattern(
				"?#?",
				"_ _",
				"?_?"
			), TileSetManager.pB);
		addPattern(new TilePattern(
				"?#?",
				"_ _",
				"?#?"
			), TileSetManager.pSV);
		
		
		addPattern(new TilePattern(
				"?_?",
				"_ _",
				"?_?"
			), TileSetManager.column);
		
		
		//T-Blocks
		addPattern(new TilePattern(
				"_#_",
				"# #",
				"?_?"
			), TileSetManager.tLRT);
		addPattern(new TilePattern(
				"?_?",
				"# #",
				"_#_"
			), TileSetManager.tLRB);
		addPattern(new TilePattern(
				"_#?",
				"# _",
				"_#?"
			), TileSetManager.tLBT);
		addPattern(new TilePattern(
				"?#_",
				"_ #",
				"?#_"
			), TileSetManager.tRBT);
		
		
		//CENTER (completely surrounded)
		addPattern(new TilePattern(
				"###",
				"# #",
				"###"
				), TileSetManager.c);
		
		//LEFT WALLS
		addPattern(new TilePattern(
				"?##",
				"_ #",
				"?##"
			), TileSetManager.l);
		addPattern(new TilePattern(
				"?#_",
				"_ #",
				"?##"
				), TileSetManager.l_icTR);
		addPattern(new TilePattern(
				"?##",
				"_ #",
				"?#_"
				), TileSetManager.l_icBR);
		
		//TOP WALLS
		addPattern(new TilePattern(
				"?_?",
				"# #",
				"###"
			), TileSetManager.t);
		addPattern(new TilePattern(
				"?_?",
				"# #",
				"_##"
				), TileSetManager.t_icBL);
		addPattern(new TilePattern(
				"?_?",
				"# #",
				"##_"
				), TileSetManager.t_icBR);
		
		//RIGHT WALLS
		addPattern(new TilePattern(
				"##?",
				"# _",
				"##?"
			), TileSetManager.r);
		addPattern(new TilePattern(
				"##?",
				"# _",
				"_#?"
				), TileSetManager.r_icBL);
		addPattern(new TilePattern(
				"_#?",
				"# _",
				"##?"
				), TileSetManager.r_icTL);
		
		//BOTTOM WALLS
		addPattern(new TilePattern(
				"###",
				"# #",
				"?_?"
				), TileSetManager.b);
		addPattern(new TilePattern(
				"##_",
				"# #",
				"?_?"
				), TileSetManager.b_icTR);
		addPattern(new TilePattern(
				"_##",
				"# #",
				"?_?"
			), TileSetManager.b_icTL);
		
		
		
		//INVERSE CORNERS
		
		//One corner
		addPattern(new TilePattern(
				"###",
				"# #",
				"##_"
			), TileSetManager.icBR);
		addPattern(new TilePattern(
				"###",
				"# #",
				"_##"
			), TileSetManager.icBL);
		addPattern(new TilePattern(
				"##_",
				"# #",
				"###"
			), TileSetManager.icTR);
		addPattern(new TilePattern(
				"_##",
				"# #",
				"###"
			), TileSetManager.icTL);
		
		
		//Two corner
		addPattern(new TilePattern(
				"##_",
				"# #",
				"_##"
			), TileSetManager.icTR_BL);
		addPattern(new TilePattern(
				"_#_",
				"# #",
				"###"
			), TileSetManager.icTL_TR);
		addPattern(new TilePattern(
				"###",
				"# #",
				"_#_"
			), TileSetManager.icBL_BR);
		addPattern(new TilePattern(
				"_##",
				"# #",
				"_##"
			), TileSetManager.icTL_BL);
		addPattern(new TilePattern(
				"##_",
				"# #",
				"##_"
			), TileSetManager.icTR_BR);
		addPattern(new TilePattern(
				"_##",
				"# #",
				"##_"
			), TileSetManager.icTL_BR);
		
		
		//Three Corner
		addPattern(new TilePattern(
				"_#_",
				"# #",
				"_##"
				), TileSetManager.icTL_TR_BL);
		addPattern(new TilePattern(
				"_#_",
				"# #",
				"##_"
				), TileSetManager.icTL_TR_BR);
		addPattern(new TilePattern(
				"##_",
				"# #",
				"_#_"
				), TileSetManager.icTR_BL_BR);
		addPattern(new TilePattern(
				"_##",
				"# #",
				"_#_"
				), TileSetManager.icTL_BL_BR);
		
		
		//All Corner
		addPattern(new TilePattern(
				"_#_",
				"# #",
				"_#_"
				), TileSetManager.icAll);
		
		
		
		//Regular Corners!
		
		//top left
		addPattern(new TilePattern(
				"?_?",
				"_ #",
				"?##"
			), TileSetManager.cTL);
		addPattern(new TilePattern(
				"?_?",
				"_ #",
				"?#_"
			), TileSetManager.cTL_icBR);
		
		//top right
		addPattern(new TilePattern(
				"?_?",
				"# _",
				"##?"
				), TileSetManager.cTR);
		addPattern(new TilePattern(
				"?_?",
				"# _",
				"_#?"
			), TileSetManager.cTR_icBL);
		
		//bottom left
		addPattern(new TilePattern(
				"?##",
				"_ #",
				"?_?"
				), TileSetManager.cBL);
		addPattern(new TilePattern(
				"?#_",
				"_ #",
				"?_?"
			), TileSetManager.cBL_icTR);
		
		//bottom right
		addPattern(new TilePattern(
				"##?",
				"# _",
				"?_?"
				), TileSetManager.cBR);
		addPattern(new TilePattern(
				"_#?",
				"# _",
				"?_?"
			), TileSetManager.cBR_icTL);

	}
	
	public void addPattern(TilePattern pattern, int t){
		patternDictionary.put(pattern, t);
	}
	
	public int getTile(TilePattern pattern){
		TilePattern bestMatch = null;
		int accuracyRating = -1;
		for(TilePattern tp: patternDictionary.keySet()){
			int rating = pattern.compare(tp);
			if(rating > accuracyRating){
				bestMatch = tp;
				accuracyRating = rating;
			}
		}
		
		if(bestMatch != null)
			return patternDictionary.get(bestMatch);
		
		System.out.println(new String(pattern.getPattern()) + "\t\t...no match");
		return -1;
	}
	
	public int getTile(char...pattern){
		return getTile(new TilePattern(pattern));
	}
}
