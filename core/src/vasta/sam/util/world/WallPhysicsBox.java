package vasta.sam.util.world;

import vasta.sam.game.entities.systems.movers.physics.PhysicsSystem;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Sam Vasta, Sep 10, 2014
 * <p>
 * This object contains a Box2d body which is automatically a static body.
 * This object can be set active and inactive through the load() and unload()
 * methods.
 * <p>
 * @author Sam Vasta
 */
public class WallPhysicsBox {
	
	/**
	 * The Box2D body assigned to this system.
	 */
	protected Body body;
	
	public WallPhysicsBox(World world){
		this(world, new Vector2(0, 0), new Vector2(TileSetManager.TILE_SIZE, TileSetManager.TILE_SIZE));
	}
	
	public WallPhysicsBox(World world, float x, float y, float width, float height){
		this(world, new Vector2(x, y), new Vector2(width, height));
	}
	
	public WallPhysicsBox(World world, Vector2 position, Vector2 dimensions){
		createBody(world, position);
		createFixture(dimensions.x, dimensions.y);
		setPosition(position);
		setUserData();
	}
	
	public boolean collidesWith(Vector2 pos){
		return body.getFixtureList().get(0).testPoint(PhysicsSystem.convertToBox(pos));
	}
	
	private void setUserData(){
		body.setUserData(this);
		for(Fixture f: body.getFixtureList()){
			f.setUserData(this);
		}
	}
	
	public void setPosition(Vector2 pos){
		body.setTransform(PhysicsSystem.convertToBox(pos), body.getAngle());
	}
	
	public void setVelocity(Vector2 vel){
		body.setLinearVelocity(PhysicsSystem.convertToBox(vel));
	}
	
	public Vector2 getPosition(){
		return PhysicsSystem.convertToWorld(body.getPosition());
	}
	
	public Vector2 getVelocity(){
		return PhysicsSystem.convertToWorld(body.getLinearVelocity());
//		return convertToWorld(body.getLinearVelocity());
	}
	
	/**
	 * Returns position directly from the body. Does not convert to World from Box.
	 * @return position - position of the body in BoxWorld values.
	 */
	public Vector2 getBoxPosition(){
		return body.getPosition();
	}
	/**
	 * Returns velocity directly from the body. Does not convert to World from box.
	 * @return velocity - velocity of the body in BoxWorld values
	 */
	public Vector2 getBoxVelocity(){
		return body.getLinearVelocity();
	}
	
	/**
	 * Sets the position directly. Does not convert from world to box.
	 * @param position - Position in BoxWorld values
	 */
	public void setBoxPosition(Vector2 position){
		body.setTransform(position,  body.getAngle());
	}
	
	/**
	 * Sets the velocity directly. Does not convert from world to box.
	 * @param vel - Velocity in BoxWorld values.
	 */
	public void setBoxVelocity(Vector2 vel){
		body.setLinearVelocity(vel);
	}
	
	public void setRotation(float theta){
		body.setTransform(body.getPosition(), theta);
	}
	
	public void unload(){
		body.setActive(false);
	}
	
	public void load(){
		body.setActive(true);
	}
	
	/**
	 * Creates a body to represent the Entity
	 * @param world - Box2D world the body will exist in
	 * @param position - The starting position of the body in the world
	 */
	protected void createBody(World world, Vector2 position){
		BodyDef bodyDef = new BodyDef(); 
	    bodyDef.type = BodyType.StaticBody;
	    bodyDef.position.set(PhysicsSystem.convertToBox(position.x),PhysicsSystem.convertToBox(position.y));
	    bodyDef.angle=0f;
	    bodyDef.fixedRotation = true;
	    body = world.createBody(bodyDef);
	}
	/**
	 * Attatches a Box2D fixture to the body
	 * @param width - width of the fixture
	 * @param height - height of the fixture
	 */
	protected void createFixture(float width, float height){
		PolygonShape bodyShape = new PolygonShape();
		
		float w=PhysicsSystem.convertToBox(width/2f);
		float h=PhysicsSystem.convertToBox(height/2f);
		bodyShape.setAsBox(w,h);
		
		FixtureDef fixtureDef=new FixtureDef();
		fixtureDef.shape=bodyShape;
		fixtureDef.isSensor = false;
		body.createFixture(fixtureDef);
		bodyShape.dispose();
	}

}
