package vasta.sam.util.world;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vasta.sam.util.MathHelper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Sam Vasta, Sep 10, 2014
 * <p>
 * This class loads and manages the various tilesets in the game.
 * <p>
 * @author Sam Vasta
 */
public class TileSetManager {
	
	/**
	 * Returns the tileset registered with the given name.
	 * @param name
	 * @return
	 */
	public static TiledMapTileSet getTileSet(String name){
		return tileSetDictionary.get(name);
	}
	
	/**
	 * Returns a specified tile from the specified tile set.
	 * @param setName
	 * @param tileID
	 * @return
	 */
	public static TiledMapTile getStandardTile(int tileID, String setName){
		return tileSetDictionary.get(setName).getTile(tileID);
	}
	
	/**
	 * returns a tile of specified id from the set of unique tiles
	 * @param tileID
	 * @return
	 */
	public static TiledMapTile getUniqueTile(String name){
		return getUniqueTile(uniqueTileIds.get(name));
	}
	
	/**
	 * returns a tile of specified id from the set of unique tiles
	 * @param tileID
	 * @return
	 */
	public static TiledMapTile getUniqueTile(int tileID){
		return tileSetDictionary.get(UNIQUE_TILESET).getTile(tileID);
	}
	
	/**
	 * Given a tileID, and a list of valid tilesets, this function
	 * returns a tile of id given from a random valid tileset.
	 * @param tileID
	 * @param setNames
	 * @return
	 */
	public static TiledMapTile getStandardTile(int tileID, String...setNames){
		TiledMapTile t = null;
		boolean[] success = new boolean[setNames.length];
		int numTries = 0;
		while(t == null && numTries < setNames.length){
			t = getStandardTile(tileID, setNames[MathHelper.nextInt(setNames.length)]);
			numTries++;
		}
		return t;
	}
	
	public static boolean isWall(int tileID){
		return !(tileID == TileSetManager.floor);// || tileSetDictionary.get(UNIQUE_TILESET).getTile(tileID))
	}
	
	public static final int TILE_SIZE = 192;
	private static HashMap<String, TiledMapTileSet> tileSetDictionary;
	private static HashMap<String, Integer> uniqueTileIds;
	private static HashMap<String, TextureAtlas> textureAtlases;
	private static DecimalFormat formater = new DecimalFormat("%02d");
	public static final String UNIQUE_TILESET = "uniqueTiles";
	private static final float ANIMATION_INTERVAL = 16f;
	/**
	 * This int keeps track of the next tile ID available.
	 * This is used for unique tiles which are not in a standard tileSet.
	 * TileIDs below 100 are reserved for these standard tileSets, so this
	 * field starts at 100.
	 */
	private static int nextTileID = 100;
	
	public static final int defaultTile = nextTileID++;
	
	/**
	 * Tile IDs for standard tileSets
	 */
	public static final int pL = 0;
	public static final int pR = 1;
	public static final int pSH = 2;
	public static final int pT = 3;
	public static final int pB = 4;
	public static final int pSV = 5;
	
	public static final int column = 6;

	public static final int tLRT = 8;
	public static final int tLRB = 9;
	public static final int tLBT = 11;
	public static final int tRBT = 12;

	public static final int b = 10;
	public static final int b_icTL = 39;
	public static final int b_icTR = 46;
	public static final int l = 13;
	public static final int l_icTR = 27;
	public static final int l_icBR = 40;
	public static final int t = 16;
	public static final int t_icBL = 28;
	public static final int t_icBR = 33;
	public static final int r = 19;
	public static final int r_icTL = 34;
	public static final int r_icBL = 45;
	public static final int c = 14;
	
	public static final int icBR = 37;
	public static final int icBL = 35;
	public static final int icTR = 41;
	public static final int icTL = 47;
	public static final int icTR_BL = 15;
	public static final int icTL_TR = 17;
	public static final int icBL_BR = 18;
	public static final int icTL_BL = 20;
	public static final int icTR_BR = 21;
	public static final int icTL_BR = 23;
	public static final int icTL_TR_BL = 26;
	public static final int icTL_TR_BR = 32;
	public static final int icTL_BL_BR = 38;
	public static final int icTR_BL_BR = 44;
	public static final int icAll = 22;
	
	public static final int cTL_icBR = 24;
	public static final int cTR = 31;
	public static final int cTL = 25;
	public static final int cTR_icBL = 30;
	public static final int cBL_icTR = 36;
	public static final int cBL = 37;
	public static final int cBR_icTL = 42;
	public static final int cBR = 43;
	
	public static final int floor = 48;
	public static final int wall = 14;

	/**
	 * Creates tileSets and unique tiles.
	 * This is 1(one) of 2(two) methods you are allowed to touch.
	 * The other is headed "private static void putUniqueTiles()"
	 * DO NOT TOUCH OTHER METHODS OR YOU WILL SCREW THINGS UP AND SAM WILL GET MAD.
	 * 
	 * <3<3<3 i   t r u s t    u <3<3<3
	 */
	public static void init(){
		tileSetDictionary = new HashMap<>();
		uniqueTileIds = new HashMap<>();
		textureAtlases = new HashMap<>();
		putUniqueTiles();
		putStandardTileSet("tiles/defaultWalls.txt");
	}
	
	public static void destroy(){
		textureAtlases.clear();
	}
	
	
	/**
	 * Put unique tiles here!
	 */
	private static void putUniqueTiles(){
		TiledMapTileSet uniqueTileSet = new TiledMapTileSet();
		
		/*Add tiles here!
		This is an example of how to load a SINGLE tile:
			TiledMapTile t = loadAndReturnAnimatedTile("Example.path");
			uniqueTileSet.putTile(nextTileID, t);
			uniqueTileIds.put("EXAMPLE TILE", nextTileID++);
		
		If a file has more than one animated tile (Stacked on top of eachother):
			TiledMapTile t = loadAndReturnAnimatedTile("Example.path").get(TILE_NUMBER_BASE_0);
			uniqueTileSet.putTile(nextTileID, t);
			uniqueTileIds.put("EXAMPLE TILE", nextTileID++);
		*/
		
//		TiledMapTile t = getStaticTile("floor.png", floor);
//		uniqueTileSet.putTile(defaultTile, t);
//		uniqueTileIds.put("default.png", defaultTile);
		
		
		
		
		
		//Stop adding tiles beyond here!
		tileSetDictionary.put(UNIQUE_TILESET, uniqueTileSet);
	}
	
	/**
	 * Creates a {@link com.badlogic.gdx.maps.tiled.TiledMapTileSet},
	 * loads a list of {@link com.badlogic.gdx.maps.tiled.TiledMapTile},
	 * and adds them to the set. It then adds the set to the dictionary.
	 * @param texPath
	 */
	private static void putStandardTileSet(String texPath){
		TiledMapTileSet tileSet = new TiledMapTileSet();
		List<TiledMapTile> tileList = loadAndReturnStaticTiles(texPath);
			
		for(TiledMapTile t: tileList){
			tileSet.putTile(t.getId(), t);
		}
		tileSetDictionary.put(texPath, tileSet);
	}
	
	/**
	 * Creates and returns an animated tile using the given texture.
	 * The texture should have frames placed immediately after eachother
	 * on the horizontal axis, and separate animations on the vertical axis.
	 * @param frameSheet
	 * @param animNumber
	 * @param tileSize
	 * @return
	 */
	private static AnimatedTiledMapTile getAnimatedTile(String atlasPath, String...frameNames){
		Array<StaticTiledMapTile> frames = new Array<>();
		TextureAtlas atlas = getAtlas(atlasPath);
		for(String s: frameNames){
			frames.add(new StaticTiledMapTile(atlas.findRegion(s)));
		}
		AnimatedTiledMapTile animTile = new AnimatedTiledMapTile(ANIMATION_INTERVAL, frames);
		animTile.setId(nextTileID++);
		return animTile;
	}
	
	/**
	 * Loads, creates, and returns a static tile from the given sprite sheet.
	 * @param frameSheet
	 * @param xPos - the column number of the tile (base 0)
	 * @param yPos - the row number of the tile (base 0)
	 * @param tileSize
	 * @return
	 */
	private static StaticTiledMapTile getStaticTile(String atlasPath, int tileNum){
		TextureAtlas atlas = getAtlas(atlasPath);
		System.out.println("tile" + String.format("%02d", tileNum));
		return new StaticTiledMapTile(atlas.findRegion("tile" + String.format("%02d", tileNum)));
	}
	

	/**
	 * Takes a given tile sheet and breaks it into static (non-animated) tiles.
	 * Uses tileSize instead of the default size.
	 * This is specialized for default (standard) tilesheets.
	 * @param texPath
	 * @return a list of tiles from the tileSheet
	 */
	private static List<TiledMapTile> loadAndReturnStaticTiles(String atlasPath){
		TextureAtlas atlas = getAtlas(atlasPath);
		
		ArrayList<TiledMapTile> tileList = new ArrayList<>();
		for(int i = 0; i < atlas.getRegions().size; i++){
			TiledMapTile t = getStaticTile(atlasPath, i);
			t.setId(i);
			tileList.add(t);
		}
		
		return tileList;
	}
	
	private static TextureAtlas getAtlas(String atlasPath){
		if(textureAtlases.containsKey(atlasPath)){
			return textureAtlases.get(atlasPath);
		}
		else{
			TextureAtlas atlas = new TextureAtlas(atlasPath);
			textureAtlases.put(atlasPath, atlas);
			return atlas;
		}
	}
	
	private TileSetManager(){}
}
