package vasta.sam.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Jul 15, 2014<br>
 * Animation Wrappers handle animation updates and rendering.
 * 
 * @author Sam Vasta
 */
public class AnimationWrapper {
	
	/**
	 * Time since the animation started
	 */
	private float deltaTime;
	/**
	 * The animation to use
	 */
	private Animation a;
	/**
	 * The current frame the animation is displaying
	 */
	private TextureRegion currentFrame;
	/**
	 * Notes if the animation repeats
	 */
	private boolean looping;
	private String name;
	
	public AnimationWrapper(Animation a, boolean looping){
		this.a = a;
		this.looping = looping;
		deltaTime = 0;
	}
	
	public AnimationWrapper(Animation a){
		this(a, true);
	}
	
	/**
	 * updates the animation's delta & current Frame
	 */
	public void update(){
		deltaTime += Gdx.graphics.getDeltaTime();
		currentFrame = a.getKeyFrame(deltaTime, looping);
	}
	
	/**
	 * Draws the animation's current frame
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 * @param originX
	 * @param originY
	 * @param width
	 * @param height
	 * @param scaleX
	 * @param scaleY
	 * @param rotation
	 * @param clockwise
	 */
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation, boolean clockwise){
		update();
		batch.draw(currentFrame, x, y, originX, originY, width, height, scaleX, scaleY, rotation, clockwise);
	}
	
	/**
	 * Draws the animation's current frame
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 * @param originX
	 * @param originY
	 * @param width
	 * @param height
	 * @param scaleX
	 * @param scaleY
	 * @param rotation
	 */
	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation){
		update();
		batch.draw(currentFrame, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
	}
	
	/**
	 * draws the animation's current frame
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 */
	public void draw(Batch batch, float x, float y){
		update();
		batch.draw(currentFrame, x, y);
	}
	
	/**
	 * draws the animation's current frame
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 * @param rotation - rotation
	 */
	public void draw(Batch batch, float x, float y, float rotation){
		update();
		Texture t = currentFrame.getTexture();
		batch.draw(currentFrame, x, y, t.getWidth()/2, t.getHeight()/2, t.getWidth(), t.getHeight(), 1f, 1f, rotation);
	}
	
	/**
	 * draws the animation's current frame
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 * @param originX
	 * @param originY
	 */
	public void draw(Batch batch, float x, float y, float originX, float originY){
		update();
		batch.draw(currentFrame, x, y, originX, originY);
	}
	
	/**
	 * draws the animation's current frame
	 * @param q
	 * @param batch
	 */
	public void draw(Batch batch){
		update();
		batch.draw(currentFrame, 0, 0);
	}
	
	/**
	 * Returns the current frame
	 * @return the current frame
	 */
	public TextureRegion getCurrentFrame(){
		return currentFrame;
	}
	
}
