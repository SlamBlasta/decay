package vasta.sam.util;

/**
 * Created by Sam Vasta, Sep 8, 2014
 * <p>
 * A point which contains an x and y value.
 * These values are exclusively integers.
 * <p>
 * @author Sam Vasta
 */
public class IntPoint {
	
	private int x, y;
	
	public IntPoint(){
		this(0, 0);
	}
	public IntPoint(int x, int y){
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String toString(){
		return ("X: " + x + ", Y: " + y);
	}
}
