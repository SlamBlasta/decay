package vasta.sam.util;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Sam Vasta, Jul 15, 2014<br>
 * Contains a list of animations which can be
 * switched between for rendering
 * @author Sam Vasta
 */
public class AnimationHandler {
	/**
	 * Dictionary of available animations
	 */
	private HashMap<String, AnimationWrapper> anims;
	/**
	 * Name of current animation
	 */
	private String currentName;
	/**
	 * current animation
	 */
	private AnimationWrapper currentAnimation;
	
	public AnimationHandler(String...animations){
		anims = new HashMap<String, AnimationWrapper>();
		for(String s: animations){
			anims.put(s, new AnimationWrapper(Animations.get(s)));
		}
		if(!anims.isEmpty())
			currentAnimation = anims.get(animations[0]);
	}
	
	public AnimationHandler(String[] animations, boolean[] loopData){
		anims = new HashMap<String, AnimationWrapper>();
		for(int i = 0; i < animations.length; i++){
			boolean looping;
			if(loopData.length > i){
				looping = loopData[i];
			}
			else
				looping = true;
			
			anims.put(animations[i], new AnimationWrapper(Animations.get(animations[i]), looping));
		}
		
		if(!anims.isEmpty())
			currentAnimation = anims.get(animations[0]);
	}
	
	/**
	 * Updates the current animation to switch frames.
	 */
	public void update(){
		currentAnimation.update();
	}
	
	/**
	 * Draws the current animation at the given xy position
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 */
	public void draw(Batch batch, float x, float y){
		currentAnimation.draw(batch, x, y);
	}
	
	/**
	 * draws the current animation at the given xy position with the given rotation angle.
	 * The center of rotation is the center of the texture.
	 * @param q
	 * @param batch
	 * @param x
	 * @param y
	 * @param rotation
	 */
	public void draw(Batch batch, float x, float y, float rotation){
		currentAnimation.draw(batch, x, y, rotation);
	}
	
	

	/**
	 * Returns the current frame of the current animation
	 * @return the current frame of the current animation
	 */
	public TextureRegion getCurrentFrame(){
		return currentAnimation.getCurrentFrame();
	}
	
	/**
	 * Switches the current animation to the animation registered with the given name.
	 * If there is no animation registered with that name, this method does nothing.
	 * @param animName
	 */
	public boolean switchTo(String animName){
		if(anims.containsKey(animName)){
			currentAnimation = anims.get(animName);
			currentName = animName;
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the name of the current animation.
	 * @return the name of the current animation.
	 */
	public String getCurrentAnimation(){
		return currentName;
	}

	public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
		currentAnimation.update();
		currentAnimation.draw(batch, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
	}
}
