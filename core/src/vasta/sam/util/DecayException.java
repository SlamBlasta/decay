package vasta.sam.util;

/**
 * Created by Sam Vasta, Jul 15, 2014<br>
 * An extension of Exception used to differentiate between native exceptions.
 * @author Sam Vasta
 */
public class DecayException extends Exception{
	String msg;
	public DecayException(String s){
		super(s);
		msg = s;
	}
	
	/**
	 * Print only what was given through the constructor, if you're into that sort of thing.
	 */
	public void printErr(){
		System.err.println(msg);
	}
}
