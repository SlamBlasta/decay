package vasta.sam.util;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

/**
 * Created by Sam Vasta, Aug 29, 2014
 *<p>
 * The Font class handles creation and rendering of fonts.
 *<p>
 * @author Sam Vasta
 */
public class Font {

	public static final FileHandle PATH_TO_FONT_TTF = Gdx.files.local("font/insane-hours-2.regular.ttf");
	
	public static HashMap<Integer, BitmapFont> fonts = new HashMap<Integer, BitmapFont>();
	
	public static final int TITLE_FONT_SIZE = 40,
							SMALL_FONT_SIZE = 20;
	
	private static BitmapFont currentFont;
	private static FreeTypeFontGenerator generator;
	private static Color fontCol = Color.WHITE;
	
	public static void init(){
		setSize(40);
		setSize(20);
	}
	
	/**
	 * Sets the size of the font to draw by creating a new font of 
	 * the specified size, and adding it to a HashMap of fonts.
	 * If the size has already been generated, no new BitmapFont is
	 * created.
	 * <p>
	 * At the end of loading, the font color is changed to match the
	 * previous color. 
	 * @param size
	 */
	public static void setSize(int size){
		if(fonts.containsKey(size)){
			currentFont = fonts.get(size);
		}
		else{
			generator = new FreeTypeFontGenerator(PATH_TO_FONT_TTF);
			FreeTypeFontParameter parameter = new FreeTypeFontParameter();
			parameter.size = size;
			parameter.kerning = false;
			fonts.put(size, generator.generateFont(parameter));
			currentFont = fonts.get(size);
			generator.dispose();
		}
		currentFont.setColor(fontCol);
		
	}
	
	/**
	 * Sets the color for drawing fonts. If the color has not changed,
	 * this function does nothing.
	 * @param c
	 */
	public static void setColor(Color c){
		if(!currentFont.getColor().equals(c)){
			fontCol = c;
			currentFont.setColor(c);
		}
	}
	
	/**
	 * A utility funtion for drawing text of a certain size in one method call
	 * @param size
	 * @param text
	 * @param x
	 * @param y
	 */
	public static void setAndRender(int size, Batch batch, String text, int x, int y){
		setSize(size);
		render(batch, text, x, y);
	}
	
	/**
	 * A utility function for drawing text of a certain size and color in one
	 * method call
	 * @param size
	 * @param col
	 * @param text
	 * @param x
	 * @param y
	 */
	public static void setAndRender(int size, Color col, Batch batch, String text, int x, int y){
		setSize(size);
		setColor(col);
		render(batch, text, x, y);
	}
	
	public static void render(Batch batch, String  text, int x, int y){
		currentFont.draw(batch, text, x /*+ 90*/, y /*+ 170*/);
	}
	
}
