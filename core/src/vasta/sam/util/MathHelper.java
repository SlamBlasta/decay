package vasta.sam.util;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Sam Vasta, Aug 6, 2014<br>
 * @author Sam Vasta
 */
public class MathHelper {

	//Did work in MathHelper from WS2
	
	private static final int SEED = (int)(Math.random()*1000d);
	public static Random r = new Random(SEED);

	public static int nextInt(int max){
		return r.nextInt(max);
	}
	
	public static int nextInt(){
		return r.nextInt();
	}
	
	public static int nextInt(int min, int max){
		if(Math.abs(min-max) == 0) return Math.min(min, max);
		return r.nextInt(Math.abs(min-max)) + Math.min(min, max);
	}
	
	public static double random(){
		return r.nextDouble();
	}
	
	public static void reset(){
		r = new Random(SEED);
	}
	
	public static float angleTo(Vector2 v1, Vector2 v2){
		return angleTo(v1.x, v1.y, v2.x, v2.y);
	}
	
	public static float angleTo(float x1, float y1, float x2, float y2){
		return (float)Math.atan2((y1-y2), (x1-x2));
	}
	
	public static Color getGradient(Color[] colors, float[] weights){
		if(weights.length < colors.length){
			return Color.WHITE;
		}
		else{
			float h = 0, s = 0, l = 0;
			float totalWeight = 0;
			for(int i = 0; i < colors.length; i++){
				float[] hsl = rgbToHsl(colors[i].r, colors[i].g, colors[i].b);
				h += hsl[0]*weights[i];
				s += hsl[1]*weights[i];
				l += hsl[2]*weights[i];
				totalWeight += weights[i];
			}
			
			if(totalWeight == 0){ return Color.WHITE;}
			
			h /= totalWeight;
			s /= totalWeight;
			l /= totalWeight;
			float[] rgbVals = hslToRgb(h, s, l);
			return new Color(rgbVals[0], rgbVals[1], rgbVals[2], 1f);
		}
	}
	
	public static float[] hslToRgb(float h, float s, float l){
		float r, g, b;
		
		if(s==0){
			r = 1;
			g = 1;
			b = 1;
		}
		else{
			float q = l < 0.5 ? l*(1+s): l+s-l*s;
			float p = 2*l-q;
			r = hueToRgb(p, q, h+1/3f);
			g = hueToRgb(p, q, h);
			b = hueToRgb(p, q, h-1/3f);
		}
		
		return new float[]{r, g, b};
	}
	
	private static float hueToRgb(float p, float q, float t){
		if(t < 0) t++;
		if(t > 1) t--;
		if(t < 1/6f) return p + (q-p)*6*t;
		if(t < 1/2f) return q;
		if(t < 2/3f) return p + (q-p)*(2/3f - t) * 6;
		return p;
	}
	
	public static float[] rgbToHsl(float r, float g, float b){
		float max = Math.max(Math.max(r, g), b);
		float min = Math.min(Math.min(r, g), b);
		float h=0, s=0, l = (max + min)/2f;
		
		if(max == min){
			h = 0;
			s = 0;
		}
		else{
			float d = max-min;
			s = l > 0.5 ? d / (2 - max - min): d/(max+min);
			if(max == r){
				h = (g-b)/d+(g<b?6:0);
			}
			else if(max == g){
				h = (b-r)/d+2;
			}
			else if(max == b){
				h = (r-g)/d+4;
			}
			h/=6f;
		}
		
		return new float[]{h, s, l};
	}
}
